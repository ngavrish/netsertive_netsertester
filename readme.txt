Netsertester Repo includes Functional UI Test Framework and Tests.

Currently contains Automated UI tests for:
    AMC
    PLATFORM
    LYON

Install Prerequisites:
    Python 3.4+ (included in dev setup)
    any JDK 7 or JDK 8 (included in dev setup)
    Firefox
    Chrome

To install netsertester and its dependencies:
    sudo -H python3 install.py

Custom HTML reinstall:
    Occasionally our custom nose html plugin is updated. To reinstall only custom HTML plugin after modifications,
    go to /Netsertive/Repo/netsertester/plugins/nose-htmloutput-withscreens and run:

    sudo -H python3 install.py

Adding Dependencies to requirements.txt:
    Please note, if adding tests that require new dependency or third party library, requirements.txt will need to be updated.