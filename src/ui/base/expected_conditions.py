from selenium.common.exceptions import StaleElementReferenceException


class text_to_be_present_in_element_attribute:

    def __init__(self, locator: tuple, attribute: str, text: str):
        self.text = text
        self.locator = locator
        self.attribute = attribute

    def __call__(self, driver):
        try:
            attribute_value = driver.find_element(*self.locator).get_attribute(self.attribute)
            return attribute_value and self.text in attribute_value
        except StaleElementReferenceException:
            return False