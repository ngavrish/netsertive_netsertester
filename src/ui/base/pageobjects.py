from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.common.keys import Keys
import logging
from inspect import signature


DEFAULT_TIMEOUT = 60


class NewStyleFormattingLoggerAdapter(logging.LoggerAdapter):
    """
    An adapter used to log messages with new style formatting.
    """

    class FormattedMessage(object):
        """
        Utility class used to return a string with data applied.
        We need this class instead of immediate formatting for lazy logs processing.
        Because log messages might be filtered out by log filters.
        """
        def __init__(self, message, kwargs):
            self.message = message
            self.kwargs = kwargs

        def __str__(self):
            return str(self.message).format(**self.kwargs)

    def __init__(self, logger, extra=None):
        super().__init__(logger, extra)
        self._log_arguments = signature(logger._log).parameters.keys()

    def process(self, msg, kwargs):
        """
        Returns message wrapped by FormattedMessage class and kwargs required by logger._log method.
        If you will not filter them here, logger._log will receive formatting kwargs as arguments producing TypeError.
        """
        return self.FormattedMessage(msg, kwargs), {key: kwargs[key] for key in self._log_arguments if key in kwargs}


class FormattingLogger:
    """
    Logging descriptor. Instantiates and returns logger by descriptor owner's class name.

    :type logger: NewStyleFormattingLoggerAdapter
    :rtype logging.Logger
    """

    def __init__(self):
        self.logger = None

    def __get__(self, instance, owner):
        if self.logger is None:
            self.logger = NewStyleFormattingLoggerAdapter(logging.getLogger(owner.__name__))

        return self.logger


class PageObject:
    """
    Base class for all page objects.
    Page object is part of page and can be whole page.
    PageObject and Element classes can be used only together.

    :type driver: WebDriver
    :type log: logging.Logger
    :type context: WebDriver|WebElement is used to look for elements of object in scope of context.
    context decreases complexity of selectors. It is not used directly in class but used by Element
    descriptor
    """

    log = FormattingLogger()

    def __init__(self, driver: WebDriver, timeout: int=DEFAULT_TIMEOUT):
        self.driver = driver
        self.timeout = timeout
        self._set_context(driver)
        self.globalWait = WebDriverWait(driver, self.timeout)

    def _set_context(self, context):
        """
        :type context: WebDriver|WebElement
        :return:
        """
        self.context = context
        self.wait = WebDriverWait(context, self.timeout)

    def title_contains(self, text: str):
        self.globalWait.until(ec.title_contains(text))

    def url_contains(self, text):
        self.globalWait.until(lambda driver: text in driver.current_url)

    @property
    def alert(self) -> Alert:
        return self.globalWait.until(ec.alert_is_present())

    def switch_to_frame(self, frame):
        """
        :param frame: str|int
        :rtype:
        """
        self.globalWait.until(ec.frame_to_be_available_and_switch_to_it(frame))

    @staticmethod
    def _element_is_visible(element, visible=True):
        try:
            return element.is_displayed() == visible
        except (NoSuchElementException, StaleElementReferenceException):
            # no element for sure
            return not visible

    def wait_element(self, element: WebElement):
        pass

    def wait_visible(self, element: WebElement):
        self.wait.until(lambda driver: self._element_is_visible(element))

    def wait_invisible(self, element: WebElement):
        self.wait.until(lambda driver: self._element_is_visible(element, False))

    def wait_disabled(self, element: WebElement):
        self.wait.until(lambda driver: not element.is_enabled())

    def wait_clickable(self, element: WebElement):
        self.wait_visible(element)
        self.wait.until(lambda driver: element.is_enabled())


class ExtendedWebElement:
    """
    Class adds additional functionality to WebElement.
    So we can use ExtendedWebElement instead of WebElement
    using delegation to WebElement via __getattr__ method.

    :type element: WebElement
    """

    def __init__(self, element: WebElement):
        self.element = element

    def __getattr__(self, item):
        return self.element.__getattribute__(item)

    def set_style(self, name, value):
        self.element.parent.execute_script('arguments[0].style.' + name + ' = "' + value + '"', self.element)

    def hide(self):
        self.set_style('display', 'none')

    def show(self):
        self.set_style('display', 'block')


class Element:
    """
    This is descriptor https://docs.python.org/3.4/howto/descriptor.html
    Element descriptor is used to incapsulate logic to get WebElement
    and to set value to WebElement based on its type and other different conditions.

    It is called Element because of its usage:
        class ParticularPageObject(PageObject):
            element = Element((By.*, 'selector string'))

            def method(self):
                self.element.click() # we just get element
                self.element = 'some value' # we set value of element, real logic depends on type of element

    :type log: logging.Logger
    :type selector: (str, str)
    """

    log = FormattingLogger()

    def __init__(self, selector: tuple):
        """
        :param selector: (str, str)
        :rtype:
        """
        self.selector = selector

    def _get_element(self, page: PageObject):
        return page.context.find_element(*self.selector)

    def __get__(self, page: PageObject, owner) -> WebElement:
        return self._get_element(page)

    def __set__(self, page: PageObject, value):
        element = self._get_element(page)

        if element.tag_name == 'select':
            Select(element).select_by_visible_text(value)
        elif element.get_attribute('type') == 'checkbox':
            if value == 'Yes' and not element.is_selected():
                element.send_keys(Keys.SPACE)
            elif value == 'No' and element.is_selected():
                element.send_keys(Keys.SPACE)
        else:
            element.clear()
            element.send_keys(value)


class Clickable(Element):
    """
    Overrides Element's _get_element to wait element is clickable before return it
    Clickable elements are visible
    """

    def _get_element(self, page: PageObject):
        return page.wait.until(ec.element_to_be_clickable(self.selector))


class Visible(Element):
    """
    Overrides Element's _get_element to wait element is visible before return it
    """

    def _get_element(self, page: PageObject):
        page.wait.until(ec.visibility_of_element_located(self.selector))
        return super()._get_element(page)

