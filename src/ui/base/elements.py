from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as ec
from .pageobjects import FormattingLogger
from logging import Logger
from . import expected_conditions as ns_ec


class NsElement:
    """
    :type _element: WebElement
    :type _log: Logger
    """

    _element = None
    _log = FormattingLogger()

    def __init__(self, selector: tuple, wait: WebDriverWait):
        self.wait = wait
        self.selector = selector

    def get_element(self) -> WebElement:
        self.wait_present()
        return self._element

    def wait_present(self):
        self._element = self.wait.until(ec.presence_of_element_located(self.selector))
        return self

    def wait_invisible(self):
        self.wait.until(ec.invisibility_of_element_located(self.selector))
        return self

    def wait_visible(self):
        self.wait.until(ec.visibility_of_element_located(self.selector))
        return self

    def wait_clickable(self):
        self._element = self.wait.until(ec.element_to_be_clickable(self.selector))
        return self

    def wait_text(self, text: str):
        self.wait.until(ec.text_to_be_present_in_element(self.selector, text))
        return self

    def wait_text_in_attribute(self, attribute: str, text: str):
        self.wait.until(ns_ec.text_to_be_present_in_element_attribute(self.selector, attribute, text))
        return self

    def click(self):
        self.wait_clickable()
        self._element.click()
        return self

    def get_attribute(self, name: str) -> str:
        return self.get_element().get_attribute(name)

    def find_elements(self, selector: tuple) -> list:
        return self.get_element().find_elements(*selector)

class NsEditable(NsElement):

    def wait_editable(self):
        self._element = self.wait.until(ec.element_to_be_clickable(self.selector))
        return self

    def set_value(self, value: str):
        self.wait_editable()
        self._set_value(value)
        return self

    def _set_value(self, value: str):
        raise Exception('Not implemented')

class NsInputText(NsEditable):

    def get_value(self) -> str:
        return self.get_attribute('value')

    def append_value(self, value: str):
        self.wait_editable()
        self._element.send_keys(value)
        return self

    def _set_value(self, value: str):
        self._element.clear()
        self._element.send_keys(value)

class NsSelect(NsEditable):

    def _set_value(self, value: str):
        Select(self._element).select_by_visible_text(value)

class NsInputCheckbox(NsEditable):

    class Values:
        CHECKED = 'checked'
        UNCHECKED = 'unchecked'

    def _set_value(self, value: str):
        if value == self.Values.CHECKED and not self._element.is_selected():
            self._element.send_keys(Keys.SPACE)
        elif value == self.Values.UNCHECKED and self._element.is_selected():
            self._element.send_keys(Keys.SPACE)

    def check(self):
        self.set_value(self.Values.CHECKED)

    def uncheck(self):
        self.set_value(self.Values.UNCHECKED)
