from nose.tools import istest
import logging
from .pageobjects import FormattingLogger

from selenium.webdriver.remote.webdriver import WebDriver as RemoteWebDriver
from selenium.webdriver.firefox.webdriver import WebDriver as Firefox
from selenium.webdriver.chrome.webdriver import WebDriver as Chrome
from selenium.webdriver.ie.webdriver import WebDriver as Ie
from selenium.webdriver.opera.webdriver import WebDriver as Opera
from selenium.webdriver.safari.webdriver import WebDriver as Safari
from selenium.webdriver.phantomjs.webdriver import WebDriver as PhantomJS


@istest
class UITestCase:
    """
    :type log: logging.Logger
    :type driver: RemoteWebDriver
    """

    log = FormattingLogger()
    driver = None

    def __init__(self):
        logging.basicConfig(format='%(asctime)s [%(levelname)s] %(name)s: %(message)s',
                            datefmt='%Y/%m/%d %H:%M:%S',
                            level=logging.INFO)

    @classmethod
    def setUpClass(cls):
        cls.config = cls._get_config()
        cls.driver = cls.get_web_driver(cls.config['driver']['browser'])
        if 'window_size' in cls.config['driver']:
            cls.driver.set_window_size(**cls.config['driver']['window_size'])

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    @classmethod
    def get_web_driver(cls, browser):
        """
        Returns the WebDriver instance to be used. Defaults to `NeedleFirefox()`.
        Override this method if you'd like to control the logic for choosing
        the proper WebDriver instance.
        """
        browser_map = {
            'firefox': Firefox,
            'chrome': Chrome,
            'ie': Ie,
            'opera': Opera,
            'safari': Safari,
            'phantomjs': PhantomJS,
        }
        browser_class = browser_map.get(browser, Firefox)
        return browser_class()

    @staticmethod
    def _get_config():
        from testconfig import config
        return config
