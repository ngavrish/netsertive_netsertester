from logging import getLogger
from nose.tools import eq_
from pytz import timezone
from .pageobjects import NewStyleFormattingLoggerAdapter
from datetime import datetime, date
import subprocess
import paramiko


class LocalCommandExecutor:
    """
    Local command line executor
    """

    def execute(self, command) -> list:
        return subprocess.getoutput(command).splitlines(True)


class SshCommandExecutor:
    """
    Ssh command line executor
    """

    def __init__(self, host, username, password):
        self.ssh = paramiko.SSHClient()
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.ssh.connect(host, username=username, password=password)

    def execute(self, command) -> list:
        # exec_command returns tuple of stdin, stdout, stderr of the executing command
        stdin, stdout, stderr = self.ssh.exec_command(command)
        return stdout.readlines()


class LogError:
    """
    LogError parse error string to error datetime and error message
    """

    _DATE_TIME_FORMAT = '%Y-%m-%d %H:%M:%S'

    def __init__(self, err_str: str, err_timezone):
        # parse date from error string like "ERROR - Y-m-d H:i:s --> message"
        err_datetime_str, err_msg = err_str[len('ERROR - '):].split(' --> ', 1)
        self.message = err_msg.strip()
        self.datetime = datetime.strptime(err_datetime_str, self._DATE_TIME_FORMAT)
        self.datetime = err_timezone.localize(self.datetime)

    def __str__(self):
        return '{datetime}: {message}'.format(**self.__dict__)


class LogCrawler:
    """
    LogCrawler gets list of error strings from log file,
    creates list of LogErrors and filter them
    """

    def __init__(self, command_executor, logs_path, tz_name='America/New_York'):
        self._logs_path = logs_path
        self.timezone = timezone(tz_name)
        self._command_executor = command_executor

    def get_errors_for_date(self, logs_date: date) -> list:
        grep_command = 'grep "ERROR - " /{logs_path}/log-{date}.php'.format(
            logs_path=self._logs_path.strip('/'),
            date=logs_date
        )

        return list(map(
            lambda err_str: LogError(err_str, self.timezone),
            self._command_executor.execute(grep_command)
        ))

    def get_errors_for_interval(self, start: datetime, finish: datetime, whitelist=set()):
        return list(filter(
            lambda error: start < error.datetime < finish and error.message not in whitelist,
            self.get_errors_for_date(start.date())
        ))

class ErrorLogAssert:
    """
    ErrorLogAssert executes test function and
    then load log errors and assert there are not errors.
    """

    def __init__(self, object, fn, log_crawler, errors_whitelist):
        self._fn = fn
        self._object = object
        self._log_crawler = log_crawler
        self._errors_whitelist = errors_whitelist
        self.logger = NewStyleFormattingLoggerAdapter(getLogger('check_logs:'+ str(fn)))

    def __call__(self, *args, **kwargs):
        start = datetime.now(self._log_crawler.timezone)
        self.logger.info('start = ' + str(start))

        result = self._fn(self._object, *args, **kwargs)

        finish = datetime.now(self._log_crawler.timezone)
        self.logger.info('finish = ' + str(finish))

        errors = self._log_crawler.get_errors_for_interval(start, finish, self._errors_whitelist)
        for error in errors:
            print("error=" + str(error))
        eq_(0, len(errors), 'Errors parsed')

        return result


class CheckLogs:
    """
    This is just factory which creates command executor,
    log crawler and error log asserter based on logcrawler config.
    """

    def __init__(self, fn):
        self.fn = fn

    def __get__(self, instance, owner):
        object = instance if instance else owner
        config = object.config['logcrawler']

        if config['local']:
            command_executor = LocalCommandExecutor()
        else:
            command_executor = SshCommandExecutor(
                config.get('remote-server-url', 'undefined'),
                config.get('remote-username', 'undefined'),
                config.get('remote-password', 'undefined')
            )

        log_crawler = LogCrawler(
            command_executor,
            config['logs-path'],
            config.get('timezone', 'America/New_York')
        )

        return ErrorLogAssert(
            object,
            self.fn,
            log_crawler,
            config.get('errors_whitelist', set())
        )
