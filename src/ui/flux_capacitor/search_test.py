'''
Created on Dec 23, 2014

@author: Gavrish
'''
from .model.grid import Grid
from .model.pod import PodModel
from .model.client import Client

from ..generator_test import GeneratorTest, CheckLogsWebUID
from .model.flux_capacitor import FluxCapacitor


class Search_Test(GeneratorTest):
    """
    Test covers creating ad and adding to freshly created ads pod and venue and searching it
    """

    # GENERATOR
    @CheckLogsWebUID("Search Ad")
    def search_ad(self, ad, venue, pod, client_name, webuid):
        """
        Add Google Display Venue Test
        """
        self.driver = self.get_web_driver()
        self.amc_model = FluxCapacitor(driver=self.driver)
        self.client = Client(driver=self.driver, timeout=60)
        self.pod_model = PodModel(driver=self.driver)
        self.grid = Grid(driver=self.driver)
        self.driver.get(self.web_ui_credentials.url)
        self.driver.maximize_window()
        self.amc_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)

        # SET UIDs
        venue_id = venue["VENUE_NAME"] + webuid
        pod_id = "pod" + webuid
        ad["Title"] += webuid[-5:]

        self.amc_model.goto_tab('Client Center')
        self.grid.search_in_client_name_column(client_name)
        self.grid.open_client(client_name)
        self.client.add_venue(venue, venue_id=venue_id)

        self.amc_model.goto_tab('Pod Center')
        self.pod_model.create_pod(pod=pod, name=pod_id)

        self.amc_model.goto_tab('Client Center')
        self.grid.search_in_client_name_column(client_name)
        self.grid.open_client(client_name)

        self.client.map_pod_on_venue(venue_id=venue_id, pod_id=pod_id,
                                     pod_network=pod['network'])
        self.client.create_ad(ad, venue_id=venue_id, pod_id=pod_id)

        self.amc_model.search_by_key(key=ad['Title'])
        self.client.validate_ad_search(ad_data=ad)

        self.amc_model.logout()

     # GENERATOR
    @CheckLogsWebUID("Search Pod")
    def search_pod(self, venue, pod, client_name, webuid):
        """
        Pod Search Test
        """
        self.driver = self.get_web_driver()
        self.amc_model = FluxCapacitor(driver=self.driver)
        self.client = Client(driver=self.driver)
        self.pod_model = PodModel(driver=self.driver)
        self.grid = Grid(driver=self.driver)
        self.driver.get(self.web_ui_credentials.url)
        self.driver.maximize_window()
        self.amc_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)

        # SET UIDs
        venue_id = venue["VENUE_NAME"] + webuid
        pod_id = "pod_" + webuid

        print("venue_id = " + str(venue_id))

        self.amc_model.goto_tab('Client Center')
        self.grid.search_in_client_name_column(client_name)
        self.grid.open_client(client_name)
        self.client.add_venue(venue, venue_id=venue_id)
        self.amc_model.goto_tab('Pod Center')
        self.pod_model.create_pod(pod=pod, name=pod_id)

        self.amc_model.search_by_key(key=pod_id)
        self.pod_model.validate_pod_search(pod_id=pod_id)
        self.amc_model.logout()

    # GENERATOR
    @CheckLogsWebUID("Search Venue")
    def search_venue(self, venue, client_name, webuid):
        """
        Search Venue
        """
        self.driver = self.get_web_driver()
        self.amc_model = FluxCapacitor(driver=self.driver)
        self.amc_model = FluxCapacitor(driver=self.driver)
        self.client = Client(driver=self.driver)
        self.client = Client(driver=self.driver)
        self.grid = Grid(driver=self.driver)
        self.driver.maximize_window()
        self.driver.get(self.web_ui_credentials.url)
        self.amc_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)

        # SET UIDs
        venue_id = venue["VENUE_NAME"] + webuid

        self.amc_model.goto_tab('Client Center')
        self.grid.search_in_client_name_column(client_name)
        self.grid.open_client(client_name)

        self.client.add_venue(venue, venue_id=venue_id)
        self.amc_model.search_by_key(key=venue_id)
        self.client.validate_venue_search(venue_id=venue_id)
        self.amc_model.logout()

    # TEST
    def test(self):
        yield self.search_ad, \
            self.get_json_data("ad_data.json")['GOOGLE']['DISPLAY'], \
            self.get_json_data("venues.json")['GOOGLE_DISPLAY'], \
            self.get_json_data("pod_data.json")["POD_INFO_DISPLAY"]["pod_1"], \
            self.get_json_data("amc_data.json")['CLIENT_4VENUES_DETAILS']['name_pref']

        yield self.search_ad, \
            self.get_json_data("ad_data.json")['GOOGLE']['SEARCH'], \
            self.get_json_data("venues.json")['GOOGLE_SEARCH'], \
            self.get_json_data("pod_data.json")["POD_INFO_SEARCH"]["pod_1"], \
            self.get_json_data("amc_data.json")['CLIENT_4VENUES_DETAILS']['name_pref']

        yield self.search_pod, \
            self.get_json_data("venues.json")['GOOGLE_DISPLAY'], \
            self.get_json_data("pod_data.json")["POD_INFO_DISPLAY"]["pod_1"]

        yield self.search_venue, \
            self.get_json_data("venues.json")['GOOGLE_DISPLAY'],\
            self.get_json_data("amc_data.json")['CLIENT_4VENUES_DETAILS']['name_pref']