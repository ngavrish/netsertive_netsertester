from ..generator_test import GeneratorTest, CheckLogsWebUID
from .model.flux_capacitor import FluxCapacitor
from .model.program_center.program_center import ProgramCenter


class SponsoredCampaign_Test(GeneratorTest):
    """
    Test covers Creating different sponsored campaigns
    """

    # GENERATOR
    @CheckLogsWebUID("Sponsored Campaign")
    def sponsored_campaign(self, campaign_data, webuid):
        """
        create sponsored campaign
        """
        self.driver = self.get_web_driver()
        self.amc_model = FluxCapacitor(driver=self.driver)
        self.program_center = ProgramCenter(driver=self.driver)
        self.driver.maximize_window()

        campaign_data['name'] += webuid
        self.driver.get(self.web_ui_credentials.url)
        self.amc_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)
        self.amc_model.goto_tab("Program Center")
        self.program_center.goto_sponsored_campaign_wizard()
        self.program_center.set_sponsored_campaign_basic(campaign_data)
        self.amc_model.goto_tab("Sponsored Campaign Info")
        self.program_center.validate_sponsored_campaign_basic(campaign_data)
        self.amc_model.goto_tab("Program Center")
        self.program_center.grid.search_sponsored_campaign_by_name(campaign_data['name'])

    # TEST
    def test(self):
        yield self.sponsored_campaign, self.get_json_data("sponsored_campaign.json")['display']
        yield self.sponsored_campaign, self.get_json_data("sponsored_campaign.json")['email']
        yield self.sponsored_campaign, self.get_json_data("sponsored_campaign.json")['mail']
        yield self.sponsored_campaign, self.get_json_data("sponsored_campaign.json")['mobile']
        yield self.sponsored_campaign, self.get_json_data("sponsored_campaign.json")['radio']
        yield self.sponsored_campaign, self.get_json_data("sponsored_campaign.json")['search']
        yield self.sponsored_campaign, self.get_json_data("sponsored_campaign.json")['social']
        yield self.sponsored_campaign, self.get_json_data("sponsored_campaign.json")['tv']
        yield self.sponsored_campaign, self.get_json_data("sponsored_campaign.json")['youtube']