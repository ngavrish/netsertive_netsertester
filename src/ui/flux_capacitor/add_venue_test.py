'''
Created on Dec 23, 2014

@author: Gavrish
'''

from .model.grid import Grid
from .model.client import Client
from ..generator_test import GeneratorTest, CheckLogsWebUID
from .model.flux_capacitor import FluxCapacitor


class AddVenue_Test(GeneratorTest):
    """
    Test covers editing Existing clients from AMC
    """

    # GENERATOR
    @CheckLogsWebUID("Add Venue")
    def add_venue(self, venue, client_name, webuid):
        """
        Add Google Display Venue Test
        """
        self.driver = self.get_web_driver()
        self.amc_model = FluxCapacitor(driver=self.driver)
        self.client = Client(driver=self.driver)
        self.grid = Grid(driver=self.driver)
        self.driver.get(self.web_ui_credentials.url)
        self.driver.maximize_window()
        self.amc_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)

        venue_id = venue["VENUE_NAME"] + webuid
        campaign_name = venue_id + venue["CAMPAIGN_NAME"]

        self.amc_model.goto_tab('Client Center')
        self.amc_model.goto_client(client_name)
        self.client.add_venue(venue_data=venue, venue_id=venue_id)

        self.amc_model.goto_tab('Client Center')
        self.amc_model.goto_client(client_name)
        self.amc_model.goto_venue(campaign_name)

    # TEST
    def bright_roll_video_test(self):
        yield self.add_venue, \
            self.get_json_data("venues.json")['BRIGHTROLL_VIDEO'],\
            self.get_json_data("amc_data.json")['CLIENT_4VENUES_DETAILS']['name_pref']

    def google_video_test(self):
        yield self.add_venue,\
            self.get_json_data("venues.json")['GOOGLE_VIDEO'],\
            self.get_json_data("amc_data.json")['CLIENT_4VENUES_DETAILS']['name_pref']

    def google_search_test(self):
        yield self.add_venue, \
            self.get_json_data("venues.json")['GOOGLE_SEARCH'],\
            self.get_json_data("amc_data.json")['CLIENT_4VENUES_DETAILS']['name_pref']

    def bing_search_test(self):
        yield self.add_venue, \
            self.get_json_data("venues.json")['BING_SEARCH'],\
            self.get_json_data("amc_data.json")['CLIENT_4VENUES_DETAILS']['name_pref']

    def google_display_test(self):
        yield self.add_venue, \
            self.get_json_data("venues.json")['GOOGLE_DISPLAY'],\
            self.get_json_data("amc_data.json")['CLIENT_4VENUES_DETAILS']['name_pref']

    def facebook_social_test(self):
        yield self.add_venue, \
            self.get_json_data("venues.json")['FACEBOOK_SOCIAL'],\
            self.get_json_data("amc_data.json")['CLIENT_4VENUES_DETAILS']['name_pref']