import time
from .model.client import Client
from ..generator_test import GeneratorTest, CheckLogsWebUID
from .model.grid import Grid


class EditExistingClientDetails_Test(GeneratorTest):
    """
    Test covers editing Existing clients from AMC
    """
    def __init__(self):
        super().__init__()
        self.layout_model = None
        self.amc_data = self.get_json_data("amc_data.json")

    @CheckLogsWebUID("Client Details and History")
    def client_info_and_history(self, webuid):
        """
        Edit Existing Client Info with History Validation Test
        """
        self.amc_data['CLIENT_DETAILS']['changed_client_info']['ClientName'] += webuid
        self.amc_data['CLIENT_DETAILS']['changed_client_info']['CompanyName'] += webuid
        self.amc_data['CLIENT_DETAILS']['default_client_info']['CompanyName'] += webuid

        self.driver = self.get_web_driver()
        self.client_model = Client(driver=self.driver)
        self.grid = Grid(driver=self.driver)
        self.driver.get(self.web_ui_credentials.url)
        self.driver.maximize_window()

        self.client_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)
        self.client_model.goto_tab('Client Center')
        self.grid.search_in_client_name_column(self.amc_data['CLIENT_DETAILS']['default_client_info']['ClientName'])
        self.grid.open_client(self.amc_data['CLIENT_DETAILS']['default_client_info']['ClientName'])
        self.client_model.goto_tab('Client Info')
        self.client_model.open_edit_details4client(self.amc_data['CLIENT_DETAILS']['default_client_info']['ClientName'])
        self.client_model.goto_tab('Basic Client Information')
        self.client_model.set_client_info(self.amc_data['CLIENT_DETAILS']['changed_client_info'])

        self.client_model.goto_tab('Platform')
        if self.client_model.wait_until_clickable_css("input[name='gid']").get_attribute("value") == "":
            self.client_model.set_input_value("input[name='gid']", str(int(time.time())))

        self.client_model.submit_update_client_info()
        self.client_model.open_edit_details4client(self.amc_data['CLIENT_DETAILS']['changed_client_info']['ClientName'])

        self.client_model.goto_tab('Basic Client Information')
        self.client_model.validate_client_info(self.amc_data['CLIENT_DETAILS']['changed_client_info'])

        self.client_model.goto_tab('Basic Client Information')
        self.client_model.set_client_info(self.amc_data['CLIENT_DETAILS']['default_client_info'])

        self.client_model.submit_update_client_info()

        self.client_model.open_edit_details4client(self.amc_data['CLIENT_DETAILS']['default_client_info']['ClientName'])
        self.client_model.goto_tab('Basic Client Information')
        self.client_model.validate_client_info(self.amc_data['CLIENT_DETAILS']['default_client_info'])

        # HISTORY VALIDATION IS BLOCKED
        # http://jira.netsertive.local/browse/NPI-8702

        # self.client_model.goto_tab('Basic Client Information')
        # self.client_model.set_input_value("input[name='companyName']",
        #                                   self.amc_data['CLIENT_DETAILS']['changed_client_info'][
        #                                       'CompanyName'] + webuid)
        # self.client_model.submit_update_client_info()

        # self.client_model.open_history_details4client()
        #
        # self.client_model.validate_history_row(rownum=2,
        #                                        row2validate=[self.web_ui_credentials.fullname,
        #                                                      "Update",
        #                                                      self.amc_data['CLIENT_DETAILS']['default_client_info'][
        #                                                          'CompanyName'],
        #                                                      self.amc_data['CLIENT_DETAILS']['changed_client_info'][
        #                                                          'CompanyName'] + webuid])
        #
        # self.client_model.open_edit_details4client(self.amc_data['CLIENT_DETAILS']['default_client_info']['ClientName'])
        # self.client_model.goto_tab('Basic Client Information')
        # self.client_model.set_input_value("input[name='companyName']",
        #                                   self.amc_data['CLIENT_DETAILS']['default_client_info']['CompanyName'])
        # self.client_model.submit_update_client_info()
        #
        # self.client_model.open_history_details4client()
        # self.client_model.validate_history_row(rownum=2,
        #                                        row2validate=[self.web_ui_credentials.fullname,
        #                                                      "Update",
        #                                                      self.amc_data['CLIENT_DETAILS']['changed_client_info'][
        #                                                          'CompanyName'] + webuid,
        #                                                      self.amc_data['CLIENT_DETAILS']['default_client_info'][
        #                                                          'CompanyName']])
        # self.client_model.logout()

    def client_info_and_history_test(self):
        yield self.client_info_and_history