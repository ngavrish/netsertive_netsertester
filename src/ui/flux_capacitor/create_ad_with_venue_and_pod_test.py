'''
Created on Dec 23, 2014

@author: Gavrish
'''

from .model.grid import Grid
from .model.pod import PodModel
from .model.client import Client

from ..generator_test import GeneratorTest, CheckLogsWebUID
from .model.flux_capacitor import FluxCapacitor


class CreateAddAdWithVenueAndPod_Test(GeneratorTest):
    """
    Test covers creating ad and adding to freshly created ads pod and venue 
    """

    # GENERATOR
    @CheckLogsWebUID("Ad Creation")
    def ad_creation(self, client, ad, clone_ad, venue, pod, webuid):
        """
        Create Ad with new Venue and new Pod
        """
        self.driver = self.get_web_driver()
        self.amc_model = FluxCapacitor(driver=self.driver)
        self.client = Client(driver=self.driver)
        self.pod_model = PodModel(driver=self.driver)
        self.grid = Grid(driver=self.driver)
        self.driver.maximize_window()
        self.driver.get(self.web_ui_credentials.url)

        ad["config"] = self.config
        clone_ad["config"] = self.config
        venue_id = "venue_" + webuid
        pod_id = "pod_" + webuid

        self.amc_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)
        self.amc_model.goto_tab('Client Center')
        self.amc_model.goto_client(client)
        self.client.add_venue(venue, venue_id=venue_id)
        self.amc_model.goto_tab('Pod Center')
        self.pod_model.create_pod(pod=pod, name=pod_id)
        self.amc_model.goto_tab('Client Center')
        self.amc_model.goto_client(client)

        self.client.map_pod_on_venue(venue_id=venue_id, pod_id=pod_id,
                                     pod_network=pod['network'])
        self.client.create_ad(ad, venue_id=venue_id, pod_id=pod_id)
        self.client.clone_ad(ad2clone=ad, clone_ad_data=clone_ad)

        if ad.get("Type", "") == 'Image':
            self.grid.find_image_ad(ad)
            self.grid.find_image_ad(clone_ad)
        elif ad.get("Image", "") == "Text":
            self.grid.find_text_ad(ad)
            self.grid.find_text_ad(clone_ad)

        self.amc_model.goto_tab('Pods')
        self.grid.find_pod(pod_id)
        self.grid.open_pod(pod_id)
        if ad.get("Type", "") == 'Image':
            self.grid.find_image_ad(ad)
            self.grid.find_image_ad(clone_ad)
        elif ad.get("Image", "") == "Text":
            self.grid.find_text_ad(ad)
            self.grid.find_text_ad(clone_ad)

        self.amc_model.goto_tab('Ads')
        self.client.delete_ad(ad)
        self.client.delete_ad(clone_ad)
        self.client.wait_until_not_visible_xpath(
            self.client.get_pods_with_title(
                self.client.get_pod_title_assembled_from_data(clone_ad)))
        self.amc_model.goto_tab('Client Center')
        self.amc_model.goto_client(client)
        self.amc_model.goto_tab('Client Info')
        self.amc_model.wait4loader2disappear()
        self.amc_model.goto_tab('Budget')
        self.client.validate_venue_in_budget(venue_id)

    # TEST
    def test(self):
        yield self.ad_creation, \
            self.get_json_data("amc_data.json")['CLIENT_4VENUES_DETAILS']['name_pref'],\
            self.get_json_data("ad_data.json")['GOOGLE']['SEARCH'], \
            self.get_json_data("ad_data.json")['GOOGLE']['TEXT_CLONE'], \
            self.get_json_data("venues.json")['GOOGLE_SEARCH'], \
            self.get_json_data("pod_data.json")["POD_INFO_SEARCH"]["pod_1"]
        # yield self.ad_creation, self.amc_data['CLIENT_4VENUES_DETAILS']['name_pref'], \
        #       self.ad_data['IMAGE']['DISPLAY'], \
        #       self.ad_data['IMAGE']['CLONE'], \
        #       self.venues_data['GOOGLE_DISPLAY'], self.pod_data["POD_INFO_DISPLAY"]["pod_1"]
        yield self.ad_creation, \
              self.get_json_data("amc_data.json")['CLIENT_4VENUES_DETAILS']['name_pref'], \
              self.get_json_data("ad_data.json")['BING']['SEARCH'], \
              self.get_json_data("ad_data.json")['BING']['TEXT_CLONE'], \
              self.get_json_data("venues.json")['BING_SEARCH'], \
              self.get_json_data("pod_data.json")["BING_SEARCH"]["pod_1"]