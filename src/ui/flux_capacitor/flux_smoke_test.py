"""
Created on Apr 14, 2015

@author = tclark@netsertive.com
"""
import time
from nose.tools import eq_
from selenium.common.exceptions import TimeoutException
from ..generator_test import GeneratorTest, CheckLogs
from .model.flux_capacitor import FluxCapacitor
from .model.grid import Grid
from .model.client import Client
from .model.pod import PodModel
from .model.program_center.program_center import ProgramCenter


class FluxSmoke_Test(GeneratorTest):
    """
    Covers Smoke Testing of Client Center Default grid - sorting asc and desc
    """

    # GENERATOR
    @CheckLogs("Client Grid Sorting Smoke")
    def client_grid_sorting_smoke(self):
        """
        AMC Client Grid Sorting Smoke test
        1) Accesses Client Home in Client Center
        2) Sorts each default display Column asc and desc
        Searches AMC logs during teardown
        """
        self.driver = self.get_web_driver()
        self.amc_model = FluxCapacitor(driver=self.driver, timeout=self.web_ui_credentials.web_timeout)
        self.grid = Grid(driver=self.driver)
        self.driver.get(self.web_ui_credentials.url)
        self.driver.maximize_window()

        self.amc_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)
        self.amc_model.goto_tab('Client Center')
        self.amc_model.wait4loader2disappear()

        self.grid.sort_column('Status')
        self.grid.sort_column('Client Name')
        self.grid.sort_column('Owner')
        self.grid.sort_column('Total Clicks')
        self.grid.sort_column('Total Impressions')
        self.grid.sort_column('Total Monthly DMF Budget')
        self.grid.sort_column('Estimated Digital Media Spend')
        self.grid.sort_column('Active DMF Kicker Total')
        self.grid.sort_column('Budget Trend %')
        self.grid.sort_column('Total Cost')
        self.grid.sort_column('Total DMF Cost')
        self.grid.sort_column('Total CTR')
        self.grid.sort_column('Total Avg CPC')
        self.grid.sort_column('Search Avg Quality Score')
        self.grid.sort_column('Imp Share')

        self.amc_model.logout()

    # GENERATOR
    @CheckLogs("Client Page Smoke")
    def client_page_smoke(self, client_name):
        """
        AMC Client Page Smoke test
        1) Accesses Client Pages in AMC
        2) Verifies title page for each tab matches expected
        3) Once all pages are loaded and with verified title, logcrawler verifies no errors are thrown in
        AMC logs during teardown
        """
        self.driver = self.get_web_driver()
        self.amc_model = FluxCapacitor(driver=self.driver)
        self.client_model = Client(driver=self.driver)
        self.grid = Grid(driver=self.driver)
        self.driver.maximize_window()
        self.driver.get(self.web_ui_credentials.url)

        self.amc_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)
        self.amc_model.goto_tab('Client Center')
        self.grid.search_in_client_name_column(client_name)
        self.grid.open_client(client_name)

        # Access each Client Info sub-tab
        self.client_model.goto_tab('Client Info')
        eq_(self.driver.title, "View Client Clients")

        # Access Platform and all sub-tabs
        self.client_model.goto_tab('Platform')
        self.client_model.goto_tab('Bing')
        self.client_model.goto_tab('Call Tracking')
        self.client_model.goto_tab('Business Listing')
        # http://jira.netsertive.local/browse/NPI-8135
        # self.client_model.goto_tab('Email Marketing')
        self.client_model.goto_tab('FTP Reports')
        self.client_model.goto_tab('Netsuite')
        self.client_model.goto_tab('Landing Page')
        self.client_model.goto_tab('Assets')
        self.client_model.goto_tab('GTM')
        self.client_model.goto_tab('Google')

        self.client_model.goto_tab('Basic Client Information')
        eq_(self.driver.title, "View Client Clients")
        self.client_model.goto_tab('Contacts')
        eq_(self.driver.title, "View Client Clients")
        self.client_model.goto_tab('Addresses')
        eq_(self.driver.title, "View Client Clients")
        self.client_model.goto_tab('Notes')
        eq_(self.driver.title, "View Client Clients")
        self.client_model.goto_tab('Budget')
        eq_(self.driver.title, "View Client Clients")
        # Access All Other Client Tabs
        self.client_model.goto_tab('Pods')
        eq_(self.driver.title, "View Client Pods")
        self.client_model.goto_tab('Ads')
        eq_(self.driver.title, "View Client Ads")
        self.client_model.goto_tab('Keywords')
        eq_(self.driver.title, "View Client Keywords")
        self.client_model.goto_tab('Placements')
        eq_(self.driver.title, "View Client Placements")
        self.client_model.goto_tab('Dimensions')
        eq_(self.driver.title, "Netsertive Dashboard")
        self.client_model.goto_tab('Ad Extensions')
        eq_(self.driver.title, "View Client Adextensions")

        self.amc_model.logout()

    # GENERATOR
    @CheckLogs("Date Picker Smoke")
    def date_picker_smoke(self):
        """
        AMC Date Picker Smoke test
        1) Navigate to Client Center in AMC
        2) Click Date Picker
        3) Cycle through each possible selection (waiting for grid to load between each)
        4) Once each selection has been loaded, reset date to today
        5) Logout of AMC
        6) logcrawler verifies no errors are thrown in AMC logs during teardown
        """
        self.driver = self.get_web_driver()
        amc_model = FluxCapacitor(self.driver)
        self.driver.maximize_window()
        self.driver.get(self.web_ui_credentials.url)

        amc_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)
        amc_model.goto_tab('Client Center')

        #todo - determine approach/needs for page validation
        # amc_model.update_date_picker('//*[@id="Yesterday"]')
        # amc_model.update_date_picker('//*[@id="Day Before Yesterday"]')
        # amc_model.update_date_picker('//*[@id="This week(Sun-Yesterday)"]')
        # amc_model.update_date_picker('//*[@id="This week(Mon-Yesterday)"]')
        # amc_model.update_date_picker('//*[@id="Last 7 days (to Yesterday)"]')
        # amc_model.update_date_picker('//*[@id="Last week(Sun-Sat)"]')
        # amc_model.update_date_picker('//*[@id="Last week(Mon-Sun)"]')
        # amc_model.update_date_picker('//*[@id="Last business week(Mon-Fri)"]')
        # amc_model.update_date_picker('//*[@id="Last 30 days (to Yesterday)"]')
        # amc_model.update_date_picker('//*[@id="This Month (to Yesterday)"]')
        # amc_model.update_date_picker('//*[@id="Last Month"]')
        amc_model.update_date_picker_custom('2016-01-01', '2016-01-31')
        amc_model.update_date_picker('//*[@id="Today"]')

        amc_model.logout()

    # GENERATOR
    @CheckLogs("Pod Page Smoke")
    def pod_page_smoke(self, pod_name):
        """
        AMC Pod Page Smoke test
        1) Accesses Pod Pages in AMC
        2) Verifies title page for each tab matches expected
        3) Once all pages are loaded and with verified title, logcrawler verifies no errors are thrown in
        AMC logs during teardown
        """
        self.driver = self.get_web_driver()
        self.amc_model = FluxCapacitor(driver=self.driver)
        self.client_model = Client(driver=self.driver)
        self.pod_model = PodModel(driver=self.driver)
        self.driver.maximize_window()
        self.driver.get(self.web_ui_credentials.url)

        self.amc_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)
        self.amc_model.goto_tab('Pod Center')
        self.pod_model.search_in_pod_name_column(pod_name)
        self.pod_model.open_pod_from_grid(pod_name)

        #Access each Pod Center tab - Validate Page Title
        self.client_model.goto_tab('Ads')
        eq_(self.driver.title, "View All Pod Ads")
        self.client_model.goto_tab('Keywords')
        eq_(self.driver.title, "View All Pod Keywords")
        self.client_model.goto_tab('Placements')
        eq_(self.driver.title, "View All Pod Placements")
        self.client_model.goto_tab('Searched Terms')
        eq_(self.driver.title, "View All Pod Searchterms")
        self.client_model.goto_tab('Pod Info')
        eq_(self.driver.title, "View Pod Info")
        self.client_model.goto_tab('Pod History')
        eq_(self.driver.title, "View Pod History")
        self.client_model.goto_tab('Venues')
        eq_(self.driver.title, "View All Pod Venues")

        self.amc_model.logout()

    # GENERATOR
    @CheckLogs("Program Center Smoke")
    def program_center_smoke(self):
        """
        AMC Program Center Smoke test
        1) Accesses Program Center Pages in AMC
        2) Once all pages are loaded and with verified title, logcrawler verifies no errors are thrown in
        AMC logs during teardown
        """
        self.driver = self.get_web_driver()
        self.amc_model = FluxCapacitor(driver=self.driver)
        self.pc_model = ProgramCenter(driver=self.driver)
        self.client_model = Client(driver=self.driver)
        self.driver.maximize_window()
        self.driver.get(self.web_ui_credentials.url)
        self.amc_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)

        self.amc_model.goto_tab('Program Center')
        self.amc_model.goto_tab('Sponsors')
        self.pc_model.create_sponsor_button.click()
        self.pc_model.cancel_create_sponsor_button.click()
        self.pc_model.grid.search_sponsor_by_name('Electrolux')
        self.pc_model.grid.open_sponsor('Electrolux')
        self.amc_model.goto_tab('Sponsor Network')
        self.pc_model.manage_sponsor_network()
        self.pc_model.cancel_manage_sponsor_network.click()
        self.pc_model.bulk_add_clients()
        self.pc_model.bulk_add_clients_cancel()
        self.pc_model.apply_labels()
        self.pc_model.filter_labels()
        self.amc_model.goto_tab('Basic Sponsor Information')
        self.pc_model.edit_sponsor()
        self.pc_model.edit_sponsor_network_labels()
        self.pc_model.cancel_sponsor_network_labels()
        self.pc_model.cancel_create_sponsor_button.click()

        self.amc_model.goto_tab('Program Center')
        self.pc_model.search_in_sponsored_campaign_column('Electrolux')
        self.pc_model.open_sponsored_campaign('Electrolux')
        self.amc_model.goto_tab('Subscribers')
        # todo uncomment and fix below
        # self.pc_model.add_sponsored_campaign_subscribers()
        # self.pc_model.cancel_sponsored_campaign_subscribers()
        self.amc_model.goto_tab('Sponsored Campaign Info')
        self.pc_model.edit_sponsored_campaign_details()
        self.pc_model.cancel_sponsored_campaign_details()
        self.pc_model.search_in_sponsored_campaign_column('Electrolux')
        self.pc_model.open_sponsored_campaign('Electrolux')
        self.amc_model.goto_tab('Sponsored Campaign Info')
        self.amc_model.goto_tab('Pods')
        self.pc_model.edit_sponsored_campaign_pods()
        self.pc_model.cancel_sponsored_campaign_pods()
        self.amc_model.logout()

    # GENERATOR
    @CheckLogs("Reports Page Smoke")
    def reports_page_smoke(self, links_list):
        """
        AMC Reports & Tools Page Smoke test
        1) Accesses Reports & Tools Pages in AMC
        2) Verifies title page for each page/tab matches expected
        3) Once all pages are loaded and with verified title, logcrawler verifies no errors are thrown in
        AMC logs during teardown
        """
        self.driver = self.get_web_driver()
        self.amc_model = FluxCapacitor(driver=self.driver)
        self.client_model = Client(driver=self.driver)
        self.driver.maximize_window()
        self.driver.get(self.web_ui_credentials.url)

        self.amc_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)
        for link in links_list:
            print("loading " + link['TITLE'])
            self.driver.get(self.web_ui_credentials.url + link['URL'])
            # wait until title gets it's value
            try:
                self.amc_model.wait_until_title_contains(link['TITLE'])
            except TimeoutException:
                self.amc_model.wait_until_title_contains(link['TITLE'])

        self.amc_model.logout()

    # TESTS
    def grid_sorting_smoke_test(self):
        yield self.client_grid_sorting_smoke

    def client_page_smoke_test(self):
        yield self.client_page_smoke, self.get_json_data("amc_data.json")['CLIENT_4VENUES_DETAILS']['name_pref']

    def date_picker_smoke_test(self):
        yield self.date_picker_smoke

    def pod_page_smoke_test(self):
        yield self.pod_page_smoke, self.get_json_data("pod_data.json")['POD_SMOKE_SEARCH']['name']

    def program_center_smoke_test(self):
        yield self.program_center_smoke

    def reports_page_smoke_test(self):
        yield self.reports_page_smoke, self.get_json_data("reports_tools_data.json")['LINKS']
