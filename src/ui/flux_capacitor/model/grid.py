from nose.tools import ok_
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.select import Select
from .flux_capacitor import FluxCapacitor


class Grid(FluxCapacitor):
    """
    Object model for Flux Capacitor.
    General functionality
    """
    def __init__(self, driver, timeout=None):
        super().__init__(driver=driver)
        if timeout:
            self.wait = WebDriverWait(driver, timeout=timeout)

    def search_in_client_name_column(self, name):
        """
        Search for the client in AMC client centre
        """
        self.wait4loader2disappear()
        self.wait_until_clickable_css(".ui-pg-selbox")
        self.wait_until_present_css("#gs_client_name")
        ActionChains(self.driver).click(self.driver.find_element_by_css_selector("#gs_client_name")).\
            send_keys(name + "\n").send_keys(Keys.RETURN).perform()
        self.wait_until_clickable_css('td[aria-describedby*="client_name"] > a[title*="' + name + '"]')

    def validate_no_client_with_name(self, name):
        """
        Search for the client in AMC client centre
        """
        self.wait4loader2disappear()
        self.wait_until_clickable_css(".ui-pg-selbox")
        self.wait_until_present_css("#gs_client_name")
        ActionChains(self.driver).click(self.driver.find_element_by_css_selector("#gs_client_name")).\
            send_keys(name + "\n").send_keys(Keys.RETURN).perform()
        self.wait_until_exists_css("table[aria-labelledby='gbox_tab-client-mode_t'] > tbody")
        self.wait_until_not_exists_css('td[aria-describedby*="client_name"] > a[title*="' + name + '"]')

    def open_client(self, name):
        """
        Open client details from client centre.
        """

        self.wait_until_clickable_css("td[aria-describedby=\"client_grid_client_name\"] > a[title*=\"" + name + "\"]").click()
        self.wait_until_clickable_css("a[title*=\"" + name + "\"]")

    def status_select(self, status):
        """
        Select Status in Grid
        """
        self.wait_until_clickable_css("#gs_status")
        Select(self.wait_until_clickable_css("#gs_status")).select_by_visible_text(status)
        self.driver.execute_script("resubmitGrid()")
        self.wait4loader2disappear()

        if status == 'Active':
            status = 'active.png'
        elif status == 'Inactive':
            status = 'deleted.gif'

        self.wait_until_clickable_css('td[aria-describedby="client_grid_status"] > img[src$="' + status + '"]')
        self.wait_until_not_visible_css('td[aria-describedby="client_grid_status"] > img:not([src$="' + status + '"])')

    def get_platforms(self):
        return Select(self.driver.find_element_by_css_selector("#extraFilters")).options

    def get_clients(self):
        return self.driver.find_elements_by_css_selector("td[aria-describedby='client_grid_client_name']")

    def select_platform(self, platform):
        self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "#extraFilters")))
        Select(self.wait_until_clickable_css("#extraFilters")).select_by_visible_text(platform)
        self.driver.execute_script("resubmitGrid()")
        self.wait4loader2disappear()

    def sort_column(self, column):
        """
        Helper to click and sort a specified column asc and then desc
        """

        def sort_and_wait(xpath):
            self.wait.until(EC.element_to_be_clickable((By.XPATH, xpath))).click()
            self.wait4loader2disappear()
            self.wait.until(EC.element_to_be_clickable((By.XPATH, xpath))).click()
            self.wait4loader2disappear()

        if column == 'Status':
            sort_and_wait("//div[@id='jqgh_client_grid_status']")
        elif column == 'Client Name':
            sort_and_wait("//div[@id='jqgh_client_grid_client_name']")
        elif column == 'Owner':
            sort_and_wait("//div[@id='jqgh_client_grid_owner']")
        elif column == 'Total Clicks':
            sort_and_wait("//div[@id='jqgh_client_grid_clicks']")
        elif column == 'Total Impressions':
            sort_and_wait("//div[@id='jqgh_client_grid_impressions']")
        elif column == 'Total Monthly DMF Budget':
            sort_and_wait("//div[@id='jqgh_client_grid_total_dmf_budget']")
        elif column == 'Estimated Digital Media Spend':
            sort_and_wait("//div[@id='jqgh_client_grid_estimated_digital_media_spend']")
        elif column == 'Active DMF Kicker Total':
            sort_and_wait("//div[@id='jqgh_client_grid_dmf_budget_kickers']")
        elif column == 'Budget Trend %':
            sort_and_wait("//div[@id='jqgh_client_grid_budget_trend']")
        elif column == 'Total Cost':
            sort_and_wait("//div[@id='jqgh_client_grid_cost']")
        elif column == 'Total DMF Cost':
            sort_and_wait("//div[@id='jqgh_client_grid_dmf_cost']")
        elif column == 'Total CTR':
            sort_and_wait("//div[@id='jqgh_client_grid_ctr']")
        elif column == 'Total Avg CPC':
            sort_and_wait("//div[@id='jqgh_client_grid_avg_cpc']")
        elif column == 'Search Avg Quality Score':
            sort_and_wait("//div[@id='jqgh_client_grid_search_average_quality_score']")
        elif column == 'Imp Share':
            sort_and_wait("//div[@id='jqgh_client_grid_impression_share_search']")


    def business_segment_select(self, segment):
        """
        Select Business Segment in grid
        """
        pass

    def add_column(self, column):
        """
        Add column to grid
        """
        pass

    def validate_status(self, client, status):
        """
        Validate status for specific client in grid
        """
        pass

    def validate_status_through_grid(self, status):
        """
        Validate same status for all clients in grid
        """
        pass

    def find_venue(self, venue_id):
        self.wait4loader2disappear()
        self.wait_until_presence_of_all_css("table#venue_grid0[role='grid'] > tbody > tr > td[role='gridcell']")
        self.set_input_value("input#gs_venue_name", venue_id + "\n\n")
        self.wait4loader2disappear()
        ActionChains(self.driver).click(self.wait_until_clickable_css("input#gs_venue_name")).send_keys("\n").perform()
        self.wait_until_visible_xpath("//td[(@aria-describedby='venue_grid0_venue_name') and contains(@title, '" + venue_id + "')]/a")

    def open_venue(self, venue_id):
        self.wait_until_clickable_xpath("//td[(@aria-describedby='venue_grid0_venue_name') and contains(@title, '" +
                                        venue_id + "')]/a").click()
        self.wait_until_visible_css("h4 a[title*='" + venue_id + "']")
        self.wait4loader2disappear()

    def find_pod(self, pod_id):
        self.wait4loader2disappear()
        self.wait_until_presence_of_all_css("table[role='grid'] > tbody > tr > td[role='gridcell']")
        self.wait_until_clickable_css("input#gs_pod_name")
        self.set_input_value("input#gs_pod_name", pod_id + "\n\n")
        ActionChains(self.driver).click(self.wait_until_clickable_css(
                "input#gs_pod_name")).send_keys(Keys.RETURN).perform()
        self.wait_until_visible_xpath(
                "//td[(@aria-describedby='pod_grid_pod_name') and contains(@title, '" + pod_id + "')]/a")

    def open_pod(self, pod_id):
        self.wait_until_clickable_xpath("//td[(@aria-describedby='pod_grid_pod_name') and contains(@title, '" +
                                        pod_id + "')]/a").click()
        self.wait_until_visible_css("h4 a[title*='" + pod_id + "']")

    def find_text_ad(self, ad_data):
        """
        :param ad_data: ad dictionary data
        :returns:bool:True if found False if not
        """
        self.wait4loader2disappear()
        self.wait_until_presence_of_all_css("table[role='grid'] > tbody > tr > td[role='gridcell']")
        title = ad_data.get("Title", "") + ad_data.get("Line1", "") + ad_data.get("Line2", "") + \
                ad_data.get("DisplayURL", "") + ad_data.get("DestinationURL", "")
        try:
            self.wait_until_presence_of_all_css("table#ad_grid > tbody > tr > td[title^='" + title + "']")
            ok_(True, "FoundTextAd")
            return True
        except TimeoutException:
            ok_(False, "FoundTextAd")
            return False

    def find_image_ad(self, ad_data):
        """
        :param ad_data: ad dictionary data
        :returns:bool:True if found False if not
        """
        try:
            self.wait_until_visible_xpath(
                    "//td[contains(@title, '" + ad_data.get("DisplayURL", "") + "') and contains(@title, '"
                    + ad_data.get("DestinationURL", "") + "') and contains(@title, 'Image ad')]")
            ok_(True, "FoundTextAd")
            return True
        except TimeoutException:
            ok_(False, "FoundImageAd")
            return False

    def validate_client_column_value(self, client_name, value):
        self.wait_until_xpath_contains_text("//td[@role='gridcell' and @title='" + client_name +
                                            "']/../td[@aria-describedby='tab-client-mode_t_pending_update_count']/a",
                                            value)

    def open_pending_requests(self, client_name):
        self.wait_until_clickable_xpath("//td[@role='gridcell' and @title='" + client_name +
                                        "']/../td[@aria-describedby='tab-client-mode_t_pending_update_count']/a")\
            .click()
        self.wait_until_visible_css("#ui-dialog-title-pending_platform_update_container")

    def validate_pending_requests(self, updates):
        self.wait_until_presence_of_all_css("table#pending_platform_update_grid > tbody > tr")
        self.implicit_waiter(30)
        i = 1
        for update in updates:
            self.wait_until_visible_xpath("//tr[@id='" + str(i) +
                                          "']/td[(@aria-describedby='pending_platform_update_grid_platform_id') and "
                                          "(@title='" + update['Platform'] + "')]")
            self.wait_until_visible_xpath("//tr[@id='" + str(i) +
                                          "']/td[(@aria-describedby='pending_platform_update_grid_action') and "
                                          "(@title='" + update['Action'] + "')]")
            self.wait_until_visible_xpath("//tr[@id='" + str(i) +
                                          "']/td[(@aria-describedby='pending_platform_update_grid_recipient_name') and "
                                          "contains(@title, '" + update['RecipientName'] + "')]")
            self.wait_until_visible_xpath("//tr[@id='" + str(i) +
                                          "']/td[(@aria-describedby='pending_platform_update_grid_recipient') and "
                                          "(@title='" + update['Recipient'] + "')]")
            i += 1

    def close_pending_requests(self):
        self.wait_until_clickable_css("div[aria-labelledby='ui-dialog-title-pending_platform_update_container'] "
                                      "a.ui-dialog-titlebar-close").click()
        self.wait_until_not_visible_css("#ui-dialog-title-pending_platform_update_container")

    def select_client_by_name(self, client_name):
        self.wait_until_clickable_xpath("//td[@title='" + client_name + "']/../td/input[@type='checkbox']").click()

    def validate_grid_empty(self):
        self.wait_until_not_visible_css("table[aria-labelledby='gbox_tab-client-mode_t'] > tbody")