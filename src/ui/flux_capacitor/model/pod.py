'''
Created on Oct 6, 2014

@author: Gavrish
'''

from functools import reduce
from nose.tools import ok_
from selenium.common.exceptions import UnexpectedAlertPresentException, TimeoutException
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from .flux_capacitor import FluxCapacitor


class PodModel(FluxCapacitor):
    """
    Object Model for Pod
    """

    def __init__(self, driver, timeout=None):
        super().__init__(driver, timeout)


    def validate_tableview_fields(self, fieldname, value):
        """
        Validate value inside of table cell.
        fieldname -- name for a parameter that we are validating
        value -- value for a validated parameter
        """
        if value:
            selector = self.wait_until_clickable_xpath(
                "//div[@class='ns-content-fields']//div[contains(@class,'ns-field') and contains(text(), '"
                + fieldname + "')]/../div[contains(@class,'ns-value')]")
            print("expected value = " + value + " and actual value = " + selector.text)
            ok_(selector.text.find(value) >= 0)

    def add_placements(self, placements):
        """
        Add placements.
        placements -- list of string values
        """
        self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[value='Add Placement to this Pod']")))
        self.driver.find_element_by_css_selector("input[value='Add Placement to this Pod']").click()
        self.wait.until(EC.presence_of_element_located((By.XPATH, "//h2[contains(text(),'Add New placement(s) for')]")))
        result_keyword = reduce(lambda x,y: x + '\n' + y, placements)
        self.set_input_value("textarea[name='placements']", result_keyword)
        self.driver.find_element_by_css_selector("#add-placements").click()
        Alert(self.driver).accept()
    #     validate keywords were added
        for word in placements:
            self.wait.until(EC.presence_of_element_located((By.CSS_SELECTOR,
                                                    "td[aria-describedby*='placement_'][title='" + word + "']")))


    def add_keywords(self, keywords):
        """
        Add keywords
        keywords -- list of values
        """

        self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[value='Add Keyword to this Pod']")))
        self.driver.find_element_by_css_selector("input[value='Add Keyword to this Pod']").click()
        self.wait.until(EC.presence_of_element_located((By.XPATH, "//h2[contains(text(),'Add New Keyword(s) for')]")))
        result_keyword = reduce(lambda x,y: x + '\n' + y, keywords)
        self.set_input_value("textarea[name='keywords']", result_keyword)
        self.driver.find_element_by_css_selector("#add-keywords").click()
        
        Alert(self.driver).accept()
    #     validate keywords were added
        for word in keywords:
            self.wait.until(EC.presence_of_element_located((By.CSS_SELECTOR,
                                                    "td[aria-describedby*='keyword_text'][title='" + word + "']")))

    def create_pod(self, pod, name):
        """
        Create pod from given pod and name datas
        Validate pod created contains correct values in it
        """

        try:
            self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[value='Add Pod']"))).click()
        except UnexpectedAlertPresentException:
            self.accept_js_alert()

        try:
            self.wait.until(EC.presence_of_element_located((By.XPATH, "//h2[contains(text(),'Create Pod')]")))
        except TimeoutException:
            self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[value='Add Pod']"))).click()
            self.accept_js_alert()
            self.wait.until(EC.presence_of_element_located((By.XPATH, "//h2[contains(text(),'Create Pod')]")))

        self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "#ns-platform")))

        Select(self.driver.find_element_by_css_selector("#ns-platform")).select_by_visible_text(pod["platform"])
        self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name='pod_name']")))
        self.set_input_value("input[name='pod_name']", name)
        self.set_input_value("textarea[name='description']", pod["description"])
        self.set_input_value("input[name='adgroup']", pod["adgroup"] + name)
        self.set_input_value("input[name='max_cpc']", pod["max_cpc"])
        self.set_input_value("input[name='groupTag']", pod["groupTag"])
        Select(self.driver.find_element_by_css_selector("#pod_cat")).select_by_visible_text(pod["category"])
        Select(self.driver.find_element_by_css_selector("#net_id")).select_by_visible_text(pod["network"])
        Select(self.driver.find_element_by_css_selector("#availability_id")).select_by_visible_text(pod["availability"])
        Select(self.driver.find_element_by_css_selector("#vert_id")).select_by_visible_text(pod["vertical"])

        self.driver.find_element_by_css_selector("input[name='create_pod']").click()

        # validate after pod creation we are on pod info view page
        self.wait.until(EC.presence_of_element_located((By.XPATH, "//li[contains(@class,'ui-state-active')]/a[text()='Pod Info']")))

    #     validate pod info
        self.validate_tableview_fields('Name', name)
        self.validate_tableview_fields('Description', pod["description"])
        self.validate_tableview_fields('Ad Group', pod["adgroup"] + name)
        self.validate_tableview_fields('Pod Category', pod["category"])
        self.validate_tableview_fields('Manufacturer', pod["manufacturer"])
        self.validate_tableview_fields('Group Tag', pod["groupTag"])
        self.validate_tableview_fields('Network', pod["network"])
        self.validate_tableview_fields('Platform', pod["platform"])
        self.validate_tableview_fields('Vertical(s)', pod["vertical"])
        self.validate_tableview_fields('Label(s)', "")
        self.validate_tableview_fields('Pod Availability', pod["availability"])

    def search_in_pod_name_column(self, name):
        """
        Search for a pod by given name
        """
        self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, ".ui-pg-selbox")))
        self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "tr.jqgrow:nth-of-type(50)")))
        self.wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, "input#gs_pod_name")))
        self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input#gs_pod_name")))
        self.driver.find_element_by_css_selector("input#gs_pod_name").click()
        self.set_input_value("input#gs_pod_name", name + "\n")
        self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'td[aria-describedby="pod_grid_pod_name"] > a[title="Chrysler Digital pod for Netsertive"]')))

    def open_pod_from_grid(self,name):
        """
        Open Pod info page once we found it by given name
        """
        self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'td[aria-describedby="pod_grid_pod_name"][title="' + name + '"] > a'))).click()
        try:
            self.wait.until(EC.presence_of_element_located((By.XPATH, "//h2[contains(text(), 'All Venues for Pod')]/a[text()='" + name +"']")))
        except TimeoutException:
            self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'td[aria-describedby="pod_grid_pod_name"][title="' + name + '"] > a'))).click()
            self.wait.until(EC.presence_of_element_located((By.XPATH, "//h2[contains(text(), 'All Venues for Pod')]/a[text()='" + name +"']")))


    def validate_pod_search(self, pod_id):
        self.wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, "td[aria-describedby='pod_grid_id'][title='" + pod_id + "']")))