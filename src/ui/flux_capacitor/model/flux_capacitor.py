'''
Created on Oct 6, 2014

@author: Gavrish
'''

from selenium.common.exceptions import TimeoutException, ElementNotVisibleException
from ...model.page_object import PageObject


class FluxCapacitor(PageObject):
    """
    Object model for Flux Capacitor.
    General functionality
    """
    LOADER_SELECTORS = ["#load_client_grid", "#load_venue_grid0", "#load_pod_grid"]

    def __init__(self, driver, timeout=None):
        super().__init__(driver, timeout)

    def login(self, login, password):
        """
        Login to AMC
        """
        self.wait_until_visible_css("input[name='username']")
        self.wait_until_clickable_css("input[name='username']").send_keys(login)
        self.wait_until_clickable_css("input[name='password']").send_keys(password)
        self.wait_until_clickable_css("input[name='submit']").click()
        self.wait_until_present_css("#ns-body-c")

    def logout(self):
        """
        Logout from AMC
        """

        self.wait_until_clickable_css("label[for='logout'] > span").click()
        try:
            self.wait_until_visible_css("input[name='username']")
        except Exception:
            # todo: Appears this isn't hit often if at all. Ask Nikita if this is needed. Maybe we remove to keep it simple?
            print("Exception thrown - trying another method...")
            self.wait_until_clickable_css("label[for='logout'] > span").click()
            self.wait_until_visible_css("input[name='username']")

    def goto_tab(self, tab_name):
        """
        Navigate to tab with given name
        """
        self.wait_until_clickable_xpath("//ul/li/a[text()='" + tab_name + "' and not(ancestor::div[contains(@style,'display: none')])]").click()

        try:
            self.wait_until_clickable_xpath(
                    "//li[contains(@class,'ui-state-active')]/a[text()='" + tab_name + "' and not(ancestor::div[contains(@style,'display: none')])]")
        except Exception:
            self.accept_js_alert()
            self.wait_until_clickable_xpath("//ul/li/a[text()='" + tab_name + "' and not(ancestor::div[contains(@style,'display: none')])]").click()
            self.wait_until_clickable_xpath("//li[contains(@class,'ui-state-active')]/a[text()='" + tab_name + "' and not(ancestor::div[contains(@style,'display: none')])]")

    def search_by_key(self, key):
        self.wait_until_clickable_css("#project").click()
        self.set_input_value("#project", key + "\n")
        self.wait_until_clickable_xpath("//h1[contains(text(), 'Search Results')]")

    def update_date_picker(self, radio_button):
        """
        Select date range from Date Picker, update to provided date range and wait for loader to disappear
        """
        try:
            self.wait_until_not_visible_css("div[class='ui-widget-overlay']" +
                                            "[style='width: 1050px; height: 2780px; z-index: 1001;']")
            self.wait_until_clickable_css("#dialog_link_dr").click()
            self.driver.implicitly_wait(5)
            self.wait_until_clickable_xpath(radio_button).click()
            self.wait_until_clickable_xpath("//div[contains(@class, 'ui-dialog')]//"
                                            "button/span[contains(text(), 'Save')]").click()
        except (ElementNotVisibleException, TimeoutException):
            self.wait_until_clickable_css("#dialog_link_dr").click()
            self.wait_until_clickable_xpath(radio_button).click()
            self.wait_until_clickable_xpath("//div[contains(@class, 'ui-dialog')]//"
                                            "button/span[contains(text(), 'Save')]").click()
        self.wait_until_not_visible_xpath(radio_button)
        self.wait_until_clickable_css("#load_client_grid")
        try:
            self.wait_until_not_visible_css("#load_client_grid")
        #     takes longer on qatestingpro QA env
        except TimeoutException:
            self.implicit_waiter(60)
            self.wait_until_not_visible_css("#load_client_grid")

    def update_date_picker_custom(self, startdate, enddate):
        """
        Select date range from Date Picker, update to provided date range and wait for loader to disappear
        """
        self.wait_until_clickable_css("#dialog_link_dr").click()
        self.wait_until_clickable_css('#Custom').click()
        self.set_input_value('#startDate', startdate + "\n")
        self.set_input_value('#endDate', enddate + "\n")
        self.wait_until_clickable_css('#Custom').click()
        self.wait_until_not_visible_css("#ui-datepicker-div")
        self.driver.implicitly_wait(5)
        self.wait_until_clickable_xpath("//div[contains(@class, 'ui-dialog')]//div[@id='dialog-dr']/.." +
                                        "//button/span[contains(text(), 'Save')]").click()

        try:
            self.wait_until_not_visible_css("#load_client_grid")
        #     takes longer on qatestingpro QA env
        except TimeoutException:
            self.implicit_waiter(60)
            self.wait_until_not_visible_css("#load_client_grid")

    def wait4loader2disappear(self):
        """
        Wait for loading spinner to disappear
        """
        # todo: update this to include any loader - there should be simple way to do this
        print("Waiting for loading spinner to disappear")
        for selector in self.LOADER_SELECTORS:
            # making it more stable
            try:
                self.wait_until_not_visible_css(selector)
            except TimeoutException:
                # in qatestingpro loader doesn't disappear for long time. why?
                self.implicit_waiter(30)
                self.wait_until_not_visible_css(selector)

    def goto_request_platform_updates(self):
        self.goto_tab('Reports & Tools')
        self.wait_until_clickable_css("a[href='./tools/view_platform_update_request']").click()
        self.wait4loader2disappear()
        self.wait_until_presence_of_all_css("td[role='gridcell']")
        self.implicit_waiter(10)

    def submit_request_update(self):
        self.wait_until_clickable_css("input#request_update1").click()
        self.wait4loader2disappear()
        self.implicit_waiter(10)

    def goto_client(self, client_name):
        self.wait_until_clickable_xpath("//div[@id='ns-clients']//a[@title='" + client_name + "']").click()

    def goto_venue(self, venue_name):
        self.wait_until_clickable_xpath("//div[@id='ns-clients']//a[@title='" + venue_name + "']").click()

    def goto_sponsor(self, sponsor_name):
        return
