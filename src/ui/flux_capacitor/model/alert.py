from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from .flux_capacitor import FluxCapacitor

__author__ = 'ngavrish'


class Alert(FluxCapacitor):
    """
    Object Model for Alert
    """

    def __init__(self, driver, timeout=None):
        super().__init__(driver, timeout)

    def add_alert(self, alert):
        # alert
        self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,
                        "button#add_task"))).click()
        self.wait.until(EC.element_to_be_clickable((By.XPATH,
                        "//div[contains(@class, 'ui-dialog-titlebar')]/span[contains(text(), 'Create New Alert')]")))
        self.set_input_value("input#action_title", alert['title'])
        self.set_input_value("textarea#message", alert['message'])
        self.select_dropdown_css_by_text('select#action_category_type_id', alert['type'])
        self.select_dropdown_css_by_text('select#action_priority', alert['priority'])

        self.wait_until_clickable_css('input.search.ui-widget-content.ui-corner-all').click()
        self.set_input_value('input.search', alert['user'] + "\n\n")

        self.wait_until_clickable_xpath("//li[contains(text(), '" + alert['user'] + "')]/a").click()
        self.wait_until_clickable_xpath("//div[contains(@class, 'list-container selected')]/ul/li[contains(text(), '" +
                                        alert['user'] + "')]")
        self.wait_until_clickable_xpath("//span[contains(text(), 'Create New Alert')]/../..//button/"
                                        "span[contains(text(),'Save')]").click()
        self.wait_until_not_visible_xpath("//div[contains(@class, 'ui-dialog-titlebar')]/span[contains(text(), "
                                      "'Create New Alert')]")

        self.wait_until_present_xpath("//td[(@aria-describedby='my_tasks_grid_action_title') and (@title='" +
                                      alert['title'] +"')]/../" +
                                    "td[(@aria-describedby='my_tasks_grid_action_category_type_id') and (@title='" +
                        alert['type'] + "')]/../td[(@aria-describedby='my_tasks_grid_action_owner_user_name') and " +
                        "(@title='" + alert['user'] +"')]/../td[(@aria-describedby='my_tasks_grid_action_message') "
                        "and (@title='" + alert['message'] + "')]")


