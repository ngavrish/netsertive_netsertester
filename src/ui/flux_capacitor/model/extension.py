from .flux_capacitor import FluxCapacitor

__author__ = 'fthong'


class Extension(FluxCapacitor):
    """
    Object Model for Extensions
    """

    def __init__(self, driver, timeout=None):
        super().__init__(driver, timeout)
        # Test data will eventually be dynamically generated.
        self.mobile_number = "7778889999"
        self.callout_text = "Automated callout text"

    def format_telephone_number(self, number):
        return '(' + number[0:3] + ') ' + number[3:6] + '-' + number[6:10]

    def open_create_extension_dialog(self, extension_type):
        self.wait_until_clickable_xpath("//input[@value='Create Ad Extension']").click()
        self.select_dropdown_css_by_visible_text_contians("#ad_extension_type_id", extension_type)

    def get_xpath_for_extension_row(self, identifying_text):
        # TODO - This works for now, but better way would be to use the extension id somehow.
        return "//table[@id='ad_extension_grid']/tbody//td[text()='" + \
               identifying_text + "']/../../../../.."

    def create_mobile_extension(self):
        self.open_create_extension_dialog("Mobile Extension")

        self.wait_until_visible_xpath("//input[@id='PhoneNumber']")
        self.wait_until_clickable_xpath("//input[@id='PhoneNumber']").send_keys(self.mobile_number)

        # For a more restrictive xpath selector, use the commented line.
        # /html/body[@class='ui-widget-content']/div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all
        # ui-draggable ui-resizable'][2]//button/span[text()='Save']
        self.wait_until_clickable_xpath("//div[contains(@class, 'ui-dialog')][2]//"
                                        "button/span[contains(text(), 'Save')]").click()

    def validate_create_mobile_extension(self):
        # Reload page to show new extension.
        self.driver.refresh()
        self.wait4loader2disappear()

        expected_xpath = self.get_xpath_for_extension_row(self.format_telephone_number(self.mobile_number))
        row_text = self.wait_until_clickable_xpath(expected_xpath).text

        assert 'Mobile Extension' in row_text, '"Mobile Extension" not in ' + row_text
        assert self.format_telephone_number(self.mobile_number) in row_text, \
            'Mobile Number ' + self.mobile_number + ' not in ' + row_text

    def cleanup_create_mobile_extension(self):
        # Get xpath for the extension's status icon.
        expected_xpath = self.get_xpath_for_extension_row(self.format_telephone_number(self.mobile_number)) + "/td[4]/a"
        self.wait_until_clickable_xpath(expected_xpath).click()

        self.select_dropdown_css_by_visible_text_contians("#ad_extension_status", "Deleted")
        self.wait_until_clickable_xpath("//div[contains(@class, 'ui-dialog')][4]//"
                                        "button/span[contains(text(), 'Save')]").click()

    def create_callout_extension(self, mobile_preferred=False):
        self.open_create_extension_dialog("Callout Extension")

        self.wait_until_visible_xpath("//input[@id='CalloutText']")
        self.wait_until_clickable_xpath("//input[@id='CalloutText']").send_keys(self.callout_text)

        if mobile_preferred:
            self.wait_until_clickable_xpath("//input[@id='MobilePreferred']").click()

        self.wait_until_clickable_xpath("//div[contains(@class, 'ui-dialog')][2]//"
                                        "button/span[contains(text(), 'Save')]").click()

    def validate_create_callout_extension(self, mobile_preferred=False):
        # Reload page to show new extension.
        self.driver.refresh()
        self.wait4loader2disappear()

        expected_xpath = self.get_xpath_for_extension_row(self.callout_text)
        row_text = self.wait_until_clickable_xpath(expected_xpath).text

        assert 'Callout Extension' in row_text, '"Callout Extension" not in ' + row_text
        assert self.callout_text in row_text, 'Callout Text ' + self.callout_text + ' not in ' + row_text
        if mobile_preferred:
            assert 'Mobile Preferred' in row_text
        else:
            assert 'Mobile Preferred' not in row_text

    def cleanup_create_callout_extension(self):
        # Get xpath for the extension's status icon.
        expected_xpath = self.get_xpath_for_extension_row(self.callout_text) + "/td[4]/a"
        self.wait_until_clickable_xpath(expected_xpath).click()

        self.select_dropdown_css_by_visible_text_contians("#ad_extension_status", "Deleted")
        self.wait_until_clickable_xpath("//div[contains(@class, 'ui-dialog')][4]//button/span[contains(text(), 'Save')]").click()
