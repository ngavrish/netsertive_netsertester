"""
Created on Jul 26, 2015

@author = tclark@netsertive.com
"""
from nose.tools import eq_
from selenium.common.exceptions import StaleElementReferenceException, TimeoutException, UnexpectedAlertPresentException, WebDriverException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from ....flux_capacitor.model.flux_capacitor import FluxCapacitor


class ProgramCenter(FluxCapacitor):
    """
    Object model for Flux Capacitor Program Center.
    General functionality
    """
    def __init__(self, driver):
        super().__init__(driver)
        self.grid = Grid(driver)

    @property
    def create_sponsor_button(self):
        return self.wait_until_clickable_css("input#create_sponsor")

    @property
    def cancel_create_sponsor_button(self):
        return self.wait_until_clickable_xpath("//input[@value='Cancel']")

    @property
    def cancel_manage_sponsor_network(self):
        return self.wait_until_clickable_css("input#cancel_manage")

    def goto_sponsor_wizard(self):
        self.wait_until_clickable_css("input#create_sponsor").click()
        self.wait_until_clickable_xpath("//h2[contains(text(),'Edit a Sponsor')]")

    def goto_sponsored_campaign_wizard(self):
        self.wait_until_clickable_css("#create_sponsored_campaign").click()
        self.wait_until_clickable_xpath("//h2[contains(text(),'Add New Sponsored Campaign')]")

    def set_sponsor_basic(self, data):
        self.driver.implicitly_wait(10)
        self.set_input_value("input[name='sponsor_name']", data['name'])
        self.select_dropdown_css_by_text("select#client_account_rep_id", data["client-acc-rep"])
        self.select_dropdown_css_by_text("select#business_segment_id", data["business-segment"])
        self.select_dropdown_css_by_text("select#company_type_id", data["company-type"])
        self.set_input_value("input[name='logo_url']", data['logo-url'])

    def validate_sponsor_basic(self, data):
        self.wait_until_clickable_xpath("//div[text()='Sponsor Name']/../div[text()='" + data['name'] + "']")
        eq_(self.wait_until_clickable_xpath("//div[text()='Business Segment']/../div[2]").text,
            data['business-segment'])
        eq_(self.wait_until_clickable_xpath("//div[text()='Company Type']/../div[2]").text,
            data['company-type'])
        self.wait_until_clickable_xpath("//div[text()='Logo URL']/../div[text()='"
                                        + data['logo-url'] + "']")

    def create_valid_sponsor(self, data):
        self.set_sponsor_basic(data)
        self.save_sponsor()
        self.wait_until_clickable_xpath("//div[contains(text(),'View Sponsor Info')]")
        self.validate_sponsor_basic(data)

    def set_sponsored_campaign_basic(self, data):
        self.driver.implicitly_wait(10)
        self.set_input_value("input[name='sponsored_campaign_name']", data['name'])
        self.set_input_value("textarea[name='description']", data['description'])
        try:
            self.wait_until_clickable_css("#start_date").click()
            self.driver.implicitly_wait(10)
            self.wait_until_clickable_css("a.ui-datepicker-next").click()
            self.wait_until_clickable_css("table.ui-datepicker-calendar > tbody > tr:first-of-type "
                                          "> td:last-of-type > a").click()
        except TimeoutException:
            self.wait_until_clickable_css("#start_date").click()
            self.driver.implicitly_wait(10)
            self.wait_until_clickable_css("a.ui-datepicker-next").click()
            self.wait_until_clickable_css("table.ui-datepicker-calendar > tbody > tr:first-of-type >"
                                          " td:last-of-type > a").click()
        # wrong element receives the click event. workaround for this:
        try:
            self.wait_until_clickable_css("#end_date").click()
        except WebDriverException:
            self.driver.implicitly_wait(10)
            try:
                self.wait_until_clickable_css("#end_date").click()
            except WebDriverException:
                self.driver.implicitly_wait(10)
                try:
                    self.wait_until_clickable_css("#end_date").click()
                except WebDriverException:
                    self.driver.implicitly_wait(10)
                    self.wait_until_clickable_css("#end_date").click()

        self.wait_until_clickable_css("a.ui-datepicker-next").click()
        self.wait_until_clickable_css("table.ui-datepicker-calendar > tbody > tr:last-of-type > "
                                      "td:first-of-type > a").click()
        self.select_dropdown_css_by_text("#ns-sponsor", data['sponsor'])
        self.wait_until_clickable_css("#" + data['type'])
        self.driver.execute_script("$('#{type}').click()".format(type=data['type']))
        try:
            self.save_sponsored_campaign()
        except UnexpectedAlertPresentException:
            self.wait_until_clickable_css("#" + data['type'])
            self.driver.execute_script("$('#{type}').click()".format(type=data['type']))
            self.save_sponsored_campaign()

    def save_sponsored_campaign(self):
        self.wait_until_clickable_css("input#save_sponsored_campaign").click()
        self.wait_until_not_exists_css("input#save_sponsored_campaign")

    def validate_sponsored_campaign_basic(self, data):
        self.wait_until_clickable_xpath("//div[text()='Sponsored Campaign Name']/../div[text()='" + data['name'] + "']")
        self.wait_until_clickable_xpath("//div[text()='Sponsored Campaign Type']/../div[text()='" + data['type'] + "']")
        self.wait_until_clickable_xpath("//div[text()='Description']/../div[text()='" + data['description'] + "']")
        self.wait_until_clickable_xpath("//div[text()='Sponsor Company Name']/../div/a[text()='" + data['sponsor'] +
                                        "']")

    def search_in_sponsored_campaign_column(self, name):
        """
        Search for Sponsored Campaign in Program Center
        """
        self.wait_until_clickable_css(".ui-pg-selbox")
        self.wait_until_clickable_css("tr.jqgrow:nth-of-type(50)")
        ActionChains(self.driver).click(self.wait_until_clickable_css("#gs_sponsored_campaign_name"))\
                                .send_keys(name).send_keys(Keys.RETURN).perform()
        self.wait_until_clickable_css('td[aria-describedby="program_center_grid_sponsored_campaign_name"] > a[title*="'
                                      + name + '"]')

    def open_sponsored_campaign(self, name):
        """
        Open sponsored campaign details
        """
        self.wait_until_clickable_css(
            'td[aria-describedby="program_center_grid_sponsored_campaign_name"] > a[title*="' + name + '"]')\
            .click()
        self.wait_until_clickable_css("a[title=\"" + name + "\"]")

    def add_sponsored_campaign_subscribers(self):
        self.wait_until_clickable_css("button#add_subscriber").click()

    # todo fix this - can't seem to find the element
    # def cancel_sponsored_campaign_subscribers(self):
    #     self.wait.until(EC.element_to_be_clickable((By.XPATH, "//select[@id='new_subscriber_status']")))
    #     self.driver.find_element_by_xpath("//span[@class='ui-icon ui-icon-closethick']").send_key(Keys.ESCAPE)

    def cancel_sponsored_campaigns(self):
        self.wait_until_clickable_css("input#cancel").click()

    def edit_sponsored_campaign_pods(self):
        self.wait_until_clickable_css("input[value='Edit Sponsored Campaign Pods']").click()

    def save_sponsored_campaign_pods(self):
        self.wait_until_clickable_css("input#save").click()

    def cancel_sponsored_campaign_pods(self):
        self.wait_until_clickable_css("input#cancel").click()

    def edit_sponsored_campaign(self):
        self.wait_until_clickable_css("input[value='Edit Sponsored Campaign Pods']").click()

    def edit_sponsored_campaign_details(self):
        self.wait_until_clickable_css("input[value='Edit Sponsored Campaign Details']").click()

    def cancel_sponsored_campaign_details(self):
        self.wait_until_clickable_xpath("//*[@id='sponsored_campaign_edit_form']/div/div[2]/input[2]").click()

    def edit_sponsor(self):
        self.wait_until_clickable_css("input#edit_sponsor").click()

    def save_sponsor(self):
        self.wait_until_clickable_css("input#save_sponsor").click()

    def edit_sponsor_network_labels(self):
        self.wait_until_clickable_css("input#edit_sponsor_labels").click()

    def cancel_sponsor_network_labels(self):
        self.wait_until_clickable_css("input#cancel_manage").click()

    def save_sponsor_network_labels(self):
        self.wait_until_clickable_css("input[value='Save']").click()

    def manage_sponsor_network(self):
        self.wait_until_clickable_css("input[value='Manage Sponsor Network']").click()

    def bulk_add_clients(self):
        self.wait_until_clickable_xpath("//input[@value='Bulk Add Clients']").click()

    def bulk_add_clients_cancel(self):
        self.wait_until_clickable_xpath("//input[@id='cancel_bulk']").click()

    def bulk_add_clients_save(self):
        self.wait_until_clickable_xpath("//input[@id='cancel_bulk']").click()

    def apply_labels(self):
        self.wait_until_clickable_xpath("//button[@id='label-apply-button']").click()

    def filter_labels(self):
        self.wait_until_clickable_xpath("//button[@id='label-filter-button']").click()


class Grid(FluxCapacitor):

    def __init__(self, driver):
        super().__init__(driver)

    def search_sponsored_campaign_by_name(self, name):
        """
        Search for the sponsored campaign in AMC program centre
        """
        self.wait_until_presence_of_all_css("tr.jqgrow")
        ActionChains(self.driver).click(self.wait_until_clickable_css("input#gs_sponsored_campaign_name")).\
            send_keys(name + "\n").send_keys(Keys.RETURN).perform()
        self.wait_until_clickable_css('td[aria-describedby="program_center_grid_sponsored_campaign_name"] > a[title*="'
                                      + name + '"]')

    def search_sponsor_by_name(self, name):
        """
        Search for the sponsor in AMC program centre
        """
        self.wait_until_presence_of_all_css("tr.jqgrow")
        ActionChains(self.driver).click(self.wait_until_clickable_css("input#gs_sponsor_name")).\
            send_keys(name + "\n").send_keys(Keys.RETURN).perform()
        self.wait4loader2disappear()
        # workaround for a race condition. DOM is being refreshed when checking for condition
        try:
            self.wait_until_clickable_css('td[aria-describedby="sponsor_grid_sponsor_name"] > a[title="' + name + '"]')
        except StaleElementReferenceException:
            self.wait_until_clickable_css('td[aria-describedby="sponsor_grid_sponsor_name"] > a[title="' + name + '"]')

    def open_sponsor(self, name):
        """
        Open sponsor campaign details
        """
        # Workaround to avoid StaleElementReferenceException
        # Stale Element exception may occur when document is loaded and element is found, grid is reloaded one
        # more time before we click on it. So we fall into StaleReferenceException.
        try:
            self.wait_until_clickable_css('td[aria-describedby="sponsor_grid_sponsor_name"] > a[title="' + name +
                                          '"]').click()
        except StaleElementReferenceException:
            self.wait_until_clickable_css('td[aria-describedby="sponsor_grid_sponsor_name"] > a[title="' + name +
                                          '"]').click()
        self.wait_until_clickable_css('a[title="' + name + '"]')
        self.wait_until_clickable_xpath("//div[contains(text(),'View Sponsor Info')]")
