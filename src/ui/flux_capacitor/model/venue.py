'''
Created on Dec 23, 2014

@author: NGavrish
'''

import time
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from ...model.page_object import PageObject


class Venue(PageObject):
    """
    Object Model for Launcher
    """

    def __init__(self, driver, timeout=None):
        super().__init__(driver, timeout)
    
    def set_basic_info(self, venue_basic, venue_id=None):
        """
        Set basic info for Venue
        """
        self.set_input_value("input[name='venue_name']", venue_id)
        self.set_input_value("input[name='campaign_name']", venue_id + venue_basic['CAMPAIGN_NAME'])
        self.set_input_value("textarea[name='description']", venue_basic['DESCRIPTION'] + venue_id)

        self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, '#ns-platform')))
        Select(self.driver.find_element_by_css_selector('#ns-platform')).select_by_visible_text(venue_basic['PLATFORM'])
        time.sleep(2)
        Select(self.driver.find_element_by_css_selector('#ns-network')).select_by_visible_text(venue_basic['NETWORK'])

        if venue_basic.get('DESKTOP', "False") == "True":
            self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "#DESKTOP")))
            if not self.driver.find_element_by_css_selector('#DESKTOP').is_selected():
                self.driver.find_element_by_css_selector('#DESKTOP').click()
        if venue_basic.get('MOBILE', "False") == "True":
            self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "#HIGHENDMOBILE")))
            if not self.driver.find_element_by_css_selector('#HIGHENDMOBILE').is_selected():
                self.driver.find_element_by_css_selector('#HIGHENDMOBILE').click()
        if venue_basic.get('TABLET', "False") == "True":
            self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "#TABLET")))
            if not self.driver.find_element_by_css_selector('#TABLET').is_selected():
                self.driver.find_element_by_css_selector('#TABLET').click()
        # SET xId if it's set
        if 'XID' in venue_basic:
            self.set_input_value("input[name='xId']", venue_basic['XID'])

    def set_budget_and_automation(self, budget_and_automation):
        """
        Set budget and automation for venue
        """                
        self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, '#budget_currency')))
        Select(self.driver.find_element_by_css_selector('#budget_currency')).select_by_visible_text(budget_and_automation['TARGET_CURRENCY'])

        # Only set the flag if it's included in the config
        if 'AUTOMATION_ENABLED' in budget_and_automation:
            Select(self.driver.find_element_by_css_selector('#automation_enabled')).select_by_visible_text(budget_and_automation['AUTOMATION_ENABLED'])
        self.set_input_value("#monthly_budget", budget_and_automation['MONTHLY_BUDGET'])
        self.set_input_value("#daily_budget", budget_and_automation['DAILY_BUDGET'])
        
    def submit(self):
        """
        Save venue info
        """
        self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, '#save_venue'))).click()

    def set_jigawatts(self, jigawatts):
        # currently set only one jigawatt
        for i in range(3):
            self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, '#addButton')))
            self.driver.find_element_by_css_selector("#addButton").click() 
            self.wait.until(EC.element_to_be_clickable((By.XPATH, "//div[@id='jigawatt']/div[contains(@class, 'ns-value')][" + str(i+2) +"]")))
        self.wait.until_not(EC.visibility_of_element_located((By.CSS_SELECTOR, "#addButton")))    
        for i in range(3):
            self.wait.until(EC.element_to_be_clickable((By.XPATH, "//div[@id='jigawatt']/div[contains(@class, 'ns-value')][1]/input[@id='removeButton']")))          
            self.driver.find_element_by_xpath("//div[@id='jigawatt']/div[contains(@class, 'ns-value')][1]/input[@id='removeButton']").click()
        self.set_input_value("#location", jigawatts[0])