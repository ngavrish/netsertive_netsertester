from nose.tools import eq_
from selenium.common.exceptions import TimeoutException
from .flux_capacitor import FluxCapacitor

__author__ = 'ngavrish'


class DJFusionModel(FluxCapacitor):
    """
    Object Model for DJ Fusion
    """

    def __init__(self, driver, timeout=None):
        super().__init__(driver, timeout)

    def start_djfusion(self, company_name):
        """
        start DJ Fusion dialog from AMC. Stops at the point when DJ fusion dialog window is shown
        """

        self.wait_until_clickable_css("#fusion").click()
        self.wait_until_clickable_css("#fusion-client-select")
        self.wait_until_visible_xpath("//button/span[contains(text(), 'Next >')]")

        self.select_dropdown_css_by_visible_text_contians("#fusion-client-select", company_name)

        print("Selected " + company_name + " Successfully")
        self.wait_until_clickable_css("div[aria-labelledby='ui-dialog-title-fusion-client-selection'] button").click()
        self.wait_until_visible_css("#ui-dialog-title-fusion-client-information")

    def set_client_information(self, client_info):
        """
        Fill in initial client information, such as:
        ClientName, CompanyName, DigitalMediaFunds, SalesPackage, Verticals, TimeZone
        """
        #wait until validation class applies
        self.wait_until_clickable_css("#fusion-digital-media-funds.fusion-prevalidate.fusion-error-class")
        self.wait_until_clickable_css("#fusion-client-name")
        self.set_input_value("#fusion-client-name", client_info["ClientName"])
        self.set_input_value("#fusion-company-name", client_info["CompanyName"])
        self.set_input_value("#fusion-digital-media-funds", client_info["DigitalMediaFunds"])
        self.select_dropdown_css_by_text("#fusion-sales-packages", client_info['SalesPackage'])
        self.select_dropdown_css_by_text("#fusion-verticals", client_info['Verticals'])
        self.select_dropdown_css_by_text("#fusion-timezone", client_info['TimeZone'])

    def clientinfo_publish_continue(self):
        """
        Press 'publish and continue' button
        """
        self.wait_until_clickable_xpath("//button/span[contains(text(), 'Publish & Continue >')]").click()
        # try:
        #
        # except WebDriverException:
        #
        #     self.wait_until_clickable_xpath("//button/span[contains(text(), 'Publish & Continue >')]").click()

    def close_alert(self):
        """
        Close custom HTML alert
        """
        self.wait_until_clickable_css(
                "div[aria-labelledby='ui-dialog-title-fusion-message-container'] a.ui-dialog-titlebar-close").click()
        self.wait_until_invisible_css("div[aria-labelledby='ui-dialog-title-fusion-message-container']")

    def goto_clientinfo_tab(self, title):
        """
        navigate to Client Info tab
        """

        href_attr = ""
        if title == "Addresses":
            href_attr = "#client-info-verification-2"
        elif title == "Contacts":
            href_attr = "#client-info-verification-3"
        elif title == "Information":
            href_attr = "#client-info-verification-1"

        try:
            self.wait_until_clickable_css("a[href='" + href_attr + "']").click()
            self.wait_until_clickable_css(
                "#client-info-verification-tabs > ul > li.ui-state-active > a[href='" + href_attr + "']")
        except TimeoutException:
            # sometimes click on tab doesn't work. stabilizing it by try-catch
            self.wait_until_clickable_css("a[href='" + href_attr + "']").click()
            self.wait_until_visible_css("#client-info-verification-tabs > ul > li.ui-state-active > a[href='" +
                                        href_attr + "']")

    def add_venue(self, venue):
        """
        Open Add Venue wizard and add the venue
        """
        self.wait_until_clickable_css("#add-venue").click()
        self.wait_until_clickable_css("div[aria-labelledby='ui-dialog-title-fusion-add-venue-dialog']")
        self.select_dropdown_css_by_visible_text_contians("#fusion-venue-templates", venue['Template'])
        self.select_dropdown_css_by_visible_text_contians("#fusion-venue-platform", venue['Platform'])
        self.select_dropdown_css_by_visible_text_contians("#fusion-venue-verticals", venue['Vertical'])
        self.wait_until_clickable_xpath("//div[@aria-labelledby='ui-dialog-title-fusion-add-venue-dialog']//" +
                                        "button/span[contains(text(),'Add')]").click()
        self.wait_until_not_visible_css("div[aria-labelledby='ui-dialog-title-fusion-add-venue-dialog']")


    def add_keywords(self, keywords=None):
        """
        Open keywords form and save client keywords
        """
        self.wait_until_clickable_css('td[aria-describedby="fusion-venues-grid_company_pod_keywords_label"]'
                                      '[title="Add"]').click()
        self.wait_until_clickable_css('#keywords').click()
        if keywords:
            self.set_input_value("#keywords", keywords)
        else:
            self.wait.until(lambda driver: driver.find_element_by_css_selector("#keywords")
                                                 .get_attribute("value") != "")
        self.wait_until_clickable_css("div[aria-labelledby='ui-dialog-title-fusion-edit-keywords-dialog'] " +
                                      "div.ui-dialog-buttonset button:nth-of-type(1)").click()
        self.accept_js_alert()
        self.wait_until_not_visible_css("div[aria-labelledby='ui-dialog-title-fusion-edit-keywords-dialog']  " +
                                        "div.ui-dialog-buttonset button:nth-of-type(1)")

    def add_jigawatts(self, jigawatts, ad_company_name=None):
        """
        Add given jigawatts for a given company name
        jigawatts -- string generated from List with values divided by commas
        ad_company_name -- name for company, where jigawatts will be added. 'Search by default
        """

        if not ad_company_name:
            ad_company_name = "Search"
        self.wait_until_clickable_css("div[aria-labelledby='ui-dialog-title-fusion-venue-setup']")
        self.wait_until_clickable_xpath("//td[contains(@title,'(" + ad_company_name + ")')]/../td[@title='Add']")
        self.driver.find_element_by_xpath(
                "//td[contains(@title,'(" + ad_company_name + ")')]/../td[@title='Add']").click()
        self.wait_until_clickable_css("#fusion-jigawatts")
        self.set_input_value("#fusion-jigawatts", jigawatts)
        self.wait_until_clickable_css(
                "div[aria-labelledby*='jigawatts-dialog'] div.ui-dialog-buttonset > button:nth-of-type(1)")
        self.wait_until_clickable_css(
                "div[aria-labelledby*='jigawatts-dialog'] div.ui-dialog-buttonset > button:nth-of-type(1)").click()

    def save_and_finish_venue_setup(self):
        """
        click on 'Save and Finish' button for venue setup
        """
        self.wait_until_clickable_css(
                "div[aria-labelledby='ui-dialog-title-fusion-venue-setup'] button:not([id])").click()

    def save_and_finish_pod_setup(self, company_name):
        """
        click on 'Save and Finish' button for pod setup
        """
        # wait until dialog loads and company pod is added and visble
        self.wait_until_visible_xpath("//td[(@aria-describedby='fusion-subscribed-pods-grid_name') and "
                                      "contains(text(),'Company - " + company_name + "')]")
        self.wait_until_clickable_xpath("//button/span[contains(text(), 'Finish')]").click()
        # wait for dialog
        self.wait_until_clickable_css("#fusion-message-container")
        eq_(self.driver.find_element_by_css_selector("#message b").text, company_name)
        self.wait_until_clickable_xpath("//button/span[text()='Close']").click()
        # wait until dialog closes
        self.wait_until_invisible_css("#fusion-message-container")

    def close_alert_message(self, message):
        """
        Wait until alert popup appears. Validate the alert message and close it.
        """
        self.wait_until_clickable_xpath("//span[(@id='message')]/b[1][contains(text(),'" + message + "')]")
        self.wait_until_clickable_xpath("//button/span[text()='Close']").click()
        self.wait_until_not_visible_xpath("//button/span[text()='Close']")
