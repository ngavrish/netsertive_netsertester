
'''
Created on Dec 23, 2014

@author: Gavrish
'''
import os

from nose.tools import eq_
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.select import Select
from selenium.common.exceptions import UnexpectedAlertPresentException, TimeoutException, StaleElementReferenceException
from .grid import Grid
from .venue import Venue
from .flux_capacitor import FluxCapacitor


class Client(FluxCapacitor):
    """
    Object model for Flux Capacitor.
    General functionality
    """

    def __init__(self, driver, timeout=None):
        super().__init__(driver, timeout)
        self.grid = Grid(driver)

    def create_ad(self, ad_data, venue_id, pod_id):
        """
        Add ad for client
        """
        self.driver.execute_script("window.alert = function() {}")
        self.goto_tab("Ads")
        self.accept_js_alert()
        self.wait_until_clickable_css("input[value='Create New Ad']").click()
        self.wait_until_clickable_css("#dialog-add-ad").click()
        ActionChains(self.driver).click(self.wait_until_clickable_css("#podLookup")).send_keys(Keys.RETURN).perform()
        self.driver.execute_script("window.alert = function() {}")
        self.wait_until_clickable_css("#podLookupList")
        self.wait_until_clickable_css("#podLookup")
        self.wait_until_clickable_xpath("//option[contains(text(), '" + pod_id + "')]")
        ActionChains(self.driver).click(self.wait_until_clickable_css("#podLookup")).send_keys(pod_id + "\n").\
            send_keys(Keys.RETURN).perform()
        self.wait_until_visible_xpath("//option[contains(text(), '" + pod_id + "')]")
        self.wait_until_clickable_css("#podLookupList")
        # stability fix. at dropdown in selector might be unattached to document.
        # double check should fix that, because after rerun it passes each time
        try:
            self.select_dropdown_css_by_index("select[id='podLookupList']", 0)
        except StaleElementReferenceException:
            self.select_dropdown_css_by_index("select[id='podLookupList']", 0)
        self.select_dropdown_css_by_text("select[id='ad_type']", ad_data['Type'])
        if ad_data.get('Type') == 'Image':
            self.fillin_image_ad(ad_data)
        if ad_data.get('Type') == 'Text':
            self.fillin_text_ad(ad_data)

    def fillin_image_ad(self, ad_data):
        self.set_input_value("#display_url", ad_data['DisplayURL'])
        self.set_input_value("#dest_url", ad_data['DestinationURL'])
        self.wait_until_clickable_css("#dest_url_warning")

        self.set_input_value("#image_data", os.path.dirname(os.path.dirname(os.path.abspath(__file__))) +
                                            ad_data["config"]["nosetests"]["test-data-path"].strip("/") +
                                            ad_data["config"]["nosetests"]["resources-relative2data-path"] +
                                            ad_data['image'])
        self.wait_until_clickable_xpath(
                    "//div[@aria-labelledby='ui-dialog-title-dialog-add-ad']//button/span[text()='Save']").click()
        # if first click in 'Save' button doesn't work, try once more
        try:
            self.wait_until_clickable_css("#display_url")
            self.wait_until_visible_xpath(
                        "//td[contains(@title, '" + ad_data.get("DisplayURL", "") + "') and contains(@title, '"
                        + ad_data.get("DestinationURL", "") + "') and contains(@title, 'Image ad')]")
        except TimeoutException:
            self.wait_until_clickable_xpath(
                    "//div[@aria-labelledby='ui-dialog-title-dialog-add-ad']//button/span[text()='Save']").click()
            self.wait_until_clickable_css("#display_url")
            self.wait_until_visible_xpath(
                        "//td[contains(@title, '" + ad_data.get("DisplayURL", "") + "') and contains(@title, '"
                        + ad_data.get("DestinationURL", "") + "') and contains(@title, 'Image ad')]")

    def fillin_text_ad(self, ad_data):
        self.set_input_value("#title", ad_data['Title'])
        self.set_input_value("#line1", ad_data['Line1'])
        if ad_data.get("Line2", None):
            self.set_input_value("#line2", ad_data['Line2'])
        self.set_input_value("#display_url", ad_data['DisplayURL'])
        self.set_input_value("#dest_url", ad_data['DestinationURL'])
        self.wait_until_clickable_xpath("//div[@aria-labelledby='ui-dialog-title-dialog-add-ad']//button/span[text()='Save']").click()
        eq_(True, self.grid.find_text_ad(ad_data), "Ad found")

    def get_pod_title_assembled_from_data(self, ad_data):
        return ad_data.get("Title", "") + ad_data.get("Line1", "") + ad_data.get("Line2", "") + \
                ad_data.get("DisplayURL", "") + ad_data.get("DestinationURL", "")

    def delete_ad(self, ad_data):
        title = self.get_pod_title_assembled_from_data(ad_data)
        self.wait_until_presence_of_all_css("table[role='grid'] > tbody > tr > td[role='gridcell']")

        self.wait_until_clickable_xpath("//td[contains(@title, '" + title +
                                        "')]/../td[@aria-describedby='ad_grid_bulk']/input").click()

        self.select_dropdown_css_by_value("#ad_action", "statusDelete")
        self.wait_until_clickable_css('div.ns-filter-buttons > input[value="Ok"]').click()
        self.wait4loader2disappear()
        ads_amount_after_delete = len(self.wait_until_presence_of_all_xpath(
            "//td[contains(@title, '" + title + "')]/../td[@aria-describedby='ad_grid_bulk']/input"))

    def clone_ad(self, ad2clone, clone_ad_data):
        title = self.get_pod_title_assembled_from_data(ad2clone)
        clone_xpath = "//td[contains(@title, '" + title + \
                      "')]/../td[@aria-describedby='ad_grid_ad']/div/img[@title='Clone this Ad']"
        self.wait_until_clickable_xpath(clone_xpath).click()
        if clone_ad_data.get('Type') == 'Image':
            self.fillin_image_ad(clone_ad_data)
        if clone_ad_data.get('Type') == 'Text':
            self.fillin_text_ad(clone_ad_data)

    def get_pods_with_title(self, title):
        return self.wait_until_presence_of_all_xpath(
            "//td[contains(@title, '" + title + "')]/../td[@aria-describedby='ad_grid_bulk']/input")

    def add_venue(self, venue_data, venue_id=None):
        """
        Add venue for client
        """
        self.venue = Venue(driver=self.driver)
        self.goto_tab("Venues")
        
        self.wait_until_clickable_css("input[value='Add Venue']").click()
        self.wait_until_present_xpath("//h2[contains(text(),'Add New Venue')]")
        self.goto_tab('Basic Venue Information')

        self.venue.set_basic_info(venue_data, venue_id)
        if venue_data.get('NETWORK') not in ['Video', 'Social']:
            self.goto_tab("Jigawatt Settings")
            self.venue.set_jigawatts(venue_data['JIGAWATTS'])

        self.goto_tab("Budget & Automation")
        self.venue.set_budget_and_automation(venue_data)
        self.venue.submit()

        try:
            self.wait_until_clickable_xpath("//div[text()='View Client Venues']")
        except UnexpectedAlertPresentException as ex:
            self.accept_js_alert()

    def map_pod_on_venue(self, venue_id, pod_id, pod_network, campaign_name=''):
        """
        Add pod for client venue
        """
        self.venue = Venue(driver=self.driver)
        self.goto_tab("Venues")
        self.goto_venue(campaign_name)
        self.wait_until_clickable_css("#edit_exsisting_pods").click()
        self.wait_until_clickable_css("input.search").click()
        self.set_input_value("input.search", pod_id + "\n")
        self.wait_until_clickable_xpath("//div[contains(@class, 'available')]//li[contains(text(),'" + pod_id + "')]/a")\
            .click()
        self.wait_until_not_visible_xpath("//div[contains(@class, 'available')]//li[contains(text(),'" + pod_id + "')]/a")
        self.wait_until_clickable_xpath("//div[contains(@class, 'selected')]//li[contains(text(),'" + pod_id + "')]/a")
        self.implicit_waiter(5)
        try:
            self.wait_until_clickable_css("input[type='submit'][value='Save Changes']").click()
        except UnexpectedAlertPresentException:
            # self.wait.until(EC.alert_is_present()).accept()
            self.accept_js_alert()
        # bloody alert capturing
        self.wait_until_clickable_css("input[value*='Go Back']").click()
        self.wait_until_present_css("td[aria-describedby='pod_grid_pod_name'][title*='" + pod_id + "']")

    def open_edit_details4client(self, client_name):
        """
        Open client details in edit mode.
        """
        self.wait_until_present_css("#edit_client_details")
        self.driver.execute_script("$(\"#edit_client_details\").click()")
        self.wait_until_present_xpath("//h2/a[contains(text(),'" + client_name + "')]")

    def submit_update_client_info(self):
        """
        Submit client edit form
        """

        self.wait_until_present_xpath("//input[@name='update_client']")
        self.driver.execute_script("$(\"input[name='update_client']\").click()")
        self.driver.implicitly_wait(10)
        self.accept_js_alert()
#         WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.CSS_SELECTOR, "#edit_client_details")))

    def validate_client_info(self, client_info):
        """
        Validate client info in edit mode
        """
        self.goto_tab('Basic Client Information')
        self.wait_until_clickable_css("input[name='client_name']")
        eq_(self.wait_until_clickable_css("input[name='client_name']").get_attribute("value"), client_info["ClientName"])
        eq_(self.wait_until_clickable_css("select[name='timezoneId'] option[selected='selected']").text, client_info["TimeZone"])
        eq_(self.wait_until_clickable_css("select[name='clientAccountRepId'] option[selected='selected']").text, client_info["ClientAccountRep"])
        eq_(self.wait_until_clickable_css("select[name='businessSegmentId'] option[selected='selected']").text, client_info["BusinessSegment"])
        eq_(self.wait_until_clickable_css("input[name='companyName']").get_attribute("value"), client_info["CompanyName"])
        eq_(self.wait_until_clickable_css("input[name='webSite']").get_attribute("value"), client_info["Website"])
        eq_(self.wait_until_clickable_css("input[name='companyAbout']").get_attribute("value"), client_info["AboutCompany"])
        eq_(self.wait_until_clickable_css("input[name='salesInfo']").get_attribute("value"), client_info["SalesInfo"])
        eq_(self.wait_until_clickable_css("input[name='association']").get_attribute("value"), client_info["Association"])
        eq_(self.wait_until_clickable_css("input[name='extendedPrograms']").get_attribute("value"), client_info["LocalExtendPrograms"])

        self.goto_tab('Budget')
        eq_(self.wait_until_clickable_css("select[name='salesPackageId'] option[selected='selected']").text, client_info["SalesPackage"])

    def set_client_info(self, client_info):
        """
        Set client info in edit mode
        :param client_info: dictionary from JSON file
        """
        self.goto_tab('Basic Client Information')
        self.wait_until_clickable_css("input[name='client_name']")
        self.set_input_value("input[name='client_name']", client_info["ClientName"])
        Select(self.wait_until_clickable_css("select[name='timezoneId']")).select_by_visible_text(client_info["TimeZone"])
        Select(self.wait_until_clickable_css("select[name='clientAccountRepId']")).select_by_visible_text(client_info["ClientAccountRep"])
        Select(self.wait_until_clickable_css("select[name='businessSegmentId']")).select_by_visible_text(client_info["BusinessSegment"])
        self.set_input_value("input[name='companyName']", client_info["CompanyName"])
        self.set_input_value("input[name='webSite']", client_info["Website"])
        self.set_input_value("input[name='companyAbout']", client_info["AboutCompany"])
        self.set_input_value("input[name='salesInfo']", client_info["SalesInfo"])
        self.set_input_value("input[name='association']", client_info["Association"])
        self.set_input_value("input[name='extendedPrograms']", client_info["LocalExtendPrograms"])

        self.goto_tab('Budget')
        Select(self.wait_until_clickable_css("select[name='salesPackageId']")).select_by_visible_text(client_info["SalesPackage"])
        self.accept_js_alert()
        self.wait_until_clickable_css("input[name='digitalMediaFunds']").clear()
        self.accept_js_alert()

        media_funds = self.wait_until_clickable_css("div[id*='venue-budget-total']:not([style='display: none;']) > div.ns-value > span.budgetValueSpan").text[:-3].replace(",", "")
        self.wait_until_clickable_css("input[name='digitalMediaFunds']").send_keys(media_funds)

    def open_history_details4client(self):
        """
        Open history for client updates from client info.
        """
        self.wait_until_clickable_css("input[value='View Client History']").click()
        self.wait.until(lambda driver: "/history/client_id/" in driver.current_url)

    def validate_history_row(self, rownum, row2validate):
        """
        Validate history message for a given row
        """
        row_selector = "tr[role='row']:nth-of-type(" + str(rownum) + ")"
        self.wait_until_present_css(row_selector + " td[aria-describedby='history_grid_fullName']")
        eq_(self.wait_until_clickable_css(row_selector + " td[aria-describedby='history_grid_fullName']").text, row2validate[0])
        eq_(self.wait_until_clickable_css(row_selector + " td[aria-describedby='history_grid_fk_historyActionId']").text, row2validate[1])
        eq_(self.wait_until_clickable_css(row_selector + " td[aria-describedby='history_grid_oldValue']").text, row2validate[2])
        eq_(self.wait_until_clickable_css(row_selector + " td[aria-describedby='history_grid_newValue']").text, row2validate[3])

    def validate_client_search(self, client_id):
        self.wait_until_clickable_xpath("//span[(@class='hl') and (text()='" + client_id + "')]")

    def validate_venue_search(self, venue_id):
        self.wait_until_clickable_css("td[title*='" + venue_id + "'][aria-describedby='venue_grid_id']")

    def validate_ad_search(self, ad_data):
        self.title_string = "Title:" + ad_data['Title'] + "Line1 :" + ad_data['Line1'] + "Line2 :" + ad_data['Line2'] + \
                            "Dest URL:" + ad_data['DestinationURL'] + "Display URL: " + ad_data['DisplayURL']
        self.wait_until_clickable_css("td[aria-describedby='text_ad_grid_ad_id'][title='" + self.title_string + "']")

    def get_platform_id(self):
        return self.wait_until_clickable_css("#tabs-1 > div:nth-of-type(2) > div:nth-of-type(2)").text

    def goto_client_in_header(self, client_name):
        self.wait_until_clickable_xpath("//div[contains(@class, 'ns-tabs')]/h4/a[contains(text(), '" +
                                        client_name + "')]").click()
        self.wait_until_present_xpath("//div[@id='ns-body-c']/div[@class='ns-metainfo']/div[text()='View Client Info']")

    def validate_venue_in_budget(self, venue_id):
        self.wait_until_present_xpath("//table[@id='venue-budget-table']/tbody/tr/th[contains(text(), '" +
                                      venue_id + "')]")

    def validate_client_info_venues(self, venues_list):
        i = 1
        venues_in_client_info = self.wait_until_presence_of_all_css("#venue-budget-table tr")
        for venue in venues_list:
            eq_(venues_in_client_info[i].find_element_by_css_selector("th").text, venue["Template"])
            eq_(venues_in_client_info[i].find_element_by_css_selector("td:nth-of-type(1)").text, venue["Network"])
            eq_(venues_in_client_info[i].find_element_by_css_selector("td:nth-of-type(2)").text, venue["Platform"])
            i += 1

    def nav_to_ad_extensions_page(self, client, campaign):
        self.goto_tab('Client Center')
        self.goto_client(client)
        self.goto_venue(campaign)
        self.goto_tab('Ad Extensions')
