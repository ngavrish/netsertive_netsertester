from .model.grid import Grid
from .model.djfusion import DJFusionModel
from .model.client import Client
from ..generator_test import GeneratorTest, CheckLogsWebUID
from .model.flux_capacitor import FluxCapacitor


class OnboardClient_Test(GeneratorTest):
    """
    Covers basic onboarding coverage
    """

    # GENERATOR
    @CheckLogsWebUID("Onboard Client Test")
    def onboard_client(self, client_data, pending_updates, webuid):
        """
        Onboard Client Test
        """
        # set come up with unique names
        self.driver = self.get_web_driver()
        self.amc_model = FluxCapacitor(driver=self.driver)
        self.client_model = Client(driver=self.driver)
        self.djfusion_model = DJFusionModel(driver=self.driver)
        self.grid = Grid(driver=self.driver)
        self.driver.get(self.web_ui_credentials.url)
        self.driver.maximize_window()

        self.amc_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)
        self.amc_model.goto_tab('Client Center')

        self.djfusion_model.start_djfusion(client_data['client_info']['ClientName'])

        client_data['client_info']['ClientName'] += webuid
        client_data['client_info']['CompanyName'] += webuid

        self.djfusion_model.set_client_information(client_data['client_info'])
        self.djfusion_model.clientinfo_publish_continue()
        self.djfusion_model.close_alert()
        self.djfusion_model.goto_clientinfo_tab("Addresses")
        self.djfusion_model.clientinfo_publish_continue()
        self.djfusion_model.close_alert()
        self.djfusion_model.goto_clientinfo_tab("Contacts")
        self.djfusion_model.clientinfo_publish_continue()
        self.djfusion_model.add_jigawatts(client_data['client_info']['Jigawatts'])
        self.djfusion_model.save_and_finish_venue_setup()
        self.djfusion_model.close_alert()
        self.djfusion_model.add_keywords()
        self.djfusion_model.add_venue(client_data['client_info']['BingVenue'])
        self.djfusion_model.add_keywords()
        self.djfusion_model.add_jigawatts(client_data['client_info']['Jigawatts'])
        self.djfusion_model.save_and_finish_venue_setup()
        self.djfusion_model.save_and_finish_pod_setup(client_data['client_info']['CompanyName'])

        CLIENT_NAME = client_data['client_info']['ClientName']
        self.amc_model.goto_client(CLIENT_NAME)
        self.amc_model.goto_tab('Client Info')
        self.client_model.validate_client_info_venues(client_data['client_info']['venues2validate'])
        self.client_model.open_edit_details4client(CLIENT_NAME)
        self.client_model.validate_client_info(client_data['client_info'])

        self.amc_model.goto_request_platform_updates()
        self.grid.search_in_client_name_column(CLIENT_NAME)
        self.grid.validate_client_column_value(CLIENT_NAME, "6")
        self.grid.open_pending_requests(CLIENT_NAME)
        self.grid.validate_pending_requests(pending_updates)
        self.grid.close_pending_requests()
        self.grid.select_client_by_name(CLIENT_NAME)
        self.amc_model.submit_request_update()
        self.grid.validate_grid_empty()
        self.grid.validate_no_client_with_name(CLIENT_NAME)
        self.grid.validate_grid_empty()
        self.amc_model.logout()

    # TEST
    def test(self):
        yield self.onboard_client, \
              self.get_json_data("djfusion_data.json").get('CLIENT_DETAILS'), \
              self.get_json_data("request_updates.json").get('ONBOARDED_CLIENT')