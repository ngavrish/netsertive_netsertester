'''
Created on Dec 23, 2014

@author: Gavrish
'''

from .model.pod import PodModel
from .model.client import Client

from .model.flux_capacitor import FluxCapacitor
from ..generator_test import GeneratorTest, CheckLogsWebUID
from .model.grid import Grid


class AddPod2Venue_Test(GeneratorTest):
    """
    Test covers Adding Pod to New Venue
    """

    # GENERATOR
    @CheckLogsWebUID("Add Pod")
    def add_pod(self, venue, pod, client_name, webuid):
        """
        Add Pod 2 Venue Test
        """
        self.driver = self.get_web_driver()
        self.amc_model = FluxCapacitor(driver=self.driver)
        self.client = Client(driver=self.driver)
        self.grid = Grid(driver=self.driver)
        self.pod_model = PodModel(driver=self.driver)
        self.driver.get(self.web_ui_credentials.url)
        self.driver.maximize_window()

        venue_id = venue["VENUE_NAME"] + webuid
        pod_id = "pod_" + webuid
        campaign_name = venue_id + venue["CAMPAIGN_NAME"]

        self.amc_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)
        self.amc_model.goto_tab('Client Center')
        self.amc_model.goto_client(client_name)
        self.client.add_venue(venue, venue_id=venue_id)
        # to implement:
        self.amc_model.goto_tab('Pod Center')
        self.pod_model.create_pod(pod=pod, name=pod_id)

        self.amc_model.goto_tab('Client Center')
        self.amc_model.goto_client(client_name)
        self.client.map_pod_on_venue(venue_id=venue_id, pod_id=pod_id,
                                     pod_network=pod['network'], campaign_name=campaign_name)
        self.amc_model.logout()

    # TEST
    def test(self):
        yield self.add_pod, \
            self.get_json_data("venues.json")['GOOGLE_DISPLAY'], \
            self.get_json_data("pod_data.json")["POD_INFO_DISPLAY"]["pod_1"],\
            self.get_json_data("amc_data.json")['CLIENT_4VENUES_DETAILS']['name_pref']

        yield self.add_pod, \
            self.get_json_data("venues.json")['GOOGLE_SEARCH'], \
            self.get_json_data("pod_data.json")["POD_INFO_SEARCH"]["pod_1"],\
            self.get_json_data("amc_data.json")['CLIENT_4VENUES_DETAILS']['name_pref']

        yield self.add_pod, \
            self.get_json_data("venues.json")['GOOGLE_VIDEO'], \
            self.get_json_data("pod_data.json")["GOOGLE_VIDEO"]["pod_1"],\
            self.get_json_data("amc_data.json")['CLIENT_4VENUES_DETAILS']['name_pref']

        yield self.add_pod, \
            self.get_json_data("venues.json")['BING_SEARCH'], \
            self.get_json_data("pod_data.json")["BING_SEARCH"]["pod_1"],\
            self.get_json_data("amc_data.json")['CLIENT_4VENUES_DETAILS']['name_pref']

        yield self.add_pod, \
            self.get_json_data("venues.json")['BRIGHTROLL_VIDEO'], \
            self.get_json_data("pod_data.json")["BRIGHTROLL_VIDEO"]["pod_1"],\
            self.get_json_data("amc_data.json")['CLIENT_4VENUES_DETAILS']['name_pref']

        yield self.add_pod, \
            self.get_json_data("venues.json")['FACEBOOK_SOCIAL'], \
            self.get_json_data("pod_data.json")["FACEBOOK_SOCIAL"]["pod_1"],\
            self.get_json_data("amc_data.json")['CLIENT_4VENUES_DETAILS']['name_pref']
