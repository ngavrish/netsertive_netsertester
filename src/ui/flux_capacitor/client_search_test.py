from .model.grid import Grid
from .model.djfusion import DJFusionModel
from .model.client import Client
from ..generator_test import GeneratorTest, CheckLogsWebUID
from .model.flux_capacitor import FluxCapacitor


class ClientSearch_Test(GeneratorTest):
    """
    Covers basic client search
    """

    def __init__(self):
        super().__init__()
        self.layout_model = None
        self.djfusion_data = self.get_json_data("djfusion_data.json")
        self.client_id = ''

    @CheckLogsWebUID("Client Search")
    def client_search(self, webuid):
        """
        Client Search Test
        """
        self.driver = self.get_web_driver()
        self.amc_model = FluxCapacitor(driver=self.driver)
        self.client_model = Client(driver=self.driver)
        self.djfusion_model = DJFusionModel(driver=self.driver)
        self.grid = Grid(driver=self.driver)
        self.driver.get(self.web_ui_credentials.url)
        self.driver.maximize_window()

        self.amc_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)
        self.amc_model.goto_tab('Client Center')

        self.djfusion_model.start_djfusion(self.djfusion_data['CLIENT_DETAILS']['client_info']['ClientName'])

        self.djfusion_data['CLIENT_DETAILS']['client_info']['CompanyName'] += webuid
        self.djfusion_data['CLIENT_DETAILS']['client_info']['ClientName'] += webuid

        self.djfusion_model.set_client_information(self.djfusion_data['CLIENT_DETAILS']['client_info'])
        self.client_id = self.client_model.get_input_value(css="#fusion-client-name")

        self.djfusion_model.clientinfo_publish_continue()
        self.djfusion_model.close_alert()
        self.djfusion_model.goto_clientinfo_tab("Addresses")
        self.djfusion_model.clientinfo_publish_continue()
        self.djfusion_model.close_alert()
        self.djfusion_model.goto_clientinfo_tab("Contacts")
        self.djfusion_model.clientinfo_publish_continue()
        self.djfusion_model.add_jigawatts(self.djfusion_data['CLIENT_DETAILS']['client_info']['Jigawatts'])
        self.djfusion_model.add_keywords(keywords=self.djfusion_data['CLIENT_DETAILS']['client_info']['CompanyName'])
        self.djfusion_model.save_and_finish_venue_setup()
        self.djfusion_model.save_and_finish_pod_setup(
            self.djfusion_data['CLIENT_DETAILS']['client_info']['CompanyName'])

        self.grid.search_in_client_name_column(self.djfusion_data['CLIENT_DETAILS']['client_info']['ClientName'])
        self.grid.open_client(self.djfusion_data['CLIENT_DETAILS']['client_info']['ClientName'])
        self.amc_model.goto_tab('Client Info')
        self.client_model.open_edit_details4client(self.djfusion_data['CLIENT_DETAILS']['client_info']['ClientName'])
        self.client_model.validate_client_info(self.djfusion_data['CLIENT_DETAILS']['client_info'])

        # self.amc_model.goto_tab('Client Info')
        self.amc_model.search_by_key(key=self.client_id)
        self.client_model.validate_client_search(client_id=self.client_id)
        self.amc_model.logout()

    def client_search_test(self):
        yield self.client_search