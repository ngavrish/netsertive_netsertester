from ..generator_test import GeneratorTest, CheckLogs
from .model.flux_capacitor import FluxCapacitor
from .model.client import Client
from .model.extension import Extension
from .model.grid import Grid
from .model.pod import PodModel

__author__ = "fthong"


class CreateExtension_Test(GeneratorTest):
    """
    Test covers creating extensions
    """

    # GENERATOR
    @CheckLogs("Create mobile extensions")
    def create_mobile_extension(self, client):
        """
        Create mobile extension test
        """
        self.driver = self.get_web_driver()
        self.amc_model = FluxCapacitor(driver=self.driver)
        self.client = Client(self.driver)
        self.grid = Grid(self.driver)
        self.pod_model = PodModel(driver=self.driver)
        self.extension_model = Extension(driver=self.driver, timeout=10)

        self.driver.maximize_window()
        self.driver.get(self.web_ui_credentials.url)
        self.amc_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)
        self.client.nav_to_ad_extensions_page(client=client, campaign='Search')

        # Create/Validate/Delete
        self.extension_model.create_mobile_extension()
        self.extension_model.validate_create_mobile_extension()
        self.extension_model.cleanup_create_mobile_extension()
        self.amc_model.logout()

    # GENERATOR
    @CheckLogs("Create callout extensions")
    def create_callout_extension(self, client):
        """
        Create callout extension test
        """
        self.driver = self.get_web_driver()
        self.amc_model = FluxCapacitor(driver=self.driver)
        self.client = Client(self.driver)
        self.grid = Grid(self.driver)
        self.pod_model = PodModel(driver=self.driver)
        self.extension_model = Extension(driver=self.driver, timeout=10)

        self.driver.maximize_window()
        self.driver.get(self.web_ui_credentials.url)
        self.amc_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)
        self.client.nav_to_ad_extensions_page(client=client, campaign='Search')

        # Create/Validate/Delete - Mobile Preferred = False
        self.extension_model.create_callout_extension(mobile_preferred=False)
        self.extension_model.validate_create_callout_extension(mobile_preferred=False)
        self.extension_model.cleanup_create_callout_extension()

        # Create/Validate/Delete - Mobile Preferred = True
        self.extension_model.create_callout_extension(mobile_preferred=True)
        self.extension_model.validate_create_callout_extension(mobile_preferred=True)
        self.extension_model.cleanup_create_callout_extension()
        self.amc_model.logout()

    # TEST
    def create_mobile_extension_test(self):
        yield self.create_mobile_extension, \
            self.get_json_data("amc_data.json")['CLIENT_4VENUES_DETAILS']['name_pref']

    def create_callout_extension_test(self):
        yield self.create_callout_extension, \
            self.get_json_data("amc_data.json")['CLIENT_4VENUES_DETAILS']['name_pref']
