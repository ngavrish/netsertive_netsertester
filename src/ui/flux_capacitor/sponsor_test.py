from ..generator_test import GeneratorTest, CheckLogsWebUID
from .model.flux_capacitor import FluxCapacitor
from .model.program_center.program_center import ProgramCenter


class Sponsors_Test(GeneratorTest):
    """
    Test covers Creating different sponsors
    """

    # GENERATOR
    @CheckLogsWebUID("Sponsor")
    def sponsors(self, sponsor_data, webuid):
        """
        create sponsored campaign
        """
        self.driver = self.get_web_driver()
        self.amc_model = FluxCapacitor(driver=self.driver)
        self.program_center = ProgramCenter(driver=self.driver)
        self.driver.maximize_window()

        sponsor_data['name'] += webuid
        self.driver.get(self.web_ui_credentials.url)
        self.amc_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)
        self.amc_model.goto_tab("Program Center")
        self.amc_model.goto_tab("Sponsors")
        self.program_center.goto_sponsor_wizard()
        self.program_center.create_valid_sponsor(sponsor_data)
        self.amc_model.goto_tab("Program Center")
        self.amc_model.goto_tab("Sponsors")
        self.program_center.grid.search_sponsor_by_name(sponsor_data['name'])
        self.program_center.grid.open_sponsor(sponsor_data['name'])
        self.program_center.validate_sponsor_basic(sponsor_data)

    # TEST
    def sponsors_test(self):
        for company_type in self.get_json_data("sponsor.json"):
            yield self.sponsors, self.get_json_data("sponsor.json")[company_type]