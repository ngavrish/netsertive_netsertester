'''
Created on Dec 23, 2014

@author: Gavrish

Updated on Jul 28, 2015 by tclark@netsertive.com
'''
from ..generator_test import GeneratorTest, CheckLogsWebUID
from .model.flux_capacitor import FluxCapacitor
from .model.pod import PodModel


class PodCenterBasic_Test(GeneratorTest):
    """
    Covers basic actions for a Pod center
    """

    # GENERATOR
    @CheckLogsWebUID("Pod Center Basic")
    def pod_center_basic(self, pod_name, pod_keywords, pod_placements, webuid):
        """
        Pod Search Test
        """
        self.driver = self.get_web_driver()
        self.amc_model = FluxCapacitor(driver=self.driver)
        self.pod_model = PodModel(driver=self.driver)

        self.driver.get(self.web_ui_credentials.url)
        self.driver.maximize_window()
        self.amc_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)

        self.amc_model.goto_tab('Pod Center')
        self.pod_model.create_pod(pod=pod_name, name=webuid)
        # Skip Keyword creation for Bing Search, Google Video, and Bright Roll Video
        if pod_keywords is not None:
            self.amc_model.goto_tab('Keywords')
            self.pod_model.add_keywords(pod_keywords)

        # Skip Placement creation for Bing Search, Google Video, and Bright Roll Video
        if pod_placements is not None:
            self.amc_model.goto_tab('Placements')
            self.pod_model.add_placements(pod_placements)
        self.amc_model.logout()

    # TEST
    def bright_roll_video_test(self):
        yield self.pod_center_basic, \
                self.get_json_data("pod_data.json")["BRIGHTROLL_VIDEO"]["pod_1"], \
                None, \
                None

    def google_video_test(self):
        yield self.pod_center_basic, \
                self.get_json_data("pod_data.json")["GOOGLE_VIDEO"]["pod_1"], \
                None, \
                None

    def google_display_test(self):
        yield self.pod_center_basic, \
                self.get_json_data("pod_data.json")["POD_INFO_DISPLAY"]["pod_1"], \
                self.get_json_data("pod_data.json")["POD_INFO_DISPLAY"]["pod_1"]["keywords"], \
                self.get_json_data("pod_data.json")["POD_INFO_DISPLAY"]["pod_1"]["placements"]

    def google_search_test(self):
        yield self.pod_center_basic, \
                self.get_json_data("pod_data.json")["POD_INFO_SEARCH"]["pod_1"], \
                self.get_json_data("pod_data.json")["POD_INFO_SEARCH"]["pod_1"]["keywords"], \
                self.get_json_data("pod_data.json")["POD_INFO_SEARCH"]["pod_1"]["placements"]

    def bing_search_test(self):
        yield self.pod_center_basic, \
                self.get_json_data("pod_data.json")["BING_SEARCH"]["pod_1"], \
                self.get_json_data("pod_data.json")["BING_SEARCH"]["pod_1"]["keywords"], \
                None

    def facebook_social_test(self):
        yield self.pod_center_basic, \
              self.get_json_data("pod_data.json")["FACEBOOK_SOCIAL"]["pod_1"], \
              None, \
              None
