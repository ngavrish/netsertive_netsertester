from ..generator_test import GeneratorTest, CheckLogs
from .model.flux_capacitor import FluxCapacitor
from .model.alert import Alert


class AddAlert_Test(GeneratorTest):
    """
    Test covers searching Venue
    """
    # GENERATOR
    @CheckLogs("Add Alert")
    def add_alert(self, alert_data):
        """
        Add alert test
        """
        self.driver = self.get_web_driver()
        self.amc_model = FluxCapacitor(driver=self.driver)
        self.alert_model = Alert(driver=self.driver, timeout=10)

        self.driver.maximize_window()

        self.driver.get(self.web_ui_credentials.url)
        self.amc_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)
        self.alert_model.add_alert(alert_data)

    # TEST
    def test(self):

        yield self.add_alert, {'title': 'title', 'message': 'message', 'type': 'Ad', 'priority': 'Highest', 'user': 'QA Automation'}