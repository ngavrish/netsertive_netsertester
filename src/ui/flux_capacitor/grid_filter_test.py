"""
Created on June 7, 2015

@author: Gavrish
"""

from ..generator_test import CheckLogsWebUID, GeneratorTest
from .model.flux_capacitor import FluxCapacitor
from .model.client import Client
from .model.pod import PodModel
from .model.grid import Grid


class GridFilter_Test(GeneratorTest):
    """
    Test covers Grid Filtering
    """
    @CheckLogsWebUID("Platform filtering")
    def platform_filter(self, webuid):
        """
        Platform filtering
        """
        self.driver = self.get_web_driver()
        self.driver.maximize_window()
        self.amc_model = FluxCapacitor(driver=self.driver)
        self.client = Client(driver=self.driver)
        self.pod_model = PodModel(driver=self.driver)
        self.grid = Grid(driver=self.driver)
        self.driver.get(self.web_ui_credentials.url)
        self.driver.maximize_window()

        self.amc_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)
        self.amc_model.goto_tab('Client Center')
        self.pod_id = "00_pod_" + webuid
        self.venue_id = "00_venue_" + webuid

        self.platforms = [platform.text for platform in self.grid.get_platforms()]
        for platform in self.platforms:
            if 'latform' not in platform and 'ilter' not in platform:
                print("Platform text = " + platform.split(" ")[0] + "\n\n\n")
                self.grid.status_select('Active')
                self.grid.select_platform(platform)
                self.clients_from_grid = [client.get_attribute('title') for client in self.grid.get_clients()]

    def platform_filter_test(self):
        yield self.platform_filter
