from .model.lyon import LyonModel
from ..generator_test import GeneratorTest, CheckLogs
from .model.performance import PerformanceModel


class StandaloneVideo_Test(GeneratorTest):
    def __init__(self):
        super().__init__(app="lyon")

    @CheckLogs("Video_ad")
    def video_ad(self, client_name, friendly_name='', is_sponsor=False):
        self.driver = self.get_web_driver()
        self.driver.maximize_window()
        self.lyon_model = LyonModel(driver=self.driver)
        self.performance_model = PerformanceModel(driver=self.driver)

        self.driver.get(self.web_ui_credentials.url)
        self.lyon_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)

        self.lyon_model.open_company(client_name, friendly_name, is_sponsor)
        self.lyon_model.select_from_topnav("Performance", "Video Advertising")
        # note this has both graph and grid
        self.lyon_model.check_graph_present()
        self.lyon_model.check_grid_present()
        self.lyon_model.check_footer()
        self.performance_model.select_item_in_dropdown('metric')
        self.lyon_model.check_graph_present()
        self.lyon_model.check_grid_present()
        self.lyon_model.check_footer()
        self.lyon_model.logout()

    # TEST
    def video_ad_test(self):
        yield self.video_ad, "Airport Chrysler Dodge Jeep"
