from ..lyon.model.lyon import LyonModel
from ..generator_test import GeneratorTest, CheckLogs
from ..lyon.model.performance import PerformanceModel

"""
Created on Aug 9, 2015

@author = tclark@netsertive.com
"""


class LyonSmoke_Test(GeneratorTest):
    def __init__(self):
        super().__init__(app="lyon")

    # GENERATOR
    @CheckLogs("Navigation")
    def navigation(self):
        self.driver = self.get_web_driver()
        self.driver.maximize_window()
        self.lyon_model = LyonModel(driver=self.driver)
        self.performance_model = PerformanceModel(driver=self.driver)

        self.driver.get(self.web_ui_credentials.url)
        self.lyon_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)

        self.lyon_model.open_company('Audio Advice')
        self.lyon_model.goto_tab('Performance')

        self.performance_model.select_time_period('Day')
        self.performance_model.select_time_period('Week')
        self.performance_model.select_time_period('Month')
        self.performance_model.click_sidebar('Site Visitors')
        self.performance_model.click_sidebar('CTR')
        self.performance_model.click_sidebar('Conversions')
        self.performance_model.click_sidebar('Share of Voice')
        self.performance_model.click_sidebar('Average Position')
        self.performance_model.click_sidebar('Categories')
        self.performance_model.click_sidebar('Geography')
        self.performance_model.click_sidebar('Email Campaigns')
        self.performance_model.click_sidebar('Impressions')
        self.lyon_model.goto_tab('Strategy')
        self.lyon_model.goto_tab('CAMPAIGNS')
        self.lyon_model.goto_tab('Ads')
        self.lyon_model.goto_tab('Shoppers')
        self.lyon_model.logout()

    # TEST
    def lyon_smoke_test(self):
        yield self.navigation
