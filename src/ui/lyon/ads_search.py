from .model.lyon import LyonModel
from ..generator_test import GeneratorTest, CheckLogs
from .model.ads import AdsModel


class AdSearch_Test(GeneratorTest):
    def __init__(self):
        super().__init__(app="lyon")

    # GENERATOR
    @CheckLogs("ad_search")
    def ads_search(self, client_name, friendly_name='', is_sponsor=False, label_enabled=False):
        self.driver = self.get_web_driver()
        self.driver.maximize_window()
        self.lyon_model = LyonModel(driver=self.driver)
        self.ads_model = AdsModel(driver=self.driver)
        self.driver.get(self.web_ui_credentials.url)
        self.lyon_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)
        self.lyon_model.open_company(client_name, friendly_name, is_sponsor)
        self.lyon_model.select_from_topnav("Ads", "Search Ads")
        self.lyon_model.check_footer()
        self.ads_model.compare_total_count("Search Ads")
        self.ads_model.check_visibility_of_ad("Search Ads", False)
        self.ads_model.open_accordion()
        self.ads_model.check_visibility_of_ad("Search Ads", True)
        self.ads_model.compare_accordion_count("Search Ads")
        self.ads_model.close_accordion()
        self.ads_model.check_visibility_of_ad("Search Ads", False)
        self.lyon_model.logout()

    # TEST
    def ad_search_test(self):
        yield self.ads_search, "Audio Advice (Raleigh)", "Audio Advice"
