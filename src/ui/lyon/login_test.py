"""
Created on Aug 9, 2015

@author = tclark@netsertive.com
"""

from ..lyon.model.lyon import LyonModel
from ..generator_test import GeneratorTest, CheckLogs
from ..model.page_object import Error


class Lyon_Test(GeneratorTest):
    LOGIN_REQUIRED = 'The Login field is required'
    PASSWORD_REQUIRED = 'The Password field is required'
    LOGIN_INCORRECT = 'Incorrect login'

    def __init__(self):
        super().__init__(app="lyon")

    @CheckLogs("LoginTest")
    def login(self, login, password, positive=True, error=None):
        self.driver = self.get_web_driver()
        self.driver.get(self.web_ui_credentials.url)
        self.driver.maximize_window()
        lyon_model = LyonModel(driver=self.driver)
        lyon_model.login(login=login, password=password, positive=positive, error=error)

    def test(self):
        yield self.login, self.web_ui_credentials.login, self.web_ui_credentials.password
        yield self.login, "", "", False, Error(message=self.LOGIN_REQUIRED)
        yield self.login, "123", "", False, Error(message=self.PASSWORD_REQUIRED)
        yield self.login, "123", "123", False, Error(message=self.LOGIN_INCORRECT)