from .model.lyon import LyonModel
from ..generator_test import GeneratorTest, CheckLogs
from .model.performance import PerformanceModel


class SiteVisitors_Test(GeneratorTest):
    def __init__(self):
        super().__init__(app="lyon")

    @CheckLogs("Site_visitors")
    def site_visitors(self, client_name, friendly_name='', is_sponsor=False, label_enabled=False):
        self.driver = self.get_web_driver()
        self.driver.maximize_window()
        self.lyon_model = LyonModel(driver=self.driver)
        self.performance_model = PerformanceModel(driver=self.driver)

        self.driver.get(self.web_ui_credentials.url)
        self.lyon_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)

        self.lyon_model.open_company(client_name, friendly_name, is_sponsor)
        self.lyon_model.select_from_topnav("Performance", "Site Visitors")
        self.lyon_model.check_graph_present()
        self.lyon_model.check_footer()
        if label_enabled:
            self.performance_model.select_item_in_dropdown('label')
            self.lyon_model.check_graph_present()
            self.lyon_model.check_footer()
        self.performance_model.select_item_in_dropdown('network', 1)
        self.lyon_model.check_graph_present()
        self.lyon_model.check_footer()
        self.performance_model.select_item_in_dropdown('network', 2)
        self.lyon_model.check_graph_present()
        self.lyon_model.check_footer()
        self.performance_model.select_item_in_dropdown("sponsorship")
        self.lyon_model.check_graph_present()
        self.lyon_model.check_footer()
        if is_sponsor:
            self.performance_model.select_item_in_dropdown('dealers')
            self.lyon_model.check_graph_present()
            self.lyon_model.check_footer()
        else:
            self.performance_model.select_item_in_dropdown('sponsorship', 2)
            self.lyon_model.check_graph_present()
            self.lyon_model.check_footer()
            self.performance_model.select_item_in_dropdown('sponsorship', 3)
            self.lyon_model.check_graph_present()
            self.lyon_model.check_footer()

        self.lyon_model.logout()

    # TEST
    def site_visitors_test(self):
        yield self.site_visitors, "Audio Advice (Raleigh)", "Audio Advice"
        yield self.site_visitors, "1 Control AV"
        yield self.site_visitors, "Electrolux", '', True, True
