from ..lyon.model.lyon import LyonModel
from ..generator_test import GeneratorTest, CheckLogs
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time
import datetime


class Asset_Test(GeneratorTest):
    def __init__(self):
        super().__init__(app="lyon")
        self.driver = self.get_web_driver()
        self.lyon_model = LyonModel(driver=self.driver)
        self.driver.delete_all_cookies()

    def login(self):
        self.driver.maximize_window()
        size = self.driver.get_window_size()
        # otherwise it has trouble finding checkboxes off the edge of the screen
        self.driver.set_window_size(size['width'] * 1.1, size['height'])
        self.driver.get(self.web_ui_credentials.url)
        self.lyon_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)

    def wait_and_click_helper_disappearing(self, element):
        self.lyon_model.wait.until(EC.element_to_be_clickable((By.XPATH, element))).click()

    def wait_and_click_helper(self, element):
        self.lyon_model.wait.until(EC.element_to_be_clickable((By.XPATH, element))).click()
        self.lyon_model.wait.until(EC.element_to_be_clickable((By.XPATH, element)))

    def click(self, name, type):
        self.wait_and_click_helper("//" + type + "[contains(.,'" + name + "')]")

    def clickDisappearing(self, name, type):
        self.lyon_model.wait.until(EC.element_to_be_clickable((By.XPATH, "//" + type + "[contains(.,'" + name + "')]"))).click()

    def navigateToAssetsAs(self, clientName, friendlyName, isSponsor):
        self.lyon_model.open_company(clientName, friendlyName, isSponsor)
        self.lyon_model.goto_tab("CAMPAIGNS")
        self.wait_and_click_helper("//span[contains(.,'Marketing Assets')]/parent::a")

    def linkFile(self, url, fullName, text):
        print("linking a file")
        self.click("ACTION", "a")
        self.clickDisappearing("from External Link", "a")
        self.lyon_model.set_input_value(selector="input[name=asset_url]", text=url)
        self.lyon_model.set_input_value(selector="input[name=asset_name]", text=fullName)
        self.lyon_model.set_input_value(selector="input[name=asset_description]", text=text)
        print("trying to link")
        # this will fail because missing company_global_key sometimes
        # try exiting all instances of your browser, or logging into lyon, logging out, and closing the tab, or other irrational things
        self.wait_and_click_helper_disappearing('//span[contains(., "Link")]/parent::button')
        print("looking for asset")
        self.lyon_model.wait.until(EC.element_to_be_clickable((By.XPATH, '//div[contains(@title, "' + fullName + '")]')))


    def deleteAsset(self, fullName):
        print("opening asset menu")
        self.wait_and_click_helper('//div[@title="' + fullName + '"]/../div[2]/div[1]')
        print("deleting asset")
        self.wait_and_click_helper_disappearing('//a[contains(.,"Delete")]/parent::li/parent::ul/parent::div/parent::div/parent::div/div[contains(@title, "' + fullName + '")]/parent::div/div[2]/div[2]/ul/li[3]/a');
        self.lyon_model.wait.until(EC.alert_is_present())
        alert = self.driver.switch_to.alert
        alert.accept()
        # cause otherwise it fails
        time.sleep(1)

    # TEST
    def test(self):
        prefix = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
        self.login()

        self.navigateToAssetsAs("Bowers & Wilkins Local Extend", '', True)

        linkName = "doodly boop"
        self.linkFile("http://www.google.com/favicon.ico", prefix + linkName, "Google favicon")

        # TODO: change to uploaded file
        uploadName = "AARG"
        # print("uploading a file")
        # self.click("ACTION", "a")
        # self.clickDisappearing("from File Upload", "a")
        # TODO: file upload
        # print("looking for asset")
        # self.lyon_model.wait.until(EC.element_to_be_clickable((By.XPATH, '//img[contains(@title, "' + prefix + uploadName + '")]/parent::a')))
        # file upload is not easily automatable - get rid of this link when we can do it
        self.linkFile("https://www.facebook.com/favicon.ico", prefix + uploadName, "Other favicon")

        print("creating a folder")
        self.click("ACTION", "a")
        self.clickDisappearing("Create Folder", "a")
        folderName = "test"
        self.lyon_model.set_input_value(selector="input[name=new_folder_name]", text=(prefix + folderName))
        self.wait_and_click_helper_disappearing('//span[contains(., "Create")]/parent::button')
        print("looking for folder")
        self.lyon_model.wait.until(EC.element_to_be_clickable((By.XPATH, '//div[contains(@title, "' + prefix + folderName + '")]')))

        print("moving an asset")
        self.wait_and_click_helper('//input[contains(@value, "' + prefix + uploadName + '")]')
        self.click("ACTION", "a")
        self.clickDisappearing("Move Selected...", "a")
        # cause otherwise it will fail
        time.sleep(1)
        print("opening folder")
        self.wait_and_click_helper_disappearing('//div[contains(@id, "move_dialog")]/div[2]/ul/li/i')
        print("choosing destination")
        self.wait_and_click_helper('//div[contains(@id, "move_dialog")]/div[2]/ul/li/ul/li/a[contains(., "' + prefix + folderName + '")]')
        print("clicking Copy")
        self.wait_and_click_helper_disappearing('//span[contains(., "Move to this Destination")]/parent::button')
        print("looking for folder")
        self.lyon_model.wait.until(EC.element_to_be_clickable((By.XPATH, '//div[contains(@title, "' + prefix + folderName + '")]')))

        print("copying an asset")
        self.wait_and_click_helper('//input[contains(@value, "' + prefix + linkName + '")]')
        self.click("ACTION", "a")
        self.clickDisappearing("Copy Selected ...", "a")
        # cause otherwise it will fail
        time.sleep(1)
        # it seems to remember the previous tree state
        # print("opening folder")
        # self.wait_and_click_helper_disappearing('//div[contains(@id, "copy_dialog")]/div[2]/ul/li/i')
        print("choosing destination")
        self.wait_and_click_helper('//div[contains(@id, "copy_dialog")]/div[2]/ul/li/ul/li/a[contains(., "' + prefix + folderName + '")]')
        print("clicking Copy")
        self.wait_and_click_helper_disappearing('//span[contains(., "Copy to this Destination")]/parent::button')
        print("looking for asset")
        self.lyon_model.wait.until(EC.element_to_be_clickable((By.XPATH, '//div[contains(@title, "' + prefix + linkName + '")]')))

        # navigateToAssetsAs expects you to be on the dashboard
        print("navigating to Dashboard")
        self.lyon_model.goto_tab('Dashboard')
        self.navigateToAssetsAs("Audio Advice (Raleigh)", "Audio Advice", False)
        print("loading Sponsor assets")
        self.wait_and_click_helper_disappearing('//a[@title="Bowers & Wilkins Local Extend"]')
        print("looking for folder")
        self.lyon_model.wait.until(EC.element_to_be_clickable((By.XPATH,'//div[contains(@title, "' + prefix + folderName + '")]')))
        print("looking for asset")
        self.lyon_model.wait.until(EC.element_to_be_clickable((By.XPATH, '//div[contains(@title, "' + prefix + linkName + '")]')))

        print("navigating to Dashboard")
        self.lyon_model.goto_tab('Dashboard')
        self.navigateToAssetsAs("Bowers & Wilkins Local Extend", '', True)

        self.deleteAsset(prefix + linkName)

        print("navigating into folder")
        self.wait_and_click_helper_disappearing('//div[contains(@title, "' + prefix + folderName + '")]/parent::div/parent::div/a')
        self.deleteAsset(prefix + linkName)
        self.deleteAsset(prefix + uploadName)

        print("navigating back out of folder")
        self.wait_and_click_helper("//span[contains(.,'Marketing Assets')]/parent::a")
        self.deleteAsset(prefix + folderName)

        print("logging out")
        self.lyon_model.logout()

# As sponsor:
# * go to Campaigns > Marketing Assets TEST
# 	* "X Folder(s), Y Asset(s)" sticker should be correct at all times
# 	* Select Thumbnail View TEST
# 		* Action > Create folder
# 			* view folder details
# 			* edit folder
# 			* delete folder TEST
# 		* Action > Create Asset from File Upload TEST
# 			* Select File(s)
# 				* Hit X
# 			* Select File(s) TEST
# 				* Select more File(s)
# 				* upload file TEST
# 		* Action > Create Asset from External Link TEST
# 			* cancel with X
# 			* cancel with Cancel
# 			* test too long url
# 			* upload video
# 			* upload generic file TEST
# 		* delete assets TEST
# 		* edit assets
# 		* test that clicking on external link takes you to link in new tab (hard to do in selenium)
# 		* Action > Select All Assets
# 		* Check a variety of checkboxes
# 			* Uncheck some
# 			* Action > Copy Selected...  TEST
# 				* folders can't be copied
# 				* test valid
# 			* Action > Move Selected... TEST
# 				* can't move a folder into itself
# 				* test valid
# 		* Click on folder
# 			* breadcrumbs indicate folder path
# 		* filter by each asset type (Preferrably with an existing asset of each type)
# 		* Verify the stickers for various things
# 		* download assets?
# 	* Select Details View
# 		* Repeat Thumbnail View tests
# 		* Sort by each sortable column, ascending and descending
# 		* verify the data in the columns
# 	* Search
# 		* something that doesn't exist
# 		* something that returns multiple things
# As dealer:
# * go to Campaigns > Marketing Assets
# 	* Click on Sponsor TEST
# 		* "X Folder(s), Y Asset(s)" sticker should be correct
# 		* Select Thumbnail View TEST
# 			* Click on folder
# 			* downloadable asset
# 			* link asset
# 		* Select Details View
# 			* Click on folder
# 			* downloadable asset
# 			* link asset
# 		* Search
# 			* something that doesn't exist
# 			* something that returns multiple things
