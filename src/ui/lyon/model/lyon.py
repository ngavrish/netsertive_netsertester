from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from ....ui.model.page_object import PageObject


class LyonModel(PageObject):
    """
    Object model for Lyon
    General functionality
    """

    def __init__(self, driver, timeout=None):
        super().__init__(driver, timeout)

    def login(self, login, password, positive=True, error=None):
        """
        Login to LYON
        """
        self.set_input_value(selector="input#username", text=login)
        self.set_input_value(selector="input#password", text=password)
        self.wait_until_clickable_css("button#login_button").click()
        if positive:
            self.wait_until_present_xpath("//a[contains(.,'My Account')]")
            print("Successfully logged into to Lyon as: " + login)
        else:
            self.wait_until_visible_css("div#errors")

    def logout(self):
        """
        Logout from LYON
        """
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//a[contains(.,'Logout')]"))).click()
        self.wait.until(EC.presence_of_element_located((By.XPATH, "//div[contains(.,'You have been successfully logged out.')]")))
        self.wait.until_not(EC.presence_of_element_located((By.XPATH, "//a[contains(.,'Logout')]")))
        print("Successfully logged out of Lyon")

    def open_company(self, client_name, friendly_name = '', is_sponsor = False):
        """
        Switch to a certain client and check for welcome message
        """
        self.set_input_value("input#company_autocomplete", client_name)
        self.wait_until_clickable_xpath("//ul[contains(@class, 'ui-autocomplete')]/li[contains(@class,'ui-menu-item')]"
                                        "/a[contains(.,'" + client_name + "')]").click()
        # sponsors do not display a name at all!
        if is_sponsor:
            self.wait_until_present_xpath("//h1[contains(.,'Welcome!')]")
        elif friendly_name:
            self.wait_until_present_xpath("//h1[contains(.,'Welcome, " + friendly_name + "')]")
        else:
            self.wait_until_present_xpath("//h1[contains(.,'Welcome, " + client_name + "')]")
        print("Successfully opened " + str.capitalize(client_name))

    def goto_tab(self, tab):
        top_nav_tab_xpath = "//div/ul/li/a[contains(.,'" + tab + "')]"
        self.wait.until(
            EC.element_to_be_clickable((By.XPATH, top_nav_tab_xpath))).click()
        self.wait.until(
            EC.element_to_be_clickable((By.XPATH, top_nav_tab_xpath)))
        print("Successfully loaded " + str.capitalize(tab) + " tab")

    def select_from_topnav(self, tab, item):
        """
        Function to click a menu item in a topnav tab menu
        :param tab: topnav tab that menu item is in
        :param item: menu item to click
        """
        top_nav_tab_xpath = "//div/ul/li/a[contains(.,'" + tab + "')]"
        item_nav_xpath = "//div/ul/li[a[contains(.,'" + tab + "')]]//li/a[contains(.,'" + item + "')]"
        element = self.wait.until(
            EC.element_to_be_clickable((By.XPATH, top_nav_tab_xpath)))
        ActionChains(self.driver).move_to_element(element).perform()
        self.wait.until(
            EC.element_to_be_clickable((By.XPATH, item_nav_xpath))).click()
        print("Successfully loaded " + str.capitalize(item) + " item in " + str.capitalize(tab) + " tab")

    def check_graph_present(self, must_be_graph=False):
        # Option for enforcing graph must exist, for example for known good date range data test
        if must_be_graph:
            self.wait_until_present_xpath('//div[contains(@id,"graph_container")]//div[contains(@aria-label,"A chart.")]')
        # Default to just checking if the graph area has loaded (graph OR no data message)
        else:
            self.wait_until_present_xpath('//div[contains(@id,"graph_container")]//div[contains(@aria-label,"A chart.")]|//h3[contains(.,"No data was found for this date range.")]|//h3[contains(.,"No data was found with the applied filters.")]')

    def check_grid_present(self, must_be_grid=False):
        # Option for enforcing grid must exist, for example for known good date range data test
        if must_be_grid:
            self.wait_until_present_xpath("//div[contains(@class,'ui-jqgrid-bdiv')]")
        else:
            self.wait_until_present_xpath("//div[contains(@class,'ui-jqgrid-bdiv')]|//h3[contains(.,'No data was found for this date range.')]|//h3[contains(.,'No data was found with the applied filters.')]")

    def check_footer(self):
        # Wait for the bottom of the page to load to make sure entire HTML is loaded
        self.wait.until(
            EC.presence_of_element_located((By.XPATH, "//div[contains(@id,'connect')]")))

    def set_date_picker_to_last_month(self):
        self.wait_until_clickable_xpath("//a[@id='dialog_link_dr']").click()
        self.wait_until_clickable_xpath("//input[@value='10']").click()
        self.wait_until_clickable_xpath("//button[contains(., 'Save')]").click()

        # TODO clean up and make this function work for all date ranges
        # def update_date_selector(self, selector):
        #     print("Update and save " + selector + " date selector")
        #
        #     element = []
        #     if selector == 'Last 7 Days':
        #         element.append("//input[@value='4']")
        #     if selector == 'Last Week':
        #         element.append("")
        #     if selector == 'This Month':
        #         element.append("")
        #     if selector == 'Last 30 Days':
        #         element.append("")
        #     if selector == 'Last Month':
        #         element.append("")
        #     if selector == 'Custom':
        #         element.append("")
        #
        #     # click Change date range element
        #     self.driver.find_element_by_xpath("//span[contains(.,'Change')]").click()
        #     # select date range radio button
        #     self.wait_and_click_helper(element[0])
        #     # click save
        #     self.driver.find_element_by_xpath("//button[contains(.,'Save')]").click()
        #
        #     print("Updated and saved " + selector + " date selector")
