'''
Created on Aug 9, 2015

@author: cwysocki@netsertive.com
'''

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from ...model.page_object import PageObject
import re


class AdsModel(PageObject):
    """
    Object model for Lyon Performance Page
    """

    def __init__(self, driver, timeout=None):
        super().__init__(driver, timeout)

    def wait_and_click_helper(self, element):
        self.wait.until(EC.element_to_be_clickable((By.XPATH, element)))
        self.driver.find_element_by_xpath(element).click()

    def get_listed_amount(self, element):
        amount = int(self.driver.find_element_by_xpath("//span[contains(.,'" + element +
                                                       "')]/span[contains(@class,'nav_element_value')]").text)
        return amount

    def compare_total_count(self, left_nav):
        actual_count = self.get_count_of_ads(left_nav)
        listed_count = self.get_listed_amount(left_nav)
        if actual_count != listed_count:
            raise ValueError(left_nav + " found do not match " + left_nav + " listed.")
        print("Successfully compared the total " + left_nav + " to the total listed " + left_nav)

    def compare_accordion_count(self, left_nav):
        actual_count = None
        if left_nav == "Search Ads":
            actual_count = len(self.driver.find_elements_by_xpath("//*[@id='generic_section']/div[3]" +
                               "/div[1]//div[contains(@class,'text_ad_landing_page')]"))
        elif left_nav == "Display Ads":
            actual_count = len(self.driver.find_elements_by_xpath("//*[@id='generic_section']/div[3]" +
                                                                  "/div[1]//div[contains(@class,'ns_img_ad')]"))
        listed_count = self.driver.find_element_by_xpath("//*[@id='ui-accordion-1-header-0']").text
        listed_count = re.findall('\(([0-9+])\)', listed_count)
        listed_count = int(listed_count[0])
        assert listed_count == actual_count
        if listed_count != actual_count:
            raise ValueError(left_nav + " found in first accordion do not match " + left_nav +
                             " listed in first accordion.")
        print("Successfully compared count of " + left_nav + " in accordion to listed ads in accordion")

    def check_visibility_of_ad(self, left_nav, clicked):
        """
        Function to check if an ad should be visible
        :param left_nav: the current left nav button being worked with
        :param clicked: Boolean determining if the accordion has been clicked
        """
        element = None
        if left_nav == "Search Ads":
            if clicked:
                self.wait.until(EC.element_to_be_clickable((By.XPATH, "//*[@id='generic_section']/div[3]" +
                                                            "/div[1]//div[contains(@class," +
                                                            "'text_ad_landing_page')]")))
            element = self.driver.find_element_by_xpath("//*[@id='generic_section']/div[3]" +
                                                        "/div[1]//div[contains(@class,'text_ad_landing_page')]")
        elif left_nav == "Display Ads":
            if clicked:
                self.wait.until(EC.element_to_be_clickable((By.XPATH, "//*[@id='generic_section']/div[3]" +
                                                            "/div[1]//div[contains(@class,'ns_img_ad')]")))
            element = self.driver.find_element_by_xpath("//*[@id='generic_section']/div[3]" +
                                                        "/div[1]//div[contains(@class,'ns_img_ad')]")
        visible = element.is_displayed()
        if not visible and clicked:
            raise ValueError(left_nav + " were not visible when accordions were clicked")
        elif visible and not clicked:
            raise ValueError(left_nav + " were visible when accordions were not clicked")
        print("Successfully checked the visibility of " + left_nav)

    def get_count_of_ads(self, left_nav):
        count = None
        if left_nav == "Search Ads":
            count = len(self.driver.find_elements_by_xpath("//div[contains(@class,'text_ad_landing_page')]"))
        elif left_nav == "Display Ads":
            count = len(self.driver.find_elements_by_xpath("//a[contains(@class,'image_ad_landing_page')]"))
        print("number of " + left_nav + " found: " + str(count))
        return count

    def click_sidebar(self, button):
        if button == 'Search Ads':
            self.wait_and_click_helper("//span[contains(@class,'search')]")
        elif button == 'Ad Extension':
            self.wait_and_click_helper("//span[contains(@class,'extensions')]")
        elif button == 'Display Ads':
            self.wait_and_click_helper("//span[contains(@class,'display')]")
        elif button == 'Video Ads':
            self.wait_and_click_helper("//span[contains(@class,'video')]")
        print("Successfully loaded: Ads > " + button + " Sidebar ")

    def select_time_period(self, period):
        self.wait_and_click_helper("//span[contains(.,'" + period + "')]")
        print("Successfully loaded " + period)

    def open_accordion(self):
        self.wait_and_click_helper("//*[@id='ui-accordion-1-header-0']/" +
                                   "span[contains(@class,'ui-accordion-header-icon ui-icon ui-icon-triangle-1-e')]")

    def close_accordion(self):
        self.wait_and_click_helper("//span[contains(@class,'ui-accordion-header-icon ui-icon ui-icon-triangle-1-s')]")
        self.wait_until_not_visible_xpath("//*[@id='ui-accordion-1-panel-0']")
