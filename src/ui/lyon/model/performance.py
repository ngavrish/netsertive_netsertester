'''
Created on Aug 9, 2015

@author: tclark@netsertive.com
'''

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from ...model.page_object import PageObject


class PerformanceModel(PageObject):
    """
    Object model for Lyon Performance Page
    """

    def __init__(self, driver, timeout=None):
        super().__init__(driver, timeout)

    def wait_and_click_helper(self, element):
        self.wait.until(EC.element_to_be_clickable((By.XPATH, element)))
        self.driver.find_element_by_xpath(element).click()
        self.wait.until(EC.element_to_be_clickable((By.XPATH, element)))

    def click_sidebar(self, button):
        if button == 'Conversions':
            self.wait_and_click_helper("//span[contains(@class,'conv')]")
        elif button == 'Impressions':
            self.wait_and_click_helper("//span[contains(@class,'imps')]")
        elif button == 'Site Visitors':
            self.wait_and_click_helper("//span[contains(@class,'clicks')]")
        elif button == 'CTR':
            self.wait_and_click_helper("//span[contains(@class,'ctr')]")
        elif button == 'Share of Voice':
            self.wait_and_click_helper("//span[contains(.,'Share of Voice')]")
        elif button == 'Average Position':
            self.wait_and_click_helper("//span[contains(@class,'avg_pos')]")
        elif button == 'Categories':
            self.wait_and_click_helper("//span[contains(@class,'cats')]")
        elif button == 'Geography':
            self.wait_and_click_helper("//span[contains(@class,'geo')]")
        elif button == 'Video Advertising':
            self.wait_and_click_helper("//span[contains(@class,'video')]")
        elif button == 'My Dealers':
            self.wait_and_click_helper("//span[contains(@class,'dealers')]")
        elif button == 'Landing Pages':
            self.wait_and_click_helper("//span[contains(@class,'landing_pages')]")
        print("Successfully loaded: Performance > " + button + " Sidebar ")

    def select_time_period(self, period):
        self.wait_and_click_helper("//span[contains(.,'" + period + "')]")
        print("Successfully loaded " + period)

    def select_item_in_dropdown(self, dropdown, dropdown_item=1):
        """
        :param dropdown: dropdown to select from
        :param dropdown_item: dropdown item to select; defaults to just not the first one
        :return: none
        """
        if dropdown == 'label':
            self.select_dropdown_css_by_index("select#sponsor_network_label_id", dropdown_item)
        elif dropdown == 'network':
            self.select_dropdown_css_by_index("select[name='perf_graph_type']", dropdown_item)
        elif dropdown == 'sponsorship':
            self.select_dropdown_css_by_index("select#sponsorship_type", dropdown_item)
        elif dropdown == 'dealers':
            self.select_dropdown_css_by_index("select#dealers", dropdown_item)
        elif dropdown == 'metric':
                self.select_dropdown_css_by_index("select[name='graph_metric_type']", dropdown_item)