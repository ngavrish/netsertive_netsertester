__author__ = '802619'

'''
Created on Aug 9, 2015

@author: tclark@netsertive.com
'''

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from .lyon import LyonModel

class MyAccount(LyonModel):
    """
    Object model for Lyon
    General functionality
    """

    def __init__(self, driver):
        super().__init__(driver)

    def go_to_vertical_menu(self, text):
        print("//div[@id='col1']//a/span[contains(text(), '" + text + "')]")
        self.wait.until(EC.element_to_be_clickable((By.XPATH,
                        "//div[@id='col1']//a/span[contains(text(), '" + text + "')]"))).click()
        self.wait.until(EC.presence_of_element_located((By.XPATH,
                        "//div[@id='generic_section']/h1[contains(text(), '" + text + "')]")))

    def add_recipient(self, type_, email):
        self.wait.until(EC.element_to_be_clickable((By.XPATH,
                        "//div[@class='email_container']//h2[contains(text(), '" + type_ + "')]/../a"))).click()
        self.wait.until(EC.presence_of_element_located((By.XPATH,
                        "//span[(@class='ui-dialog-title') and (contains(text(), 'Add email recipient'))]")))
        self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,
                        "#email")))
        self.set_input_value("#email", email)
        self.wait.until(EC.element_to_be_clickable((By.XPATH,
                        "//button/span[contains(text(), 'Add Email Recipient')]/.."))).click()
        self.wait.until(EC.alert_is_present())
        self.accept_js_alert()