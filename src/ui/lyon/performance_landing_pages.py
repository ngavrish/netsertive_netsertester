from .model.lyon import LyonModel
from ..generator_test import GeneratorTest, CheckLogs
from .model.performance import PerformanceModel


class LandingPages_Test(GeneratorTest):
    def __init__(self):
        super().__init__(app="lyon")

    @CheckLogs("Landing_pages")
    def landing_pages(self, client_name, company_name='', is_sponsor=False):
        self.driver = self.get_web_driver()
        self.driver.maximize_window()
        self.lyon_model = LyonModel(driver=self.driver)
        self.performance_model = PerformanceModel(driver=self.driver)

        self.driver.get(self.web_ui_credentials.url)
        self.lyon_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)

        self.lyon_model.open_company(client_name, company_name, is_sponsor)
        self.lyon_model.select_from_topnav("Performance", "Landing Pages")
        self.lyon_model.check_grid_present()
        self.lyon_model.check_footer()

        self.lyon_model.logout()

    # TEST
    def landing_pages_test(self):
        yield self.landing_pages, "Electrolux", '', True
