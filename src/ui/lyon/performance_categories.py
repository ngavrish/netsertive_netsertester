from .model.lyon import LyonModel
from ..generator_test import GeneratorTest, CheckLogs
from .model.performance import PerformanceModel


class Categories_Test(GeneratorTest):
    def __init__(self):
        super().__init__(app="lyon")

    # GENERATOR
    @CheckLogs("Left_nav_categories_test")
    def categories(self, client_name, friendly_name='', is_sponsor=False, label_enabled=False):
        self.driver = self.get_web_driver()
        self.driver.maximize_window()
        self.lyon_model = LyonModel(driver=self.driver)
        self.performance_model = PerformanceModel(driver=self.driver)
        self.driver.get(self.web_ui_credentials.url)
        self.lyon_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)

        self.lyon_model.open_company(client_name, friendly_name, is_sponsor)
        self.lyon_model.select_from_topnav("Performance", "Categories")
        self.lyon_model.check_grid_present()
        self.lyon_model.check_footer()
        self.performance_model.select_item_in_dropdown('network', 1)
        self.lyon_model.check_grid_present()
        self.lyon_model.check_footer()
        self.performance_model.select_item_in_dropdown('network', 2)
        self.lyon_model.check_grid_present()
        self.lyon_model.check_footer()
        self.performance_model.select_item_in_dropdown("sponsorship")
        self.lyon_model.check_grid_present()
        self.lyon_model.check_footer()
        if not is_sponsor:
            self.performance_model.select_item_in_dropdown('sponsorship', 2)
            self.lyon_model.check_grid_present()
            self.lyon_model.check_footer()
            self.performance_model.select_item_in_dropdown('sponsorship', 3)
            self.lyon_model.check_grid_present()
            self.lyon_model.check_footer()

        self.lyon_model.logout()

    # TEST
    def categories_test(self):
        yield self.categories, "Audio Advice (Raleigh)", "Audio Advice"
        yield self.categories, "1 Control AV", ''
        yield self.categories, "Electrolux", '', True, True
