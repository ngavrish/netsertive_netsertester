__author__ = '802619'


from ..lyon.model.lyon import LyonModel
from ..generator_test import GeneratorTest, CheckLogs
from ..lyon.model.performance import PerformanceModel
from ..lyon.model.my_account import MyAccount


class EmailPreferences_Test(GeneratorTest):
    def __init__(self):
        super().__init__(app="lyon")

    # GENERATOR
    @CheckLogs("Add_weekly_snapshot_recipient")
    def add_weekly_snapshot_recipient(self):
        self.driver = self.get_web_driver()
        self.lyon_model = LyonModel(driver=self.driver)
        self.driver.maximize_window()
        self.my_account = MyAccount(driver=self.driver)
        self.performance_model = PerformanceModel(driver=self.driver)

        self.driver.get(self.web_ui_credentials.url)
        self.lyon_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)
        self.lyon_model.goto_tab('My Account')
        self.my_account.go_to_vertical_menu("Email Preferences")
        self.my_account.add_recipient("Weekly Snapshot", 'myemail@com.com')
        self.lyon_model.logout()

    # TEST
    def test(self):
        yield self.add_weekly_snapshot_recipient
