from .model.lyon import LyonModel
from ..generator_test import GeneratorTest, CheckLogs
from .model.performance import PerformanceModel


class Shoppers_Test(GeneratorTest):
    def __init__(self):
        super().__init__(app="lyon")

    @CheckLogs("Shoppers")
    def shoppers(self, client_name, friendly_name='', is_sponsor=False):
        self.driver = self.get_web_driver()
        self.driver.maximize_window()
        self.lyon_model = LyonModel(driver=self.driver)
        self.performance_model = PerformanceModel(driver=self.driver)

        self.driver.get(self.web_ui_credentials.url)
        self.lyon_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)

        self.lyon_model.open_company(client_name, friendly_name, is_sponsor)
        self.lyon_model.goto_tab("Shoppers")

        # note this has both graph and grid
        self.lyon_model.check_grid_present()

        self.performance_model.select_time_period('Day')
        self.lyon_model.check_graph_present()
        self.lyon_model.check_footer()

        self.performance_model.select_time_period('Week')
        self.lyon_model.check_graph_present()
        self.lyon_model.check_footer()

        self.performance_model.select_time_period('Month')
        self.lyon_model.check_graph_present()
        self.lyon_model.check_footer()

        # change the date range
        self.lyon_model.set_date_picker_to_last_month()
        self.lyon_model.check_grid_present()
        self.lyon_model.check_graph_present()
        self.lyon_model.check_footer()

        self.lyon_model.logout()

    # TEST
    def shoppers_test(self):
        yield self.shoppers, "Audio Advice (Raleigh)", "Audio Advice"
