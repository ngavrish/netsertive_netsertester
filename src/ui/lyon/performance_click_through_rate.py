from .model.lyon import LyonModel
from ..generator_test import GeneratorTest, CheckLogs
from .model.performance import PerformanceModel


class ClickThroughRate_Test(GeneratorTest):
    def __init__(self):
        super().__init__(app="lyon")

    # GENERATOR
    @CheckLogs("Leftnav_click_through_rate")
    def click_through_rate(self, client_name, friendly_name='', is_sponsor=False, label_enabled=False):
        self.driver = self.get_web_driver()
        self.driver.maximize_window()
        self.lyon_model = LyonModel(driver=self.driver)
        self.performance_model = PerformanceModel(driver=self.driver)
        self.driver.get(self.web_ui_credentials.url)
        self.lyon_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)

        self.lyon_model.open_company(client_name, friendly_name, is_sponsor)
        self.lyon_model.select_from_topnav("Performance", "Click-through Rate")
        self.lyon_model.check_graph_present()
        self.lyon_model.check_footer()
        if label_enabled:
            self.performance_model.select_item_in_dropdown('label')
            self.lyon_model.check_graph_present()
            self.lyon_model.check_footer()
        self.performance_model.select_item_in_dropdown('network', 1)
        self.lyon_model.check_graph_present()
        self.lyon_model.check_footer()
        self.performance_model.select_item_in_dropdown("sponsorship")
        self.lyon_model.check_graph_present()
        self.lyon_model.check_footer()
        if is_sponsor:
            self.performance_model.select_item_in_dropdown('dealers')
            self.lyon_model.check_graph_present()
            self.lyon_model.check_footer()
        else:
            self.performance_model.select_item_in_dropdown('sponsorship', 2)
            self.lyon_model.check_graph_present()
            self.lyon_model.check_footer()
            self.performance_model.select_item_in_dropdown('sponsorship', 3)
            self.lyon_model.check_graph_present()
            self.lyon_model.check_footer()

        self.lyon_model.logout()

    # TEST
    def click_through_rate_test(self):
        yield self.click_through_rate, "Audio Advice (Raleigh)", "Audio Advice"
        yield self.click_through_rate, "1 Control AV"
        yield self.click_through_rate, "Electrolux", '', True, True
