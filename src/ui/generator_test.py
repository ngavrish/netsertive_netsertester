from datetime import datetime
import time
from functools import wraps
import functools
import json
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.firefox.webdriver import WebDriver as Firefox
from selenium.webdriver.chrome.webdriver import WebDriver as Chrome
from selenium.webdriver.ie.webdriver import WebDriver as Ie
from selenium.webdriver.opera.webdriver import WebDriver as Opera
from selenium.webdriver.safari.webdriver import WebDriver as Safari
from selenium.webdriver.phantomjs.webdriver import WebDriver as PhantomJS
from nose.tools import eq_
import pytz
from testconfig import config
from ..utils.log_crawler import LogCrawler

__author__ = 'ngavrish'


class WebUICredentials():

    def __init__(self, login, password, url, web_timeout="30", fullname="QA Automation"):
        self.login = login
        self.password = password
        self.url = url
        self.web_timeout = int(web_timeout)
        self.fullname = fullname

    @property
    def fullname(self):
        return self.__fullname

    @fullname.setter
    def fullname(self, fullname):
        self.__fullname = fullname

    @property
    def login(self):
        return self.__login

    @login.setter
    def login(self, login):
        self.__login = login

    @property
    def password(self):
        return self.__password

    @password.setter
    def password(self, password):
        self.__password = password

    @property
    def url(self):
        return self.__url

    @url.setter
    def url(self, url):
        self.__url = url

    @property
    def web_timeout(self):
        return self.__web_timeout

    @web_timeout.setter
    def web_timeout(self, web_timeout):
        self.__web_timeout = web_timeout


class GeneratorTest():
    def __init__(self, app=None):
        self.config = config
        self.web_ui_credentials = WebUICredentials(config['credentials']['login'], config['credentials']['password'],
                                                   config['credentials']['url'], config['nosetests']['timeout'],
                                                   config['credentials']['fullname'])
        self.app = app

    @classmethod
    def get_json_data(self, filename):
        with open(config['nosetests'].get("test-data-path", "/").strip(
                    "/") + "/" + filename) as json_data:
                json_data = json.load(json_data)
        return json_data

    def set_driver(self):
        if config:
            self.driver_name = config['nosetests'].get('driver', "Firefox")
        else:
            self.driver_name = "Firefox"
        if self.driver_name == "Firefox":
            self.driver = Firefox()

    @classmethod
    def get_web_driver(cls):
        """
        Returns the WebDriver instance to be used. Defaults to `NeedleFirefox()`.
        Override this method if you'd like to control the logic for choosing
        the proper WebDriver instance.
        """
        browser_map = {
            'firefox': Firefox,
            'chrome': Chrome,
            'ie': Ie,
            'opera': Opera,
            'safari': Safari,
            'phantomjs': PhantomJS,
        }
        try:
            browser_class = browser_map.get(config['nosetests'].get('driver'), Firefox)()
            if config['nosetests'].get('driver') == 'firefox':
                browser_class.desired_capabilities['unexpectedAlertBehaviour'] = "ignore"
            return browser_class
        # Another attempt to wait until connectable. Webdriver might be slow starting on Jenkins.
        except WebDriverException:
            browser_class = browser_map.get(config['nosetests'].get('driver'), Firefox)()
            if config['nosetests'].get('driver') == 'firefox':
                browser_class.desired_capabilities['unexpectedAlertBehaviour'] = "ignore"
            return browser_class

    def ui_wrapper(self, *args, **kwargs):
        try:
            self.set_driver()
            self.start_stamp = datetime.now(pytz.timezone('America/New_York')).strftime(LogCrawler.TIMESTAMP_FORMAT)
            print("start = " + self.start_stamp)

            # run the test
            args[0](self.driver, *args[1:])

            self.finish_stamp = datetime.now(pytz.timezone('America/New_York')).strftime(LogCrawler.TIMESTAMP_FORMAT)
            print("finish = " + self.finish_stamp)
            errors_list = LogCrawler(start=self.start_stamp, finish=self.finish_stamp, config=config['logcrawler'])\
                .filter_errors()
            eq_(0, len(errors_list), "Errors parsed: " + str(errors_list))
            self.driver.quit()
        except IndexError:
            self.driver.quit()


def check_logs(fn):
    @wraps(fn)
    def decorator(*args, **kwargs):
        start_stamp = datetime.now(pytz.timezone('America/New_York')).strftime(LogCrawler.TIMESTAMP_FORMAT)
        print('start = ' + start_stamp)

        fn(*args, **kwargs)

        finish_stamp = datetime.now(pytz.timezone('America/New_York')).strftime(LogCrawler.TIMESTAMP_FORMAT)
        print('finish = ' + finish_stamp)
        error_list = LogCrawler(start=start_stamp, finish=finish_stamp, config=config['logcrawler']).filter_errors()
        eq_(0, len(error_list), "Errors parsed: " + str(error_list))
    return decorator


class CheckLogs:
    def __init__(self, test_description):
        self.test_description = test_description

    def __call__(self, fn):
        @functools.wraps(fn)
        def decorated(*args, **kwargs):
            start_stamp = datetime.now(pytz.timezone('America/New_York')).strftime(LogCrawler.TIMESTAMP_FORMAT)
            config["case_description"] = self.test_description
            fn(*args, **kwargs)
            finish_stamp = datetime.now(pytz.timezone('America/New_York')).strftime(LogCrawler.TIMESTAMP_FORMAT)
            error_list = LogCrawler(start=start_stamp, finish=finish_stamp, config=config['logcrawler']).filter_errors()
            eq_(0, len(error_list), "Errors parsed: " + str(error_list))
        return decorated


class CheckLogsWebUID:
    def __init__(self, test_description):
        self.test_description = test_description

    def __call__(self, fn):
        @functools.wraps(fn)
        def decorated(*args, **kwargs):
            start_stamp = datetime.now(pytz.timezone('America/New_York')).strftime(LogCrawler.TIMESTAMP_FORMAT)
            webuid = str(time.time())[:10]
            config["case_description"] = self.test_description + webuid[-2:]
            fn(webuid=webuid, *args, **kwargs)
            finish_stamp = datetime.now(pytz.timezone('America/New_York')).strftime(LogCrawler.TIMESTAMP_FORMAT)
            error_list = LogCrawler(start=start_stamp, finish=finish_stamp, config=config['logcrawler']).filter_errors()
            eq_(0, len(error_list), "Errors parsed: " + str(error_list))
        return decorated