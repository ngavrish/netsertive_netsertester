#!/usr/bin/env bash
nosetests test_smoke.py -s --tc-file=./config/qa.py --tc-format python --tc=lp_name:"$1"

# an example of usage:
# ./smoke.sh "SAP - ERP - SKU"
