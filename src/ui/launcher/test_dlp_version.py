from ..base.checklogs import CheckLogs
from .testcases import LauncherTestCase
from datetime import datetime
from .pageobjects.dlp import DlpVersion

class DlpVersionTestCase(LauncherTestCase):

    def test_copy_version(self):
        if 'lp_name' not in self.config:
            raise Exception('Landing Page name is required in order to run this test. Please use "lp_name" option.')

        ACTIVE = DlpVersion.Statuses.ACTIVE
        INACTIVE = DlpVersion.Statuses.INACTIVE
        DELETED = DlpVersion.Statuses.DELETED

        self.pages.login_page().login(self.config['app']['login'], self.config['app']['password'])

        dlp = self.pages.main_page()\
            .open_landing_page(self.config['lp_name'])

        active_version_name = dlp.versions.get_version({
            DlpVersion.Fields.STATUS: ACTIVE
        }).name

        new_version_name = self._generate_version_name()

        dlp.open_new_version_popup()\
            .open_copy_version()\
            .copy_version(
                active_version_name,
                new_version_name)

        original_version = dlp.versions.get_version({
            DlpVersion.Fields.NAME: active_version_name
        })

        # we need all because by default we don't show deleted versions
        dlp.grid.filter_by_status('All Versions')

        new_version = dlp.versions.get_version({
            DlpVersion.Fields.NAME: new_version_name
        })

        # default status should be Inactive
        new_version.wait_status(INACTIVE)

        self._set_status(new_version, DELETED)
        self._set_status(new_version, INACTIVE)
        self._set_status(new_version, ACTIVE)

        self._set_status(original_version, ACTIVE)
        new_version.wait_status(INACTIVE)

        self._set_status(new_version, DELETED)
        self._set_status(new_version, ACTIVE)

    @staticmethod
    def _set_status(version: DlpVersion, status: str):
        version.set_status(status)
        version.wait_status(status)

    @staticmethod
    def _generate_version_name() -> str:
        from random import choice
        from string import ascii_letters

        new_version_name = '{rand} {datetime}'.format(
                rand=''.join(choice(ascii_letters) for i in range(4)),
                datetime=datetime.now().strftime('%Y-%m-%d_%H:%M:%S'))

        """
        version name is limited to 50 chars because UI strips version name to 50 chars
        generation algorithm is subject to change. To keep this constraint we explicitly
        limit version name length
        """
        return new_version_name[:50]