from os import path
from selenium.webdriver.common.by import By


class SelectorFactory:

    @staticmethod
    def html_editable(attribute):
        return By.XPATH, \
            '//label[@for="' + attribute + '"]/..//div[contains(concat(" ", @class, " "), " ns-html-editor ")]'

    @staticmethod
    def uploadable(attribute):
        return sf.uploadable_client(attribute + '-value')

    @staticmethod
    def uploadable_client(attribute):
        return By.CSS_SELECTOR, '[data-name="' + attribute + '"]'

    @staticmethod
    def dynamic_form(attribute, param):
        return By.CSS_SELECTOR, \
            '.ns-dynamic-form [data-name="' + attribute + '"] .ns-dynamic-form__row-' + param + ' input'

    @staticmethod
    def dynamic_form_setting(setting_name):
        return By.CSS_SELECTOR, \
            '.ns-dynamic-form .ns-dynamic-form__setting-row[data-name="{name}"] input'.format(name=setting_name)

    @staticmethod
    def name(attribute):
        return By.NAME, attribute + '-value'

    @staticmethod
    def id(attribute):
        return By.ID, attribute

sf = SelectorFactory


def get_absolute_filepath(filename):
    return path.join(path.dirname(__file__), 'resources', filename)


"""
Color Strip
Coupon Image
Coupon Image 2
Dynamic Client Info
Dynamic Form
Feature Description
Featured Products
Featured Products with Constrained Images
Featured Products with Wrapping
Footer
Hero Image
Hero Image 2
Hero Image 3
Map
Quadrant
SKU Description
SKU Dynamic Form
SKU Product Highlights
SKU Products
SKU Static Bar
Video
"""

new_dlps = [
    {
        'name': 'Prime - Super',
        'template': 'Prime',

        'subtemplates': {
            sf.name('web_form_subtemplate'): 'Prime - Dynamic Form',
            sf.name('subtemplate_1'): 'Prime - Hero Image',
            sf.name('subtemplate_2'): 'Prime - Color Strip',
            sf.name('subtemplate_3'): 'Prime - Dynamic Client Info',
            sf.name('subtemplate_4'): 'Prime - Map',
            sf.name('subtemplate_5'): 'Prime - Coupon Image',
            sf.name('subtemplate_6'): 'Prime - Feature Description',
            sf.name('subtemplate_7'): 'Prime - Featured Products',
            sf.name('subtemplate_8'): 'Prime - Quadrant',
            sf.name('subtemplate_9'): 'Prime - Footer',

            sf.name('background_color'): 'ff0000',
        },

        'content_attrs': {
            # Dynamic Form
            sf.html_editable('static_bar_text'): 'Static Bar',
            sf.html_editable('form_header_text'): 'Form Header Text',
            sf.html_editable('form_description_text'): 'Form Description Text',
            sf.name('campaign_name'): 'Campaign Name',
            sf.html_editable('form_button_text'): 'Form Button Text',
            sf.html_editable('form_response_message'): 'Form Response Message',
            sf.name('document_url'): 'https://s3.amazonaws.com/NetUEV/MichaelAmini/Michael_Amini_Brochure.pdf',
            sf.name('button_color'): '9B2583',
            sf.name('button_bottom_color'): '9B2583',
            sf.name('button_border_color'): '3c132a',

            sf.dynamic_form('name', 'display'): 'Yes',
            sf.dynamic_form('name', 'title'): 'Full Name',
            sf.dynamic_form('name', 'required'): 'Yes',
            sf.dynamic_form('email', 'display'): 'Yes',
            sf.dynamic_form('zip', 'display'): 'Yes',
            sf.dynamic_form('confirmation', 'display'): 'Yes',
            sf.dynamic_form_setting('markOptional'): 'Yes',

            # Hero Image
            sf.uploadable('header_image_filename'): get_absolute_filepath('headers/hero.jpg'),

            # Color Strip
            sf.uploadable('block_1_image_filename'): get_absolute_filepath('block1/animated_banana.gif'),
            sf.html_editable('block_1_header'): 'Block 1 Header',
            sf.html_editable('block_1_text'): 'Block 1 Text',
            sf.name('block_1_bg_color'): 'fd9f13',

            # Map

            # Coupon Image
            sf.uploadable('offer_image_filename'): get_absolute_filepath('coupons/pdf/coupon.pdf'),
            sf.name('offer_url'): 'http://s3.amazonaws.com/NetUEV/content_server/dlps/765/FP-Product Brochure.pdf',
            sf.uploadable('offer_display_image_filename'): get_absolute_filepath('coupons/images/coupon.jpg'),
            sf.html_editable('offer_text'): 'Offer Text',

            # Dynamic Client Info

            # Feature Description
            sf.uploadable('block_2_image_filename'): get_absolute_filepath('block_2.png'),
            sf.html_editable('block_2_header'): 'Block 2 Header',
            sf.html_editable('block_2_text'): 'Block 2 Text',
            sf.name('block_2_bg_color'): '7ff296',

            # Featured Products
            sf.html_editable('product_1_name'): 'Product 1 Name',
            sf.uploadable('product_1_image_filename'): get_absolute_filepath('products/product1.jpg'),
            sf.html_editable('product_1_text'): 'Product 1 Text',

            sf.html_editable('product_2_name'): 'Product 2 Name',
            sf.uploadable('product_2_image_filename'): get_absolute_filepath('products/product2.jpg'),
            sf.html_editable('product_2_text'): 'Product 2 Text',

            sf.html_editable('product_3_name'): 'Product 3 Name',
            sf.uploadable('product_3_image_filename'): get_absolute_filepath('products/product3.jpg'),
            sf.html_editable('product_3_text'): 'Product 3 Text',

            sf.html_editable('product_4_name'): 'Product 4 Name',
            sf.uploadable('product_4_image_filename'): get_absolute_filepath('products/product4.jpg'),
            sf.html_editable('product_4_text'): 'Product 4 Text',

            sf.html_editable('product_header_text'): 'Product Header Text',

            sf.name('product_1_bg_color'): '99b5f6',
            sf.name('product_2_bg_color'): '6a83be',
            sf.name('product_3_bg_color'): '99b5f6',
            sf.name('product_4_bg_color'): '6a83be',

            # Quadrant Subtemplate
            sf.uploadable('bg_image_filename'): get_absolute_filepath('block_2.png'),
            sf.uploadable('bg_video_filename'): get_absolute_filepath('batteries.mp4'),
            sf.html_editable('header_text'): 'Header Text',
            sf.html_editable('left_text'): 'Left Text',
            sf.html_editable('right_button_text'): 'Right Button Text',
            sf.html_editable('right_small_text'): 'Right Small Text',
            sf.html_editable('right_text'): 'Right Text',
            sf.name('left_button_bg_color'): '7b0550',
            sf.name('left_button_hover_color'): 'eaf491',
            sf.name('right_button_bg_color'): '0d45c7',
            sf.name('right_button_hover_color'): 'eaf491',
            sf.name('left_bg_color'): '9b9ab0',
            sf.name('right_bg_color'): '9b9ab0',

            # Footer
            sf.html_editable('footer_text'): 'Footer Text',
            sf.name('footer_background_color'): '000',
        },

        'clients': [
            {
                'name': 'Netsertive',
                'attrs': {
                    sf.id('offer_url'):
                        'http://s3.amazonaws.com/NetUEV/content_server/dlps/765/FP-Product Brochure.pdf',
                    sf.uploadable_client('offer_image_filename'):
                        get_absolute_filepath('coupons/pdf/One-Two-FreeFlyer.pdf'),
                    sf.id('right_text'): 'Right text',
                    sf.id('right_small_text'): 'Right text small',
                }
            }
        ],
    },

    {
        'name': 'Prime - SKU',
        'template': 'Prime',

        'subtemplates': {
            sf.name('web_form_subtemplate'): 'Prime - SKU Dynamic Form',
            sf.name('subtemplate_1'): 'Prime - SKU Static Bar',
            sf.name('subtemplate_2'): 'Prime - SKU Description',
            sf.name('subtemplate_3'): 'Prime - SKU Product Highlights',
            sf.name('subtemplate_4'): 'Prime - SKU Products',
            sf.name('subtemplate_5'): 'Prime - Video',
            sf.name('subtemplate_6'): 'Prime - Footer',


            sf.name('background_color'): '000',
        },

        'content_attrs': {
            # SKU Dynamic Form
            sf.uploadable('header_image_filename'): get_absolute_filepath('headers/SKU.jpg'),
            sf.uploadable('logo_url'): get_absolute_filepath('SKU-white-logo.png'),
            sf.html_editable('header_text'): 'Header Text',
            sf.html_editable('main_text_line1'): 'Main Text Line1',
            sf.uploadable('header_product_image_filename'): get_absolute_filepath('wireless-routers.png'),
            sf.html_editable('form_header_text'): 'Form Header Text',
            sf.name('campaign_name'): 'Campaign Name',
            sf.html_editable('form_button_text'): 'Form Button Text',
            sf.html_editable('form_bottom_text'): 'Form Bottom Text',
            sf.html_editable('form_response_message'): 'Form Response Message',
            sf.name('document_url'): 'https://s3.amazonaws.com/NetUEV/MichaelAmini/Michael_Amini_Brochure.pdf',
            sf.name('header_background_color'): '000',
            sf.name('form_background_color'): 'b1bdce',
            sf.name('button_shadow_color'): '3c0271',
            sf.name('button_bg_gradient_start_color'): 'e6f199',
            sf.name('button_bg_gradient_end_color'): 'fd9f13',
            sf.name('button_text_color'): '19035e',
            sf.name('button_text_shadow_color'): '000',
            sf.name('button_border_color'): '1f0447',
            sf.name('button_hover_text_color'): '1c4704',
            sf.name('button_hover_shadow_color'): '1c4704',
            sf.name('button_hover_bg_gradient_start_color'): 'f99e84',
            sf.name('button_hover_bg_gradient_end_color'): 'd42f00',
            sf.name('button_active_shadow_color'): '806b8f',

            sf.dynamic_form('company', 'display'): 'Yes',
            sf.dynamic_form('email', 'display'): 'Yes',
            sf.dynamic_form('email', 'required'): 'Yes',
            sf.dynamic_form('phone_number', 'display'): 'Yes',
            sf.dynamic_form('confirmation', 'display'): 'Yes',
            sf.dynamic_form('confirmation', 'title'): 'Confirm',
            sf.dynamic_form_setting('markOptional'): 'Yes',

            # SKU Static Bar
            sf.uploadable('static_bar_map_image_filename'): get_absolute_filepath('SKU map-icon-violet.png'),
            sf.name('register_now_button_text'): 'Register Now Button Text',
            sf.name('static_bar_bg_gradient_start_color'): '9c9090',
            sf.name('static_bar_bg_gradient_end_color'): '000',
            sf.name('static_bar_text_color'): 'ffffff',
            sf.name('static_bar_delimiter_color'): 'c0f712',

            # SKU Description
            sf.uploadable('content_image_filename'): get_absolute_filepath('skucontent/4k-content-image.png'),
            sf.html_editable('content_text'): 'Content Text',
            sf.name('content_bg_color'): '392b57',
            sf.name('content_bg_gradient_color'): '897ea0',

            # SKU Product Highlights
            sf.html_editable('feature_header_text'): 'Feature Header Text',
            sf.uploadable('feature_1_image_filename'): get_absolute_filepath('skufeatures/feature1.jpg'),
            sf.html_editable('feature_1_text'): 'Feature 1 Text',
            sf.uploadable('feature_2_image_filename'): get_absolute_filepath('skufeatures/feature2.jpg'),
            sf.html_editable('feature_2_text'): 'Feature 2 Text',
            sf.uploadable('feature_3_image_filename'): get_absolute_filepath('skufeatures/feature3.jpg'),
            sf.html_editable('feature_3_text'): 'Feature 3 Text',
            sf.uploadable('feature_4_image_filename'): get_absolute_filepath('skufeatures/feature2.jpg'),
            sf.html_editable('feature_4_text'): 'Feature 4 Text',

            # SKU Products
            sf.html_editable('product_header_text'): 'Product Header Text',

            sf.html_editable('product_1_name'): 'Product 1 Name',
            sf.uploadable('product_1_image_filename'): get_absolute_filepath('skuproducts/prod1.jpg'),
            sf.html_editable('product_1_text'): 'Product 1 Text',
            sf.html_editable('product_2_name'): 'Product 2 Name',
            sf.uploadable('product_2_image_filename'): get_absolute_filepath('skuproducts/prod2.jpg'),
            sf.html_editable('product_2_text'): 'Product 2 Text',
            sf.html_editable('product_3_name'): 'Product 3 Name',
            sf.uploadable('product_3_image_filename'): get_absolute_filepath('skuproducts/prod3.jpg'),
            sf.html_editable('product_3_text'): 'Product 3 Text',
            sf.html_editable('product_4_name'): 'Product 4 Name',
            sf.uploadable('product_4_image_filename'): get_absolute_filepath('skuproducts/prod4.jpg'),
            sf.html_editable('product_4_text'): 'Product 4 Text',

            sf.name('products_header_bg_gradient_start_color'): '897ea0',
            sf.name('products_header_bg_gradient_end_color'): '392b57',
            sf.name('product_border_color'): '000',
            sf.name('product_bg_gradient_start_color'): 'c0bbc5',
            sf.name('product_bg_gradient_end_color'): '705f80',
            sf.name('product_title_delimiter_color'): '97f73f',

            # Video
            sf.name('video_url'): '//fast.wistia.net/embed/iframe/s7coovhl2q',

            # Footer
            sf.html_editable('footer_text'): 'ff0000',
            sf.name('footer_background_color'): '2c2144',
        },

        'clients': [
            {
                'name': 'Body Training Systems',
                'attrs': {
                    sf.id('offer_url'):
                        'http://s3.amazonaws.com/NetUEV/content_server/dlps/765/FP-Product Brochure.pdf',
                    sf.uploadable_client('offer_image_filename'):
                        get_absolute_filepath('coupons/pdf/One-Two-FreeFlyer.pdf'),
                    sf.id('right_text'): 'Right text',
                    sf.id('right_small_text'): 'Right text small',
                }
            }
        ],

        'sponsored_campaign': 'Body Training Systems',
    },

    {
        'name': 'Prime - Dynamic Form',
        'template': 'Prime',

        'subtemplates': {
            sf.name('web_form_subtemplate'): 'Prime - Dynamic Form',
            sf.name('subtemplate_1'): 'Prime - Hero Image',
            sf.name('subtemplate_2'): 'Prime - Map',
        },

        'content_attrs': {
            # Dynamic Form
            sf.html_editable('static_bar_text'): 'Static Bar Text',
            sf.html_editable('form_header_text'): 'Form Header Text',
            sf.html_editable('form_description_text'): 'Form Description Text',
            sf.name('campaign_name'): 'Campaign Name',
            sf.html_editable('form_button_text'): 'Form Button Text',
            sf.html_editable('form_response_message'): 'Form Response Message',
            sf.name('document_url'): 'http://www.amini.com/catalog/cat_19000/catalog.pdf',
            sf.name('button_color'): '008000',
            sf.name('button_bottom_color'): '008000',
            sf.name('button_border_color'): '008000',

            sf.dynamic_form('first_name', 'display'): 'Yes',
            sf.dynamic_form('first_name', 'title'): 'Name',
            sf.dynamic_form('first_name', 'required'): 'Yes',

            sf.dynamic_form('last_name', 'display'): 'Yes',
            sf.dynamic_form('last_name', 'title'): 'Surname',
            sf.dynamic_form('last_name', 'required'): 'Yes',

            # Hero Image
            sf.uploadable('header_image_filename'):
                get_absolute_filepath('headers/AMM_Hero_Banner_ALL-SIZES_nobox.jpg'),
        },

        'clients': [
            {
                'name': 'Netsertive',
                'attrs': {
                    sf.id('offer_url'):
                        'http://s3.amazonaws.com/NetUEV/content_server/dlps/765/FP-Product Brochure.pdf',
                    sf.uploadable_client('offer_image_filename'):
                        get_absolute_filepath('coupons/pdf/One-Two-FreeFlyer.pdf'),
                    sf.id('right_text'): 'Right text',
                    sf.id('right_small_text'): 'Right text small',
                }
            },
            {
                'name': 'Acme Integration',
                'attrs': {
                    sf.id('offer_url'):
                        'http://s3.amazonaws.com/NetUEV/content_server/dlps/765/FP-Product Brochure.pdf',
                    sf.uploadable_client('offer_image_filename'):
                        get_absolute_filepath('coupons/pdf/One-Two-FreeFlyer.pdf'),
                    sf.id('right_text'): 'Right text2',
                    sf.id('right_small_text'): 'Right text small2',
                }
            }
        ],
    },
]
