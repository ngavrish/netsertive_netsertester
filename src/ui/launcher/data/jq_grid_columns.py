jqGridAlwaysVisibleColumns = {
    'Main': ['Landing Page Name'],
    'Dlp': ['Version'],
    'DlpClients': ['Client Name']
}

jqGridHideableColumns = {
    'Main': {
        'active_version': 'Active Version',
        'client_count': 'Client Count',
        'status': 'Status',
        'pageViews': 'Views',
        'videoPlay': 'Video Plays',
        'formSubmit': 'Form Submits',
        'couponDownload': 'Coupon 1',
        'couponDownload2': 'Coupon 2',
        'totalConversion': 'Total Conversions'
    },

    'Dlp': {
        'status': 'Status',
        'updateDatetime': 'Last Update',
        'pageViews': 'Views',
        'videoPlay': 'Video Plays',
        'formSubmit': 'Form Submits',
        'couponDownload': 'Coupon 1',
        'couponDownload2': 'Coupon 2',
        'totalConversion': 'Total Conversions'
    },

    'DlpClients': {
        'hydrator_url': 'Hosted URL',
        'dlp_url': 'URL',
        'coupon': 'Coupon 1 URL',
        'coupon2': 'Coupon 2 URL',
        'content_key': 'Content Key',
        'site_key': 'Site Key',
        'preview_link': 'Preview',
        'status': 'Unsubscribe',
        'pageViews': 'Views',
        'videoPlay': 'Video Plays',
        'formSubmit': 'Form Submits',
        'couponDownload': 'Coupon 1',
        'couponDownload2': 'Coupon 2',
        'totalConversion': 'Total Conversions'
    }
}

jqGridColumnsCombinationsToTest = {
    'Main': {
        (),
        ('active_version',),
        ('client_count',),
        ('status',),
        ('active_version', 'client_count'),
        ('active_version', 'status'),
        ('active_version', 'client_count', 'status'),
        ('pageViews', 'videoPlay', 'formSubmit', 'couponDownload', 'totalConversion'),
        ('pageViews', 'videoPlay', 'formSubmit', 'couponDownload2', 'totalConversion'),
        ('pageViews', 'videoPlay', 'formSubmit', 'couponDownload', 'couponDownload2', 'totalConversion'),
        ('client_count', 'pageViews', 'videoPlay', 'formSubmit', 'couponDownload', 'couponDownload2',
         'totalConversion'),
        ('status', 'pageViews', 'videoPlay', 'formSubmit', 'couponDownload', 'couponDownload2', 'totalConversion'),
        ('status', 'client_count', 'pageViews', 'videoPlay', 'formSubmit', 'couponDownload', 'couponDownload2',
         'totalConversion'),
        ('active_version', 'status', 'client_count', 'pageViews', 'videoPlay', 'formSubmit', 'couponDownload',
         'couponDownload2', 'totalConversion')
    },

    'Dlp': {
        (),
        ('status',),
        ('status', 'updateDatetime'),
        ('pageViews', 'videoPlay', 'formSubmit', 'couponDownload', 'totalConversion'),
        ('pageViews', 'videoPlay', 'formSubmit', 'couponDownload2', 'totalConversion'),
        ('pageViews', 'videoPlay', 'formSubmit', 'couponDownload', 'couponDownload2', 'totalConversion'),
        ('status', 'pageViews', 'videoPlay', 'formSubmit', 'couponDownload', 'couponDownload2', 'totalConversion'),
        ('updateDatetime', 'pageViews', 'videoPlay', 'formSubmit', 'couponDownload', 'couponDownload2',
         'totalConversion'),
        ('status', 'updateDatetime', 'pageViews', 'videoPlay', 'formSubmit', 'couponDownload', 'couponDownload2',
         'totalConversion')
    },

    'DlpClients': {
        (),
        ('status',),
        ('coupon',),
        ('coupon2',),
        ('coupon', 'status'),
        ('coupon2', 'status'),
        ('coupon', 'coupon2', 'status'),
        ('status', 'hydrator_url', 'dlp_url', 'preview_link'),
        ('status', 'hydrator_url', 'dlp_url', 'coupon', 'preview_link'),
        ('status', 'hydrator_url', 'dlp_url', 'coupon', 'coupon2', 'preview_link'),
        ('hydrator_url', 'dlp_url', 'preview_link'),
        ('pageViews', 'videoPlay', 'formSubmit', 'couponDownload', 'totalConversion'),
        ('pageViews', 'videoPlay', 'formSubmit', 'couponDownload2', 'totalConversion'),
        ('pageViews', 'videoPlay', 'formSubmit', 'couponDownload', 'couponDownload2', 'totalConversion'),
        ('status', 'pageViews', 'videoPlay', 'formSubmit', 'couponDownload', 'couponDownload2', 'totalConversion'),
        ('hydrator_url', 'dlp_url', 'content_key', 'preview_link', 'status', 'pageViews', 'videoPlay', 'formSubmit',
         'couponDownload', 'couponDownload2', 'totalConversion'),
        ('hydrator_url', 'dlp_url', 'site_key', 'preview_link', 'status', 'pageViews', 'videoPlay', 'formSubmit',
         'couponDownload', 'couponDownload2', 'totalConversion')
    }
}
