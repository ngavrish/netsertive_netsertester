from datetime import datetime
from ..base.testcases import UITestCase
from .pageobjects import Pages
from ..base.checklogs import CheckLogs


class LauncherTestCase(UITestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.pages = Pages(cls.driver, cls.config['app']['base-url'])

    # GENERATOR
    def test_create_dlps(self):
        self.pages.login_page().login(self.config['app']['login'], self.config['app']['password'])

        from .data.new_dlps import new_dlps

        for dlp in new_dlps:
            yield self.create_dlp, dlp

    # TEST
    @CheckLogs
    def create_dlp(self, data: dict):
        dlp_name = '* {name} ({template}) {datetime}'.format(
            datetime=datetime.now().strftime('%Y-%m-%d_%H:%M:%S'),
            **data
        )

        self.log.info('Creating DLP "{name}"', name=dlp_name)

        dlp_page = self.pages.main_page()\
            .open_create_dlp_popup()\
            .create_dlp(dlp_name, data['template'])

        dlp_page.open_editor()\
            .edit(data['subtemplates'])\
            .edit(data['content_attrs'])\
            .close()

        page = self.add_clients(data.get('clients', []), dlp_page)
        self.add_sponsored_campaign(data.get('sponsored_campaign', ''), page)

    @classmethod
    def add_clients(cls, clients, page):
        if not clients:
            return page

        dlp_clients = page.tabs.clients()

        for i, client in enumerate(clients, start=1):
            cls.log.info('Adding client #{index} "{name}"', index=i, name=client['name'])

            dlp_clients \
                .open_add_client_popup() \
                .add_client(client['name'])

            dlp_clients \
                .open_edit_client(client['name']) \
                .edit(client['attrs'])

        return dlp_clients

    @staticmethod
    def add_sponsored_campaign(name, page):
        if not name:
            return page

        dlp_campaigns = page.tabs.sponsored_campaigns()
        dlp_campaigns.open_add_campaign_popup()\
            .add_campaign(name)

        return dlp_campaigns
