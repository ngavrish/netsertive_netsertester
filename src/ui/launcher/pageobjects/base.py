from selenium.common.exceptions import WebDriverException, UnexpectedAlertPresentException, TimeoutException, \
    StaleElementReferenceException
from ...base.pageobjects import PageObject, Element as BaseElement, Clickable as BaseClickable, Visible as BaseVisible
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as ec
from ...base.elements import NsElement


class Element(BaseElement):

    def __get__(self, page: PageObject, owner) -> WebElement:
        """
        workaround in case we cannot load analytics
        """
        try:
            return self._get_element(page)
        except UnexpectedAlertPresentException:
            # "All Landing Pages" page may produce Alert when analytics loading has been interrupted by leaving
            # that page.
            alert = page.driver.switch_to.alert
            self.log.warning(
                "UnexpectedAlertPresentException, page={page}, element selector={selector}, alert={text}",
                page=page.__class__.__name__,
                selector=self.selector,
                text=alert.text,
                exc_info=True
            )
            alert.accept()
            self.__get__(page, owner)


class Clickable(Element, BaseClickable):
    pass


class Visible(Element, BaseVisible):
    pass


class Attribute(Element):

    def __set__(self, page: PageObject, value):
        super().__set__(page, value)

        # workaround for color picker because it hover other elements
        color_input_name = self.selector[1]
        if 'color-value' in color_input_name:
            page.driver.execute_script('$(".ui-colorpicker").hide()')
            return

        element = self._get_element(page)

        if element.get_attribute('type') == 'file':
            # Actually this is workaround to wait until file uploaded to AWS bucket
            select_by = self.selector[0]
            selector_text = self.selector[1].replace('[data-', '[')

            try:
                page.wait.until(ec.text_to_be_present_in_element_value(
                    (select_by, selector_text), 'content_server/dlps'))
            except TimeoutException as te:
                self.log.exception('Failed wait for selector: {by}, {text}', by=select_by, text=selector_text)
                raise te

            return


class Popup(PageObject):
    """
    :type close_button: WebElement
    :type cancel_button: WebElement
    :type ok_button: WebElement
    """

    popup = None
    close_button = Clickable((By.CSS_SELECTOR, 'button[title=close]'))
    cancel_button = Clickable((By.CSS_SELECTOR, 'input[name=cancel]'))
    ok_button = Clickable((By.CSS_SELECTOR, 'input[name=ok]'))

    def __init__(self, driver: WebDriver):
        super().__init__(driver)

        self.overlay = NsElement((By.CSS_SELECTOR, '.ui-widget-overlay.ui-front'), self.globalWait)

        popup = self.popup
        if popup:
            self._set_context(popup)

    def close(self):
        self.close_button.click()

    def cancel(self):
        self.cancel_button.click()

    def confirm(self):
        self.ok_button.click()
        self.overlay.wait_invisible()


class DlpTabs(PageObject):
    """
    :type container: WebElement
    :type clients_tab: WebElement
    :type info_tab: WebElement
    :type campaigns_tab: WebElement
    """

    container = Element((By.ID, 'tabs'))
    clients_tab = Clickable((By.PARTIAL_LINK_TEXT, 'Clients'))
    info_tab = Clickable((By.PARTIAL_LINK_TEXT, 'Info'))
    campaigns_tab = Clickable((By.PARTIAL_LINK_TEXT, 'Sponsored Campaigns'))

    def __init__(self, driver: WebDriver):
        super().__init__(driver)
        self._set_context(self.container)

    def _click(self, element):
        """
        :type element: WebElement
        :return:
        """

        """
        Here is work around for:
        nose.proxy.WebDriverException: Message: unknown error: Element is not clickable at point (317, 381).
        Other element would receive the click: <h1>...</h1>
        Possible reason is static panel may hover element
        """
        for i in range(1, 3):
            try:
                element.click()
                break
            except WebDriverException:
                # workaround because of static panel may hover element
                self.log.warning('Failed to click on tab. Trying again after scrolling up.', exc_info=True)
                self.driver.execute_script('window.scrollTo(0, 0)')

    def clients(self):
        """
        :rtype: DlpClients
        """
        self._click(self.clients_tab)

        from .clients import DlpClients
        return DlpClients(self.driver)

    def info(self):
        """
        :rtype: DlpInfo
        """
        self._click(self.info_tab)

        from .info import DlpInfo
        return DlpInfo(self.driver)

    def sponsored_campaigns(self):
        """
        :rtype: SponsoredCampaigns
        """
        self._click(self.campaigns_tab)

        from .sponsoredcampaigns import SponsoredCampaigns
        return SponsoredCampaigns(self.driver)


class JqGridColumnsPopup(Popup):
    """
    :type temp: WebElement
    """

    temp = None

    popup = Visible((
        By.XPATH,
        '//*[contains(concat(" ", @class, " "), " ns-popup ")][.//*[contains(concat(" ", @class, " "), " ui-dialog-title ")]="Hide/Show Columns"]'
    ))

    def set_visible(self, columns: dict):
        for column_name, checked in columns.items():
            self.__class__.temp = Element((By.CSS_SELECTOR, 'input[name={name}]'.format(name=column_name)))
            self.temp = 'Yes' if checked else 'No'

        return self


class JqGrid(PageObject):
    """
    :type display_landing_pages_select: WebElement
    :type lp_grid_columns_button: WebElement
    """

    filter_by_status_select = Visible((By.XPATH, '//select[@id="ns_dlp_status_filter" and not(@disabled)]'))
    lp_grid_columns_button = Clickable((By.ID, 'ns_hide_show_jqgrid_columns'))

    def __init__(self, *args, grid_id='page_list_grid', **kwargs):
        super().__init__(*args, **kwargs)
        self.grid_id = grid_id

    def _ensure_grid_has_content(self):
        """
        Ensures that grid content div is present in DOM. That doesn't guarantee that grid content is really loaded.
        For example grids in Launcher are requesting analytics data once the main content of grid is loaded. Thus
        the grid is "fully loaded" only when analytics data is loaded.
        """
        self.wait.until(ec.visibility_of_element_located((By.ID, self.grid_id)))

    def get_status_fields(self):
        self._ensure_grid_has_content()
        return self.driver.find_elements_by_css_selector(
            '#{grid_id} td[role=gridcell][aria-describedby="{grid_id}_status"]'.format(grid_id=self.grid_id)
        )

    def filter_by_status(self, value):
        """
        Sets the value for "Display" select box and ensures the value is applied.
        (Select box is disabled when new value is set until grid is updated according to new filtering.)
        """
        self.wait_element(self.filter_by_status_select)
        self.filter_by_status_select = value
        self.wait_element(self.filter_by_status_select)

    def get_columns(self):
        return self.driver.find_elements_by_css_selector('#jq_grid_container tr.ui-jqgrid-labels th[role="columnheader"]')

    def open_columns_popup(self) -> JqGridColumnsPopup:
        self.lp_grid_columns_button.click()
        return JqGridColumnsPopup(self.driver)

    def get_field_rows(self, name):
        self._ensure_grid_has_content()
        return self.driver.find_elements_by_css_selector(
            '#{grid_id} td[role="gridcell"][aria-describedby="{name}"]'.format(name=name, grid_id=self.grid_id)
        )


class JqGridElement(NsElement):

    def click(self):
        """
        Override base implementation for grid elements.
        Because grid can be reloaded or in reloading state.
        We try to click 3 times. If click was successful we just break loop.
        If exception happens we just continue loop and try to click again.
        """
        for i in range(1, 3):
            try:
                self.wait_clickable()
                self._element.click()
                break
            except StaleElementReferenceException:
                self._log.warning('Element is staled. Clicking again.', exc_info=True)
            except WebDriverException:
                self._log.warning('Failed to click on element in grid. Trying again.', exc_info=True)
                # self.wait._driver.execute_script('window.scrollTo(0, 0)')

        return self


class NsJqGrid(PageObject):
    """
    :type _grid_id: str
    :type _columns: set[str]
    """

    def __init__(self, container: tuple, grid_id: str, columns: set, driver: WebDriver):
        """
        :type container: (str, str)
        :type grid_id: str
        :type columns: set[str]
        :type driver: WebDriver
        :return:
        """
        super().__init__(driver)
        self._set_context(NsElement(container, self.wait).get_element())
        self._grid_id = grid_id
        self._columns = columns

    def get_row(self, filters: dict) -> dict:
        """
        :type filters: dict[str, str]
        :rtype: dict[str, NsElement]
        """
        selector = '//table[@id="{grid_id}"]//tr[@role="row"]'.format(grid_id=self._grid_id)

        for column, value in filters.items():
            selector += '[./td[@aria-describedby="{grid_id}_{column}"][contains(@title, "{value}")]]'.format(
                    grid_id=self._grid_id,
                    column=column,
                    value=value
            )

        result_row = {}
        for column in self._columns:
            td_selector = (By.XPATH, selector + '//td[@aria-describedby="{grid_id}_{column}"]'.format(
                    grid_id=self._grid_id,
                    column=column
            ))
            result_row[column] = JqGridElement(td_selector, self.wait)

        return result_row
