from .base import Popup, PageObject, Element, Visible, Clickable, DlpTabs
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement


class AddCampaignPopup(Popup):
    """
    :type campaignNameAutoComplete: WebElement
    :type addBtn: WebElement
    """

    popup = Visible((
        By.XPATH,
        '//*[contains(concat(" ", @class, " "), " ns-popup ")][.//*[contains(concat(" ", @class, " "), " ui-dialog-title ")]="Add New Sponsored Campaign"]'
    ))
    campaignNameInput = Clickable((By.ID, 'ns_popup_input0'))
    campaignNameAutoComplete = Element((By.CSS_SELECTOR, '.ui-autocomplete a'))
    addBtn = Clickable((By.CSS_SELECTOR, 'input[name=ok][value="Add"]'))

    def add_campaign(self, campaign_name: str):
        self.campaignNameInput = campaign_name
        self.campaignNameAutoComplete.click()
        self.addBtn.click()
        self.overlay.wait_invisible()


class SponsoredCampaigns(PageObject):
    """
    :type addCampaignPopupBtn: WebElement
    """

    addCampaignPopupBtn = Clickable((By.ID, 'ns_add_sponsored_campaign'))

    def __init__(self, driver):
        super().__init__(driver)
        self.wait_element(self.addCampaignPopupBtn)

        self.tabs = DlpTabs(driver)

    def open_add_campaign_popup(self):
        """
        :rtype: AddCampaignPopup
        """
        self.addCampaignPopupBtn.click()
        return AddCampaignPopup(self.driver)
