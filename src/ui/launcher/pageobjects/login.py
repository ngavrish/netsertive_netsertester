from selenium.common.exceptions import UnexpectedAlertPresentException
from .base import PageObject, Element
from .main import Main
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.remote.webdriver import WebDriver


class Login(PageObject):
    """
    Login page functionality

    :type userInput: WebElement
    :type passwordInput: WebElement
    :type loginBtn: WebElement
    """

    userInput = Element((By.ID, 'username'))
    passwordInput = Element((By.ID, 'password'))
    loginBtn = Element((By.ID, 'login_button'))

    def __init__(self, driver: WebDriver):
        super().__init__(driver)
        try:
            self.title_contains('Login Page')
        except UnexpectedAlertPresentException:
            # "All Landing Pages" page may produce Alert when analytics loading has been interrupted by leaving
            # that page.
            self.log.warning("Failed load login page: {alert_text}", alert_text=self.alert.text, exc_info=True)

    def login(self, login: str, password: str) -> Main:
        self.userInput = login
        self.passwordInput = password
        self.loginBtn.click()

        return Main(self.driver)
