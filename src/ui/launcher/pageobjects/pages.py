from .login import Login
from .main import Main
from selenium.webdriver.remote.webdriver import WebDriver


class Pages:

    def __init__(self, driver: WebDriver, base_url: str):
        self.driver = driver
        self.base_url = base_url

    def login_page(self):
        """
        :rtype: Login
        """
        self.driver.get(self.base_url + '/auth/login')
        return Login(self.driver)

    def main_page(self):
        """
        :rtype: Main
        """
        self.driver.get(self.base_url + '/')
        return Main(self.driver)

    def logout(self):
        self.driver.get(self.base_url + '/auth/logout')
        return Login(self.driver)

