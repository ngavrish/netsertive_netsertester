from .base import Popup, PageObject, Element, Visible, Clickable, JqGrid
from .dlp import Dlp
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.action_chains import ActionChains


class CreateDlpPopup(Popup):
    """
    Create DLP popup functionality

    :type popup: WebElement
    :type nameInput: WebElement
    :type templateInput: WebElement
    :type createBtn: WebElement
    """

    popup = Visible((
        By.XPATH,
        '//*[contains(concat(" ", @class, " "), " ns-popup ")][.//*[contains(concat(" ", @class, " "), " ui-dialog-title ")]="Create New Landing Page"]'
    ))
    nameInput = Element((By.ID, 'ns_popup_input0'))
    templateInput = Element((By.ID, 'ns_popup_input2'))
    createBtn = Element((By.CSS_SELECTOR, 'input[name=ok][value=Go]'))

    def create_dlp(self, name: str, template: str) -> Dlp:
        self.nameInput = name
        self.templateInput = template
        self.createBtn.click()

        return Dlp(self.driver, name)


class Main(PageObject):
    """
    Main page functionality

    :type title_h1: WebElement
    :type new_dlp_popup_button: WebElement
    :type grid: JqGrid
    """

    title_h1 = Visible((By.XPATH, '//*[@id="landing_page_mgmt"]/h1[text()[contains(.,"All Landing Pages")]]'))
    new_dlp_popup_button = Clickable((By.ID, 'ns_show_new_dlp_popup'))
    page_link = None
    grid = None

    def __init__(self, driver: WebDriver):
        super().__init__(driver)
        self.wait_element(self.title_h1)
        self.grid = JqGrid(driver)

    def open_create_dlp_popup(self) -> CreateDlpPopup:
        self.new_dlp_popup_button.click()
        return CreateDlpPopup(self.driver)

    def open_landing_page(self, name):
        self.__class__.page_link = Clickable((
            By.CSS_SELECTOR,
            '#page_list_grid td[role="gridcell"][aria-describedby="page_list_grid_name"][title="{name}"] a'.format(
                name=name
            )
        ))

        ActionChains(self.driver)\
            .move_to_element(self.page_link)\
            .click(self.page_link)\
            .perform()

        return Dlp(self.driver, name)
