from .base import PageObject, Element, Visible, DlpTabs
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.remote.webdriver import WebDriver


class DlpInfo(PageObject):
    """
    DLP Info page functionality

    :type edit_lp_info_button: WebElement
    :type save_lp_info_button: WebElement
    :type cancel_lp_info_editing_button: WebElement
    :type lp_name_input: WebElement
    :type lp_description_input: WebElement
    """

    edit_lp_info_button = Element((By.ID, 'enable_edit_campaign_button'))
    save_lp_info_button = Element((By.ID, 'update_campaign_button'))
    cancel_lp_info_editing_button = Element((By.ID, 'cancel_campaign_button'))

    lp_name_input = Visible((By.ID, 'campaign_name'))
    lp_description_input = Visible((By.ID, 'campaign_description'))

    def __init__(self, driver: WebDriver):
        super().__init__(driver)
        self.tabs = DlpTabs(driver)

    def enable_editing(self):
        self.wait_clickable(self.edit_lp_info_button)
        self.edit_lp_info_button.click()

    def cancel_editing(self):
        self.wait_clickable(self.cancel_lp_info_editing_button)
        self.cancel_lp_info_editing_button.click()
