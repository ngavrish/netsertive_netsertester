from .base import Popup, PageObject, Visible, Clickable, Attribute, DlpTabs, JqGrid
from selenium.common.exceptions import WebDriverException, StaleElementReferenceException, TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException


class ClientEditor(Popup):
    """
    :type saveBtn: WebElement
    """

    field = None
    popup = Visible((By.XPATH, '//*[@id="client_details"][./h1[text()[contains(.,"Client Attribute List")]]]'))
    saveBtn = Clickable((By.CSS_SELECTOR, 'button.accept_attributes'))

    def edit(self, data: dict):
        for selector, value in data.items():
            self.__class__.field = Attribute(selector)
            self.field = value

        # temporary work around for error:
        # Element is not clickable at point (471, 873). Other element would receive the click:
        # <td class="ui-search-input">...</td>
        try:
            self.saveBtn.click()
        except WebDriverException:
            self.log.warning("Failed to click on a button. Trying again.", exc_info=True)
            self.saveBtn.click()

        self.alert.accept()


class AddClientPopup(Popup):
    """
    :type popup: WebElement
    :type clientNameAutoComplete: WebElement
    :type addBtn: WebElement
    """

    popup = Visible((
        By.XPATH,
        '//*[contains(concat(" ", @class, " "), " ns-popup ")][.//*[contains(concat(" ", @class, " "), " ui-dialog-title ")]="Add New Client"]'
    ))
    clientNameInput = Clickable((By.ID, 'ns_popup_input0'))
    clientNameAutoComplete = Visible((By.CSS_SELECTOR, '.ui-autocomplete a'))
    addBtn = Clickable((By.CSS_SELECTOR, 'input[name=ok][value="Add Client"]'))

    def add_client(self, client_name: str):
        # disallow grid clicks because after adding client grid reloads
        # self.driver.execute_script('window.__gridClicksAllowed__ = false')

        self.clientNameInput = client_name
        self.clientNameAutoComplete.click()
        self.addBtn.click()
        self.overlay.wait_invisible()


class DlpClients(PageObject):
    """
    :type new_client_popup_button: WebElement
    :type client_edit_button: WebElement
    """

    new_client_popup_button = Clickable((By.ID, 'ns_show_new_client_popup'))
    client_edit_button = None
    grid = None

    def __init__(self, driver: WebDriver):
        super().__init__(driver)
        self.wait_element(self.new_client_popup_button)

        self.tabs = DlpTabs(driver)
        self.grid = JqGrid(driver, grid_id='client_list_grid')

        """
        Workaround for http://jira.netsertive.local/browse/NPI-6485
        and for StaleElementReferenceException when we click to grid's
        elements immediately after adding client.

        Grid reloads after some actions. For example adding new client.
        So we subscribe on jqgrid jqGridLoadComplete event to wait until grid reloads.
        Because if we want to work with grid after it reloads
        we need to make sure grid is not changing.

        self.driver.execute_script("
            window.__gridClicksAllowed__ = true;
            $(function(){
                $('#client_list_grid').bind('jqGridLoadComplete', function (e, rowid, orgClickEvent) {
                    __gridClicksAllowed__ = $(this).getGridParam('datatype') != 'json';
                });
            });
        ")
        """

    def open_add_client_popup(self) -> AddClientPopup:

        """
        We try to click 3 times. If click was successfull we just break loop.
        If exception happens we just continue loop and try to click again:
        nose.proxy.WebDriverException: Message: unknown error: Element is not clickable at point (824, 420).
        Other element would receive the click: <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">...</ul>

        I don't know why this random exception happens
        """
        for i in range(1, 3):
            try:
                self.new_client_popup_button.click()
                break
            except WebDriverException:
                self.log.warning('Failed to click on Add New Client button. Trying again.', exc_info=True)
                self.driver.execute_script('window.scrollTo(0, 0)')

        try:
            return AddClientPopup(self.driver)
        except TimeoutException:
            self.log.warning('Add client popup timed out. Trying again after page refresh.', exc_info=True)

            # refresh page and try again
            self.driver.refresh()
            self.new_client_popup_button.click()
            return AddClientPopup(self.driver)

    def _click_client_edit(self, name: str):
        self.__class__.client_edit_button = Clickable(
            (By.CSS_SELECTOR, '#client_list_grid td[role=gridcell][title="{}"] a'.format(name)))

        """
        We try to click 3 times. If click was successfull we just break loop.
        If exception happens we just continue loop and try to click again.
        """
        for i in range(1, 3):
            try:
                self.client_edit_button.click()
                break
            except StaleElementReferenceException:
                self.log.warning('client_edit_button staled. Clicking again', exc_info=True)
            except WebDriverException:
                self.log.warning("Failed to click on client row in a grid. Trying again.", exc_info=True)
                self.driver.execute_script('window.scrollTo(0, 0)')

    def open_edit_client(self, name: str):
        """
        :param name:
        :rtype: ClientEditor
        """

        # wait until grid reloads
        # self.globalWait.until(lambda driver: driver.execute_script('return __gridClicksAllowed__'))

        self._click_client_edit(name)

        # workaround for http://jira.netsertive.local/browse/NPI-6485
        try:
            return ClientEditor(self.driver)
        except TimeoutException:
            self.log.warning('Client editor timed out. Trying again after page refresh.', exc_info=True)

            # refresh page and try again
            self.driver.refresh()
            self.client_edit_button.click()
            return ClientEditor(self.driver)

    def get_landing_page_preview_link(self, client_name: str, hosted_url: bool=False):
        """
        Returns link to the very first client who has a preview or hydrator url, depending on argument.

        :param client_name: return preview link for this client
        :param hosted_url: true to return link to hydrator url, false to return simple preview url (default)
        :return: WebElement
        """

        loading = self.driver.find_element_by_id('loading')
        self.wait_invisible(loading)

        client_names = [row.get_attribute('title') for row in self.grid.get_field_rows('client_list_grid_client_name')]
        preview_links = self.grid.get_field_rows(
                'client_list_grid_hydrator_url' if hosted_url else 'client_list_grid_dlp_url')

        try:
            link = preview_links[client_names.index(client_name)].find_element_by_tag_name('A')
        except ValueError as ve:
            self.log.error('Sorry, client "{name}" is not found in the list', name=client_name, exc_info=True)
            raise ve
        except NoSuchElementException as nsee:
            # there might be no A element in there
            self.log.error('Sorry, no {hosted} link found for client "{client_name}"',
                           client_name=client_name, hosted='hosted' if hosted_url else 'preview', exc_info=True)
            raise nsee

        self.wait_clickable(link)
        return link
