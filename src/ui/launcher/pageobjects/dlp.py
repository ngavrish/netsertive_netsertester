from .base import PageObject, Clickable, Attribute, Element, Visible, DlpTabs, JqGrid, Popup, NsJqGrid
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.remote.webdriver import WebDriver
from ...base.elements import NsSelect, NsElement, NsInputText


class Editor(PageObject):
    """
    Version editor functionality

    :type temp: WebElement
    :type saveBtn: WebElement
    :type closeBtn: WebElement
    """

    temp = None
    saveBtn = Clickable((By.ID, 'post_link'))
    closeBtn = Clickable((By.CSS_SELECTOR, '.ns_version_details_popup button[title=close]'))

    def __init__(self, driver: WebDriver):
        super().__init__(driver)
        self.switch_to_frame(0)

    def edit(self, data: dict):
        """
        :rtype: Editor
        """
        for selector, value in data.items():
            self.__class__.temp = Attribute(selector)
            self.temp = value

        self.saveBtn.click()
        self.alert.accept()

        return self

    def close(self):
        self.driver.switch_to.default_content()
        self.closeBtn.click()


class CopyVersionPopup(Popup):
    """
    :type popup: WebElement
    """

    popup = Visible((
        By.XPATH,
        '//*[contains(concat(" ", @class, " "), " ns-popup ")][.//*[contains(concat(" ", @class, " "), " ui-dialog-title ")]="Copy Existing Version"]'
    ))

    def __init__(self, driver: WebDriver):
        super().__init__(driver)

        self.versionSelect = NsSelect((By.ID, 'ns_popup_input0'), self.wait)
        self.newVersionNameInput = NsInputText((By.ID, 'ns_popup_input1'), self.wait)

    def copy_version(self, from_version: str, new_version_name: str = ''):
        """
        :param from_version: str
        :param new_version_name: str new version name is limited to 50 chars in UI
        :return:
        """
        if len(new_version_name) > 50:
            raise Exception('New version name cannot be more than 50 chars')

        self.versionSelect.set_value(from_version)
        if new_version_name:
            self.newVersionNameInput.set_value(new_version_name)
        self.confirm()


class NewVersionPopup(Popup):
    """
    :type popup: WebElement
    :type copy_version_button: WebElement
    """

    popup = Visible((
        By.XPATH,
        '//*[contains(concat(" ", @class, " "), " ns-popup ")][.//*[contains(concat(" ", @class, " "), " ui-dialog-title ")]="Create New Version"]'
    ))
    copy_version_button = Clickable((By.CSS_SELECTOR, 'div.ns-clone-version-button'))

    def open_copy_version(self) -> CopyVersionPopup:
        self.copy_version_button.click()
        return CopyVersionPopup(self.driver)


class RenameVersionPopup(Popup):
    """
    :type popup: WebElement
    :type version_text: WebElement
    """

    popup = Visible((
        By.XPATH,
        '//*[contains(concat(" ", @class, " "), " ns-popup ")][.//*[contains(concat(" ", @class, " "), " ui-dialog-title ")]="Edit Version Name"]'
    ))
    version_text = Visible((By.CSS_SELECTOR, 'input[type=text]'))

class ConfirmVersionStatusPopup(Popup):
    """
    :type popup: WebElement
    """

    popup = Visible((
        By.XPATH,
        '//*[contains(concat(" ", @class, " "), " ns-popup ")][.//*[contains(concat(" ", @class, " "), " ui-dialog-title ")]="Activate Version"]'
    ))

class ChangeVersionStatusPopup(Popup):
    """
    :type popup: WebElement
    """

    popup = Visible((
        By.XPATH,
        '//*[contains(concat(" ", @class, " "), " ns-popup ")][.//*[contains(concat(" ", @class, " "), " ui-dialog-title ")]="Change Version Status"]'
    ))

    def __init__(self, driver: WebDriver):
        super().__init__(driver)
        self.statusSelect = NsSelect((By.CSS_SELECTOR, '.ns-change-version-status select'), self.wait)

    def set_status(self, status: str) -> ConfirmVersionStatusPopup:
        self.statusSelect.set_value(status)

        if status == 'Active':
            self.ok_button.click()
            return ConfirmVersionStatusPopup(self.driver)
        else:
            self.confirm()
            return None

class DlpVersion(PageObject):

    class Statuses:
        ACTIVE = 'Active'
        INACTIVE = 'Inactive'
        DELETED = 'Deleted'

    class Fields:
        """
        Full columns list
        'edit', 'id', 'description', 'status', 'updateDatetime', 'preview', 'pageViews', 'bounceRate',
        'averageTimeOnPage', 'videoPlay', 'formSubmit', 'couponDownload', 'totalConversion'
        Add new constants once required
        """
        NAME = 'description'
        STATUS = 'status'

    columns = {Fields.NAME, Fields.STATUS}

    def __init__(self, row: dict, driver: WebDriver):
        """
        :type row: dict[str, NsElement]
        :rtype:
        """
        super().__init__(driver)
        self._row = row

        # verify that version is valid by waiting for its name to present
        self._row[self.Fields.NAME].wait_present()

    def _get_field_value(self, field: str) -> str:
        return self._row[field].get_attribute('title').strip()

    @property
    def name(self):
        return self._get_field_value(self.Fields.NAME)

    @property
    def status(self):
        return self._get_field_value(self.Fields.STATUS)

    def open_change_status_popup(self) -> ChangeVersionStatusPopup:
        self._row[self.Fields.STATUS].click()
        return ChangeVersionStatusPopup(self.driver)

    def set_status(self, status: str):
        confirm_popup = self.open_change_status_popup()\
            .set_status(status)

        if confirm_popup:
            confirm_popup.confirm()

    def wait_status(self, status: str):
        self._row[self.Fields.STATUS].wait_text(status)

class DlpVersions(PageObject):

    def __init__(self, driver: WebDriver):
        super().__init__(driver)
        self._grid = NsJqGrid((By.ID, 'jq_grid_container'), 'page_list_grid', DlpVersion.columns, driver)

    def get_version(self, filters: dict) -> DlpVersion:
        """
        :type filters: dict[str, str]
        :rtype: DlpVersion
        """
        row = self._grid.get_row(filters)
        return DlpVersion(row, self.driver)


class Dlp(PageObject):
    """
    DLP page functionality

    :type titleH1: WebElement
    :type edit_version_button: WebElement
    :type new_version_button: WebElement
    :type rename_version_button: WebElement
    :type grid: JqGrid
    """

    titleH1 = None
    edit_version_button = Clickable((By.CSS_SELECTOR, '#jq_grid_container .version_editor'))
    new_version_button = Element((By.ID, 'ns_show_new_dlp_version_popup'))
    rename_version_button = Clickable((By.CSS_SELECTOR, '#jq_grid_container a.version_desc_editor'))
    grid = None

    def __init__(self, driver: WebDriver, name: str):
        super().__init__(driver)
        self.__class__.titleH1 = Visible(
            (By.XPATH, '//*[@id="landing_page_mgmt"]/h1[text()[contains(.,"' + name + '")]]'))
        self.wait_element(self.titleH1)
        self.grid = JqGrid(driver)
        self.versions = DlpVersions(driver)

        pos = self.driver.current_url.rfind('/') + 1
        self.dlpId = self.driver.current_url[pos:]

        self.tabs = DlpTabs(driver)

    def open_editor(self) -> Editor:
        self.edit_version_button.click()
        return Editor(self.driver)

    def open_new_version_popup(self) -> NewVersionPopup:
        self.new_version_button.click()
        return NewVersionPopup(self.driver)

    def open_rename_version_popup(self) -> RenameVersionPopup:
        self.rename_version_button.click()
        return RenameVersionPopup(self.driver)

    def get_version_name(self):
        return self.edit_version_button.text
