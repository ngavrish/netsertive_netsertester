from .pageobjects import Pages
from ..base.testcases import UITestCase

class LauncherTestCase(UITestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.pages = Pages(cls.driver, cls.config['app']['base-url'])