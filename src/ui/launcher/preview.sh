#!/usr/bin/env bash
nosetests test_preview_dlp.py -s --tc-file=./config/qa.py --tc-format python --tc=lp_name:"$1" --tc=tests.preview.client:"${2:=Netsertive}"

# an example of usage:
# ./preview.sh "* Prime - Dynamic Form (Prime) 2015-12-01_16:18:34" "Darbys' Big Furniture"
# second argument is optional and is "Netsertive" by default