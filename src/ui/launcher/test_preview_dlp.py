from ..base.testcases import UITestCase
from .pageobjects import Pages
from ..base.checklogs import CheckLogs
import re


class PreviewDlpTestCase(UITestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.pages = Pages(cls.driver, cls.config['app']['base-url'])

    def __getattr__(self, item):
        return self.config['tests']['preview'][item] if item in self.config['tests']['preview'] else None

    def test_preview_dlp(self):
        if 'lp_name' in self.config:
            self._preview_dlp(self.config['lp_name'], self.client)
        else:
            raise Exception("Landing Page name is required in order to run this test. Please use \"lp_name\" option.")

    @CheckLogs
    def _preview_dlp(self, lp_name: str, client_name: str):
        self.pages.login_page().login(self.config['app']['login'], self.config['app']['password'])

        clients = self.pages\
            .main_page()\
            .open_landing_page(lp_name)\
            .tabs.clients()

        self._click_and_ensure_tab_opened(clients, client_name, hosted=False)
        self._click_and_ensure_tab_opened(clients, client_name, hosted=True)

    def _click_and_ensure_tab_opened(self, clients, client_name: str, hosted: bool):
        link = clients.get_landing_page_preview_link(client_name, hosted)

        href = link.get_attribute('href')

        if hosted and self.remove_subdomain:
            # replaces "proto://foo.bar.baz/..." by "proto://bar.baz/..."
            href = re.sub(r'([^:]+://)[^\.]+\.(.*)', r'\1\2', href)
            self.driver.execute_script('arguments[0].setAttribute("href", "{href}")'.format(href=href), link)

        link.click()

        tabs = self.driver.window_handles
        if len(tabs) != 2:
            self.log.error('Unexpected number of tabs. Tabs: {tabs}', tabs=tabs)
            raise Exception('Unexpected number of tabs')

        self.driver.switch_to.window(tabs[1])
        if self.driver.current_url != href:
            self.log.error('Wrong tab. Expected "{expected}", actual: "{actual}"',
                           expected=href, actual=self.driver.current_url)
            raise Exception("Wrong tab")

        self.driver.close()
        self.driver.switch_to.window(tabs[0])
