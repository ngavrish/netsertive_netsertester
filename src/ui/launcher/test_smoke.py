from ..base.testcases import UITestCase
from .pageobjects import Pages
from nose.tools import nottest
from selenium.common.exceptions import UnexpectedAlertPresentException


class LauncherTestCase(UITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.pages = Pages(cls.driver, cls.config['app']['base-url'])

    def test_smoke(self):
        if 'lp_name' not in self.config:
            raise Exception("Landing Page name is required in order to run this test. Please use \"lp_name\" option.")

        self.pages.login_page().login(self.config['app']['login'], self.config['app']['password'])
        yield self.main_page
        yield self.dlp_page_versions
        yield self.dlp_page_clients
        yield self.dlp_page_info
        yield self.dlp_page_sponsored_campaigns

    #TODO: Move to PageObjects level
    @nottest
    def test_page_display_selector(self, page, state_all, state_no_deleted):
        page.grid.filter_by_status(state_all)
        status_fields = page.grid.get_status_fields()
        was_deleted = False
        for field in status_fields:
            try:
                if field.get_attribute('title') not in ['Active', 'Deleted']:
                    self.log.error('One of the cells has invalid status: {status}', status=field.get_attribute('title'))
                else:
                    was_deleted |= field.get_attribute('title') == 'Deleted'
            except UnexpectedAlertPresentException:
                self.handle_alert(page)

        if not was_deleted:
            self.log.warning('No deleted items for page {name}, is that ok?', name=page.__class__.__name__)

        page.grid.filter_by_status(state_no_deleted)
        status_fields = page.grid.get_status_fields()
        for field in status_fields:
            if field.get_attribute('title') != 'Active':
                self.log.error('One of the cells has invalid status: {status}', status=field.get_attribute('title'))

    def handle_alert(self, page):
        alert = page.driver.switch_to.alert
        self.log.warning(
                "UnexpectedAlertPresentException, page={page}, alert={text}",
                page=page.__class__.__name__,
                text=alert.text,
                exc_info=True
        )
        alert.accept()

    #TODO: Move to PageObjects level
    @nottest
    def test_page_grid_columns(self, page):

        from .data import jq_grid_columns

        page_name = page.__class__.__name__
        self.log.info('Testing {page_name} grid columns', page_name=page_name)

        always_visible = jq_grid_columns.jqGridAlwaysVisibleColumns[page_name]
        columns = jq_grid_columns.jqGridHideableColumns[page_name]
        test_combinations = jq_grid_columns.jqGridColumnsCombinationsToTest[page_name]

        for combination in test_combinations:
            self.log.info('Testing combination: {combination}', combination=combination)

            page.grid\
                .open_columns_popup()\
                .set_visible({column: column in combination for column in columns.keys()})\
                .confirm()

            # mind that WebElement.is_displayed() verifies if the element is actually visible for user
            # that is why I verify for display:none style instead.
            visible_columns = [column.get_attribute('title') for column in page.grid.get_columns()
                               if column.value_of_css_property('display') != 'none']

            expected_columns = [column_title for column, column_title in columns.items() if column in combination]
            expected_columns.extend(always_visible)  # this columns are always displayed

            if set(visible_columns) != set(expected_columns):
                self.log.error('Invalid columns, expected: "{expected}", actual: "{actual}"'.format(
                    expected=expected_columns,
                    actual=visible_columns
                ))
                assert False

    # TEST METHOD
    def main_page(self):
        main_page = self.pages.main_page()

        self.test_page_display_selector(main_page, 'All Landing Pages', 'All but Deleted Landing Pages')
        self.test_page_grid_columns(main_page)

        main_page.open_create_dlp_popup().cancel()

    # TEST METHOD
    def dlp_page_versions(self):
        main_page = self.pages.main_page()
        dlp_page = main_page.open_landing_page(self.config['lp_name'])

        self.test_page_display_selector(dlp_page, 'All Versions', 'All but Deleted Versions')
        self.test_page_grid_columns(dlp_page)

        new_version_popup = dlp_page.open_new_version_popup()
        new_version_popup.open_copy_version().cancel()
        new_version_popup.cancel()

        version_name = dlp_page.get_version_name()

        rename_version_popup = dlp_page.open_rename_version_popup()
        popup_version_name = rename_version_popup.version_text.get_attribute('value')
        if popup_version_name != version_name:
            self.log.error(
                'Invalid version name in "Edit Version Name" popup. Expected: "{expected}", actual: "{actual}"'.format(
                    expected=version_name,
                    actual=popup_version_name
                )
            )
            assert False

    # TEST METHOD
    def dlp_page_clients(self):
        main_page = self.pages.main_page()
        clients_page = main_page\
            .open_landing_page(self.config['lp_name'])\
            .tabs \
            .clients()

        self.test_page_grid_columns(clients_page)

        clients_page.open_add_client_popup().cancel()

    # TEST METHOD
    def dlp_page_info(self):
        main_page = self.pages.main_page()
        info_page = main_page \
            .open_landing_page(self.config['lp_name']) \
            .tabs \
            .info()

        info_page.enable_editing()
        info_page.wait_invisible(info_page.edit_lp_info_button)
        info_page.wait_clickable(info_page.save_lp_info_button)
        info_page.wait_clickable(info_page.cancel_lp_info_editing_button)
        info_page.wait_clickable(info_page.lp_name_input)
        info_page.wait_clickable(info_page.lp_description_input)

        info_page.cancel_editing()
        info_page.wait_clickable(info_page.edit_lp_info_button)
        info_page.wait_invisible(info_page.save_lp_info_button)
        info_page.wait_invisible(info_page.cancel_lp_info_editing_button)
        info_page.wait_disabled(info_page.lp_name_input)
        info_page.wait_disabled(info_page.lp_description_input)

    # TEST METHOD
    def dlp_page_sponsored_campaigns(self):
        main_page = self.pages.main_page()
        sponsored_campaigns_page = main_page \
            .open_landing_page(self.config['lp_name']) \
            .tabs \
            .sponsored_campaigns()

        sponsored_campaigns_page.open_add_campaign_popup().cancel()
