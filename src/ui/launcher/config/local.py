global config
config = {
    'driver': {
        'browser': 'chrome',
        'window_size': {
            'width': 1024,
            'height': 768,
        },
    },

    'app': {
        'base-url': 'http://localhost/reports/launcher',
        'login': 'qaautomation@netsertive.com',
        'password': 'qaautomation',
    },

    'logcrawler': {
        'local': True,
        'logs-path': '/Netsertive/Repo/websites/launcher/application/logs',
        'errors_whitelist': {
            'SLOW QUERY FOUND!',
            '404 Page Not Found --> favicon.ico',
            '404 Page Not Found --> images',
        },
    },

    'tests': {
        'preview': {
            'client': 'Netsertive',
            'remove_subdomain': True
        }
    },
}
