global config
config = {
    'driver': {
        'browser': 'chrome',
        'window_size': {
            'width': 1024,
            'height': 768,
        },
    },

    'app': {
        'base-url': 'http://steam-01.netsertive.local/reports/launcher/index.php',
        'login': 'qaautomation@netsertive.com',
        'password': 'qaautomation',
    },

    'logcrawler': {
        'local': False,
        'logs-path': '/Netsertive/Repo/Websites/launcher/application/logs',
        'remote-server-url': 'steam-01.netsertive.local',
        'remote-username': 'webuser',
        'remote-password': 'rocker157R',
        'errors_whitelist': {
            'SLOW QUERY FOUND!',
            '404 Page Not Found --> images',
        },
    },

    'tests': {
        'preview': {
            'client': 'Netsertive',
            'remove_subdomain': True
        }
    },
}
