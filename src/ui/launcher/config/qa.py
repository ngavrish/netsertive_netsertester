global config
config = {
    'driver': {
        'browser': 'chrome',
        'window_size': {
            'width': 1024,
            'height': 768,
        },
    },

    'app': {
        'base-url': 'http://qa-launcher.netsertive.local/index.php',
        'login': 'qaautomation@netsertive.com',
        'password': 'qaautomation',
    },

    'logcrawler': {
        'local': False,
        'logs-path': '/AppData/webs/qa/launcher/v1/launcher/application/logs',
        'remote-server-url': 'vh1-qaweb-01.netsertive.local',
        'remote-username': 'webuser',
        'remote-password': 'P1ngP0ng!',
        'errors_whitelist': {
            'SLOW QUERY FOUND!',
            '404 Page Not Found --> images',
        },
    },

    'tests': {
        'preview': {
            'client': 'Netsertive',
            'remove_subdomain': True
        }
    },
}
