global config
config = {
    'driver': {
        'browser': 'chrome',
        'window_size': {
            'width': 1024,
            'height': 768,
        },
    },

    'app': {
        'base-url': 'http://tst-launcher.netsertive.local/index.php',
        'login': 'qaautomation@netsertive.com',
        'password': 'qaautomation',
    },

    'logcrawler': {
        'local': False,
        'logs-path': '/AppData/webs/tst/launcher/v1/launcher/application/logs',
        'remote-server-url': 'vh1-tstweb-01.netsertive.local',
        'remote-username': 'webuser',
        'remote-password': 'rocker157R',
        'errors_whitelist': {
            'SLOW QUERY FOUND!',
            '404 Page Not Found --> images',
        },
    },

    'tests': {
        'preview': {
            'client': 'Netsertive',
            'remove_subdomain': True
        }
    },
}
