"""
Created on Apr 23, 2015

@author = tclark@netsertive.com
"""

from functools import reduce

import time
from nose.tools import eq_, ok_
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.select import Select


class ReportsToolsModel(object):
    """
    Object Model for Reports and Tools
    """

    def __init__(self, test):
        self.test = test

    def Links(self, link):
        pass

