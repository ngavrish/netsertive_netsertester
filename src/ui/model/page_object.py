from nose.tools import ok_
from selenium.common.exceptions import UnexpectedAlertPresentException, TimeoutException, StaleElementReferenceException
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait
from testconfig import config

__author__ = 'ngavrish'


class Error(object):
    def __init__(self, message=""):
        self.message = message


class PageObject(object):
    def __init__(self, driver, timeout):
        self.driver = driver
        if not timeout:
            timeout = int(config['nosetests']['timeout'])
        self.wait = WebDriverWait(driver, timeout=timeout)

    def set_input_value(self, selector, text, xpath=None):
        """
        Set input value inside given CSS or XPATH selector
        Waits are edited because of bad, timeout-dependent code, so we need to make pauses
        So that text values had been set, would be saved on the server
        """
        if xpath:
            self.wait_until_clickable_xpath(xpath).clear()
            if text:
                self.wait_until_clickable_xpath(xpath).send_keys(text)
        elif selector:
            self.wait_until_clickable_css(selector).clear()
            if text:
                self.wait_until_clickable_css(selector).send_keys(text)

    def get_input_value(self, css=None, xpath=None):
        if css:
            return self.wait_until_clickable_css(css).get_attribute('value')
        elif xpath:
            self.wait.until(EC.presence_of_element_located((By.XPATH, xpath)))
            return self.wait_until_clickable_xpath(xpath).get_attribute('value')

    def wait_until_clickable_xpath(self, selector):
        try:
            return self.wait.until(EC.element_to_be_clickable((By.XPATH, selector)))
        except UnexpectedAlertPresentException:
            self.accept_js_alert()
            return self.wait.until(EC.element_to_be_clickable((By.XPATH, selector)))

    def wait_until_not_visible_xpath(self, selector):
        try:
            return self.wait.until_not(EC.visibility_of_element_located((By.XPATH, selector)))
        except UnexpectedAlertPresentException:
            self.accept_js_alert()
            return self.wait.until_not(EC.visibility_of_element_located((By.XPATH, selector)))

    def wait_until_not_present_xpath(self, selector):
        try:
            return self.wait.until_not(EC.presence_of_element_located((By.XPATH, selector)))
        except UnexpectedAlertPresentException:
            self.accept_js_alert()
            return self.wait.until_not(EC.presence_of_element_located((By.XPATH, selector)))

    def wait_until_invisible_css(self, selector):
        try:
            return self.wait.until_not(EC.visibility_of_element_located((By.CSS_SELECTOR, selector)))
        except UnexpectedAlertPresentException:
            self.accept_js_alert()
            return self.wait.until_not(EC.visibility_of_element_located((By.CSS_SELECTOR, selector)))

    def wait_until_visible_xpath(self, selector):
        try:
            return self.wait.until(EC.visibility_of_element_located((By.XPATH, selector)))
        except UnexpectedAlertPresentException:
            self.accept_js_alert()
            return self.wait.until(EC.visibility_of_element_located((By.XPATH, selector)))

    def wait_until_visible_css(self, selector):
        print("waiting until visible " + selector)
        try:
            return self.wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, selector)))
        except UnexpectedAlertPresentException:
            self.accept_js_alert()
            return self.wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, selector)))

    def wait_until_clickable_css(self, selector):
        print("waiting until located " + selector)
        try:
            return self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, selector)))
        except (UnexpectedAlertPresentException, TimeoutException):
            self.accept_js_alert()
            return self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, selector)))

    def wait_until_not_clickable(self, selector):
        try:
            return self.wait.until_not(EC.element_to_be_clickable((By.CSS_SELECTOR, selector)))
        except (UnexpectedAlertPresentException, TimeoutException):
            self.accept_js_alert()
            return self.wait.until_not(EC.element_to_be_clickable((By.CSS_SELECTOR, selector)))

    def wait_until_presence_of_all_css(self, selector):
        return self.wait.until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, selector)))

    def wait_until_presence_of_all_xpath(self, selector):
        return self.wait.until(EC.presence_of_all_elements_located((By.XPATH, selector)))

    def wait_until_exists_css(self, selector):
        print("waiting until located " + selector)
        try:
            return self.wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, selector)))
        except UnexpectedAlertPresentException:
            self.accept_js_alert()
            return self.wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, selector)))

    def wait_until_not_exists_css(self, selector):
        print("waiting until located " + selector)
        try:
            return self.wait.until_not(EC.presence_of_element_located((By.CSS_SELECTOR, selector)))
        except UnexpectedAlertPresentException:
            self.accept_js_alert()
            return self.wait.until_not(EC.presence_of_element_located((By.CSS_SELECTOR, selector)))

    def wait_until_not_visible_css(self, selector):
        print("waiting until located " + selector)
        try:
            return self.wait.until_not(EC.visibility_of_element_located((By.CSS_SELECTOR, selector)))
        except UnexpectedAlertPresentException:
            self.accept_js_alert()
            return self.wait.until_not(EC.visibility_of_element_located((By.CSS_SELECTOR, selector)))
        except TimeoutException:
            # everything is so bloody slow.
            print("timeout value failed = " + str(self.wait._timeout))
            return self.wait.until_not(EC.visibility_of_element_located((By.CSS_SELECTOR, selector)))

    def select_dropdown_css_by_index(self, selector, index):
        try:
            Select(self.wait_until_clickable_css(selector)).select_by_index(index)
        except UnexpectedAlertPresentException:
            self.accept_js_alert()
            Select(self.wait_until_clickable_css(selector)).select_by_index(index)

    def select_dropdown_css_by_value(self, selector, value):
        try:
            Select(self.wait_until_clickable_css(selector)).select_by_value(value)
        except UnexpectedAlertPresentException:
            self.accept_js_alert()
            Select(self.wait_until_clickable_css(selector)).select_by_value(value)

    def select_dropdown_css_by_text(self, selector, text):
        try:
            Select(self.wait_until_clickable_css(selector)).select_by_visible_text(text)
        except UnexpectedAlertPresentException:
            self.accept_js_alert()
            Select(self.wait_until_clickable_css(selector)).select_by_visible_text(text)

    def wait_until_present_css(self, selector):
        try:
            return self.wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, selector)))
        except UnexpectedAlertPresentException:
            self.accept_js_alert()
            return self.wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, selector)))

    def wait_until_present_xpath(self, selector):
        try:
            return self.wait.until(EC.presence_of_element_located((By.XPATH, selector)))
        except UnexpectedAlertPresentException:
            self.accept_js_alert()
            return self.wait.until(EC.presence_of_element_located((By.XPATH, selector)))

    def select_checkbox_by_xpath(self, selector):
        self.wait_until_clickable_xpath(selector).click()
        checkbox = self.wait_until_clickable_xpath(selector)
        if not checkbox.isSelected():
            self.wait_until_clickable_xpath(selector).click()
        ok_(checkbox.isSelected())

    def wait_until_title_contains(self, title):
        try:
            self.wait.until(EC.title_contains(title))
        except TimeoutException:
        #         everything is so bloody slow
            print("Failed timeout value = " + str(self.wait._timeout))
            self.wait.until(EC.title_contains(title))

    def implicit_waiter(self, timeout):
        custom_wait = WebDriverWait(self.driver, timeout=timeout)
        try:
            custom_wait.until_not(EC.presence_of_element_located((By.CSS_SELECTOR, "div")))
        except TimeoutException:
            pass

    def select_dropdown_css_by_visible_text_contians(self, selector, text):
        for element in self.wait_until_presence_of_all_css(selector + " > option"):
            if text in element.text:
                self.select_dropdown_css_by_value(selector, element.get_attribute("value"))
                break

    def accept_js_alert(self):
        old_timeout = self.wait._timeout
        self.wait._timeout = 2
        try:
            self.wait.until(EC.alert_is_present())
            self.driver.switch_to.alert.accept()
        except TimeoutException:
            self.driver.execute_script("window.alert = function() {}")
        finally:
            self.wait._timeout = old_timeout

    def wait_until_xpath_contains_text(self, selector, text):
        try:
            self.wait.until(EC.text_to_be_present_in_element((By.XPATH, selector), text))
        #     in case we loose this element on page, try one more time
        except StaleElementReferenceException:
            self.wait.until(EC.text_to_be_present_in_element((By.XPATH, selector), text))
