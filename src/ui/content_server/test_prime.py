from ..base.testcases import UITestCase
from .pageobjects.prime_subtemplates import DynamicForm
from nose.tools import eq_
from ..base.elements import NsInputCheckbox

class PrimeTestCase(UITestCase):
    """
    required params of test:
    - url is url of landing page to preview
    """

    def setUp(self):
        if 'url' not in self.config:
            raise Exception('"url" param is required for test. Pass landing page url with "url" param')

        self.driver.get(self.config['url'])

    def test_dynamic_form(self):
        """
        This test is implementation of task http://jira.netsertive.local/browse/NPI-6703
        Steps commented in code are taken from description of task.
        :return:
        """

        form = DynamicForm(self.driver)

        form.show_form()
        required_fields = []
        fields = form.get_fields()
        document_url = form.document_url.get_attribute('href').strip()

        # 3. Verify input fields are enabled
        for field in fields:
            field.wait_clickable()
            if field.get_attribute('required'):
                required_fields.append(field)

        # 4. Verify СTA Button is clickable
        form.submit_button.wait_clickable()

        # 5. Validate that the form cannot be submitted without the required field(s)
        if len(required_fields):
            form.submit_button.click()
            for field in required_fields:
                eq_('ntsrtv-error', field.get_attribute('class'))

        # fill in form with some predefined values
        for field in fields:
            if isinstance(field, NsInputCheckbox):
                value = NsInputCheckbox.Values.CHECKED
            elif field.get_attribute('type') == 'email':
                value = 'test@email.com'
            else:
                value = 'test value'

            field.set_value(value)

        form.submit_button.click()

        form.thank_you_message.wait_visible()

        if not document_url:
            return

        # Validate new tab was opened
        tabs = self.driver.window_handles
        eq_(2, len(tabs), 'Tab with document url was not opened')

        # Switch to document tab and validate url
        main_tab, document_tab = tabs
        self.driver.switch_to.window(document_tab)
        eq_(document_url, self.driver.current_url)
