from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from ...base.pageobjects import PageObject
from ...base.elements import NsElement, NsInputText, NsInputCheckbox, NsEditable

class DynamicForm(PageObject):

    def __init__(self, driver: WebDriver):
        super().__init__(driver)

        self.static_button = NsElement((By.ID, 'ns_prime_slide_form_button'), self.wait)

        self._set_context(NsElement((By.ID, 'ns_prime_form_block'), self.wait).get_element())

        self.form = NsElement((By.ID, 'ns_prime_web_form'), self.wait)
        self.submit_button = NsElement((By.CSS_SELECTOR, 'button[role=submit]'), self.wait)
        self.thank_you_message = NsElement((By.CLASS_NAME, 'ntsrtv-thank-you-message'), self.wait)
        self.document_url = NsElement((By.ID, 'ns_pdf_link'), self.globalWait)

        self.static_button.wait_present()

    def show_form(self):
        self.static_button.click()
        self.form.wait_visible()

    def get_fields(self) -> list:
        """
        :rtype: list[NsEditable]
        """
        result = []
        fields = self.form.find_elements((By.CSS_SELECTOR, 'input[type=text], input[type=email], input[type=checkbox]'))

        for field in fields:
            type = field.get_attribute('type')
            selector = (By.ID, field.get_attribute('id'))
            if type in ['text', 'email']:
                el = NsInputText(selector, self.wait)
            elif type == 'checkbox':
                el = NsInputCheckbox(selector, self.wait)
            result.append(el)

        return result