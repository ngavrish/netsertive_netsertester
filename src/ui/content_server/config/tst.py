global config
config = {
    'driver': {
        'browser': 'chrome',
    },

    'logcrawler': {
        'local': False,
        'logs-path': '/AppData/webs/tst/content_server/v1/content_server/application/logs',
        'remote-server-url': 'vh1-tstweb-01.netsertive.local',
        'remote-username': 'webuser',
        'remote-password': 'rocker157R',
        'errors_whitelist': {
            'SLOW QUERY FOUND!',
            '404 Page Not Found --> images',
        },
    },
}
