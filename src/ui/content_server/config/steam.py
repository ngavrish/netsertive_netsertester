global config
config = {
    'driver': {
        'browser': 'chrome',
    },

    'logcrawler': {
        'local': False,
        'logs-path': '/Netsertive/Repo/Websites/launcher/application/logs',
        'remote-server-url': 'steam-01.netsertive.local',
        'remote-username': 'webuser',
        'remote-password': 'rocker157R',
        'errors_whitelist': {
            'SLOW QUERY FOUND!',
            '404 Page Not Found --> images',
        },
    },
}
