global config
config = {
    'driver': {
        'browser': 'chrome',
    },

    'logcrawler': {
        'local': False,
        'logs-path': '/AppData/webs/qa/content_server/v1/content_server/application/logs',
        'remote-server-url': 'vh1-qaweb-01.netsertive.local',
        'remote-username': 'webuser',
        'remote-password': 'P1ngP0ng!',
        'errors_whitelist': {
            'SLOW QUERY FOUND!',
            '404 Page Not Found --> images',
        },
    },
}
