global config
config = {
    'driver': {
        'browser': 'chrome',
    },

    'logcrawler': {
        'local': True,
        'logs-path': '/Netsertive/Repo/websites/content_server/application/logs',
        'errors_whitelist': {
            'SLOW QUERY FOUND!',
            '404 Page Not Found --> favicon.ico',
            '404 Page Not Found --> images',
        },
    },
}