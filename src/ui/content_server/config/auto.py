global config
config = {
    'driver': {
        'browser': 'chrome',
    },

    'logcrawler': {
        'local': False,
        'logs-path': '/AppData/webs/qa/launcher/v1/launcher/application/logs',
        'remote-server-url': 'autoenv.netsertive.local',
        'remote-username': 'webuser',
        'remote-password': 'rocker157R',
        'errors_whitelist': {
            'SLOW QUERY FOUND!',
            '404 Page Not Found --> images',
        },
    },
}
