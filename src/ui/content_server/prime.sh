#!/usr/bin/env bash
nosetests test_prime.py -s --tc-file=./config/tst.py --tc-format python --tc=url:"$1"

# an example of usage:
# ./prime.sh "http://tst-dlp_server.netsertive.com/content/serve/37292b5ab33fb12e0ac06bc4f8f754ee?nsck=a08adcd3a55bb33d1ec84cc7b5b134cd"
