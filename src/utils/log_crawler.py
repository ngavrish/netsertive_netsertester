'''
Created on Mar 17, 2015

@author: Gavrish
'''
from datetime import datetime
import subprocess
import paramiko
import pytz

class LogCrawler(object):

    TIMESTAMP_FORMAT = "%Y-%m-%d %H:%M:%S"
    GREP_CMD = "grep 'ERROR - ' /{logs_path}/log-{start}.php"

    def __init__(self, start, finish, config):
        print("----- LOG CRAWLER OUTPUT -----")
        self.local = True if config.get('local', 'True') == 'True' else False
        self.whitelist_counter_map = {}
        if not start:
            self.start = datetime.now(pytz.timezone('America/New_York')).strftime(self.TIMESTAMP_FORMAT)
        else:
            self.start = start
        self.finish = datetime.strptime(finish, self.TIMESTAMP_FORMAT)
        self.grep_errors_cmd = self.GREP_CMD.format(logs_path=config['logs-path'].strip("/"), start=self.start.split(" ")[0])
        if not self.local:
            self.ssh = paramiko.SSHClient()
            self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.ssh.connect(config.get('remote-server-url', 'undefined'),
                             username=config.get('remote-username', 'undefined'),
                             password=config.get('remote-password', 'undefined'))
        self.set_whitelist(config['whitelist'])

    def set_whitelist(self, filepath):
        with open(filepath) as f:
            whitelist = f.read().splitlines()
        for item in whitelist:
            # counter represents the number of errors after this error you should ignore
            # ex: SLOW QUERY dumps 3 further informational errors, for a total of 4
            error, counter = item.split(",")
            self.whitelist_counter_map[error] = counter

    def get_errors(self):
        errors = []
        if self.local:
            error_output = subprocess.getoutput(self.grep_errors_cmd)
            # the output from local is text.  the output from non-local is an array
            if error_output != '':
                error_output = error_output.split('\n')
            else:
                error_output = [];
        else:
            # exec_command returns tuple of stdin, stdout, stderr of the executing command
            # we need stdout that goes to index 1
            error_output = self.ssh.exec_command(self.grep_errors_cmd)[1].readlines()
        self.start = datetime.strptime(self.start, self.TIMESTAMP_FORMAT)
        for error_line in error_output:
            line_date = datetime.strptime(str(error_line.split(" -")[1].strip()), self.TIMESTAMP_FORMAT)
            if self.start < line_date < self.finish:
                errors.append(error_line)
        print("Errors = " + str(errors))
        return errors

    def filter_errors(self):
        print("Whitelist counter map = " + str(self.whitelist_counter_map))
        errors = self.get_errors()
        print("Errors = " + str(errors))
        filtered_errors = []
        if errors:
            i = 0
            while i in range(len(errors)):
                print("Current index = " + (str(i)))
                found_in_whitelist = False
                for error_string, total_lines in self.whitelist_counter_map.items():
                    if error_string in errors[i]:
                        print("Found whitelist error " + error_string + ' in ' + errors[i])
                        # total_lines represents the number of errors after and including this error you should ignore
                        # ex: SLOW QUERY dumps 3 further informational errors, for a total of 4
                        i += int(total_lines) - 1  # we've all ready accounted for this line
                        found_in_whitelist = True
                        break
                if not found_in_whitelist:
                    print("Error not found in whitelist.")
                    filtered_errors.append(errors[i])
                i += 1
        print("Filtered Errors = " + str(filtered_errors))
        return filtered_errors