SET @uuid := (SELECT UUID() AS UUID);
SET @clientXid := (SELECT CASE WHEN MAX(clientXId)+MAX(id) IS NULL THEN 1 ELSE MAX(clientXId)+MAX(id) END FROM client2thirdPartyId);
SET @companyName := CONCAT(@prefix,"1client1", @clientXid);

SET @website := "http://www.netsertive.com";
SET @email := "qaautomation4@netsertive.com";
SET @clientXid := (SELECT MAX(clientXId)+MAX(id) FROM client2thirdPartyId);
# save the global key
SET @globalKey := (SELECT fn_createGlobalKey());

# Insert the Company and Company2CompanyType
INSERT INTO company ( globalKey, companyName, fk_repUserId, fk_businessSegmentId, createDatetime, updateDatetime ) 
VALUES ( @globalKey, @companyName, 112, 6, NOW(), NOW() );

# save the company id
SET @companyId := (SELECT MAX(id) FROM company);

INSERT INTO company2companyType ( fk_companyId, fk_companyTypeId, createDatetime ) 
VALUES ( @companyId, 4, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime )
VALUES ( @companyId, 30, "globalKey", 1, 44, 22, NULL, NULL, @globalKey, NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime )
VALUES ( @companyId, 30, "companyName", 1, 44, 22, NULL, NULL, @companyName, NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @companyId, 30, "fk_repUserId", 1, 44, 22, NULL, NULL, "112", NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @companyId, 30, "fk_businessSegmentId", 1, 44, 22, NULL, NULL, "6", NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @companyId, 43, "fk_companyId", 1, 44, 22, NULL, NULL, @companyId, NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @companyId, 43, "fk_companyTypeId", 1, 44, 22, NULL, NULL, "4", NULL, @uuid, NOW() );

#Insert the Client and Client2ThirdPartyId
INSERT INTO client ( fk_companyId, NAME, googleIsActive, digitalMediaFunds, companyName, fk_timezoneId, webSite, fk_clientRepUserId, DATE, updateDatetime )
VALUES ( @companyId, @companyName, 0, 0, @companyName, 19, @website, 112, NOW(), NOW() );

#save the client Id
SET @clientId = (SELECT MAX(id) FROM client);

INSERT INTO client2thirdPartyId ( STATUS, fk_clientId, fk_thirdPartyId, clientXId, createDatetime, updateDatetime ) 
VALUES ( 0, @clientId, 3, @clientXid, NOW(), NOW() );

# save the client2thirdPartyId
SET @client2thirdPartyId = (SELECT MAX(id) FROM client2thirdPartyId);

INSERT INTO clientChangeHistory ( fk_userId, ACTION, fk_historyActionId, actionBatchId, fk_clientId, field, oldValue, newValue, actionDatetime ) 
VALUES ( 44, "Add", 0, @uuid, @clientId, "fk_companyId", NULL, @companyId, NOW() );	
INSERT INTO clientChangeHistory ( fk_userId, ACTION, fk_historyActionId, actionBatchId, fk_clientId, field, oldValue, newValue, actionDatetime ) 
VALUES ( 44, "Add", 0, @uuid, @clientId, "name", NULL, @companyName, NOW() );	
INSERT INTO clientChangeHistory ( fk_userId, ACTION, fk_historyActionId, actionBatchId, fk_clientId, field, oldValue, newValue, actionDatetime ) 
VALUES ( 44, "Add", 0, @uuid, @clientId, "googleIsActive", NULL, "0", NOW() );	
INSERT INTO clientChangeHistory ( fk_userId, ACTION, fk_historyActionId, actionBatchId, fk_clientId, field, oldValue, newValue, actionDatetime ) 
VALUES ( 44, "Add", 0, @uuid, @clientId, "digitalMediaFunds", NULL, "0", NOW() );	
INSERT INTO clientChangeHistory ( fk_userId, ACTION, fk_historyActionId, actionBatchId, fk_clientId, field, oldValue, newValue, actionDatetime ) 
VALUES ( 44, "Add", 0, @uuid, @clientId, "bingIsActive", NULL, "0", NOW() );	
INSERT INTO clientChangeHistory ( fk_userId, ACTION, fk_historyActionId, actionBatchId, fk_clientId, field, oldValue, newValue, actionDatetime ) 
VALUES ( 44, "Add", 0, @uuid, @clientId, "companyName", NULL, @companyName, NOW() );	
INSERT INTO clientChangeHistory ( fk_userId, ACTION, fk_historyActionId, actionBatchId, fk_clientId, field, oldValue, newValue, actionDatetime ) 
VALUES ( 44, "Add", 0, @uuid, @clientId, "fk_timezoneId", NULL, "19", NOW() );	
INSERT INTO clientChangeHistory ( fk_userId, ACTION, fk_historyActionId, actionBatchId, fk_clientId, field, oldValue, newValue, actionDatetime ) 
VALUES ( 44, "Add", 0, @uuid, @clientId, "webSite", NULL, @website, NOW() );	
INSERT INTO clientChangeHistory ( fk_userId, ACTION, fk_historyActionId, actionBatchId, fk_clientId, field, oldValue, newValue, actionDatetime ) 
VALUES ( 44, "Add", 0, @uuid, @clientId, "fk_clientRepUserId", NULL, "112", NOW() );	
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @clientId, 1, "fk_companyId", 1, 44, 22, NULL, NULL, @companyId, NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @clientId, 1, "name", 1, 44, 22, NULL, NULL, @companyName, NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @clientId, 1, "googleIsActive", 1, 44, 22, NULL, NULL, "0", NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @clientId, 1, "digitalMediaFunds", 1, 44, 22, NULL, NULL, "0", NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @clientId, 1, "bingIsActive", 1, 44, 22, NULL, NULL, "0", NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @clientId, 1, "companyName", 1, 44, 22, NULL, NULL, @companyName, NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @clientId, 1, "fk_timezoneId", 1, 44, 22, NULL, NULL, "19", NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @clientId, 1, "webSite", 1, 44, 22, NULL, NULL, @website, NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @clientId, 1, "fk_clientRepUserId", 1, 44, 22, NULL, NULL, "112", NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @client2thirdPartyId, 40, "status", 1, 44, 22, NULL, NULL, "0", NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @client2thirdPartyId, 40, "fk_clientId", 1, 44, 22, NULL, NULL, @clientId, NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @client2thirdPartyId, 40, "fk_thirdPartyId", 1, 44, 22, NULL, NULL, "3", NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @client2thirdPartyId, 40, "clientXId", 1, 44, 22, NULL, NULL, "0", NULL, @clientXid, NOW() );

# Insert Address and Client2Address
-- with coordinates
/* INSERT INTO address ( street1, street2, city, state, zip, country, latitude, longitude, fk_addressTypeId, createDate, updateDatetime ) 
VALUES ( "The Old Gas Works", "2 Michael Road", "London", "GB", "SW6 2AD", "GB", 51.477781, -0.1883395, 3, NOW(), NOW() ); */
/* INSERT INTO address ( street1, street2, city, state, zip, country, latitude, longitude, fk_addressTypeId, createDate, updateDatetime )
VALUES ( "The Old Gas Works", "2 Michael Road", "London", "GB", "SW6 2AD", "GB", 51.477781, -0.1883395, 3, NOW(), NOW() ); */
# 10250 Santa Monica Blvd.
# Suite 616
# Los Angeles, CA 90067
# without cooedinates
# INSERT INTO address ( street1, street2, city, state, zip, country, latitude, longitude, fk_addressTypeId, createDate, updateDatetime )
# VALUES ( "The Old Gas Works", "2 Michael Road", "London", "GB", "SW6 2AD", "GB", 51.477781, -0.1883395, 3, NOW(), NOW() );

INSERT INTO address ( street1, street2, city, state, zip, country, latitude, longitude, fk_addressTypeId, createDate, updateDatetime )
VALUES ( "10250 Santa Monica Blvd.", "Suite 616 ", "Los Angeles", "CA", "90067", "US", NULL, NULL, 3, NOW(), NOW() );

# save the address id
SET @addressId = (SELECT MAX(id) FROM address);

INSERT INTO client2address ( fk_addressId, fk_clientId, createDate, updateDatetime ) 
VALUES ( @addressId, @clientId, NOW(), NOW() );

# save the client2address id
SET @client2addressId = (SELECT MAX(id) FROM client2address);

INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @addressId, 5, "street1", 1, 44, 22, NULL, NULL, "The Old Gas Works", NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @addressId, 5, "street2", 1, 44, 22, NULL, NULL, "2 Michael Road", NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @addressId, 5, "city", 1, 44, 22, NULL, NULL, "London", NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @addressId, 5, "state", 1, 44, 22, NULL, NULL, "GB", NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @addressId, 5, "zip", 1, 44, 22, NULL, NULL, "SW6 2AD", NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @addressId, 5, "country", 1, 44, 22, NULL, NULL, "GB", NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @addressId, 5, "fk_addressTypeId", 1, 44, 22, NULL, NULL, "3", NULL, @uuid, NOW() );
-- running withoug presetting coordinates
/* INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @addressId, 5, "latitude", 1, 44, 22, NULL, NULL, "51.477781", NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @addressId, 5, "longitude", 1, 44, 22, NULL, NULL, "-0.1883395", NULL, @uuid, NOW() ); */
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @client2addressId, 43, "fk_addressId", 1, 44, 22, NULL, NULL, @addressId, NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @client2addressId, 43, "fk_clientId", 1, 44, 22, NULL, NULL, @clientId, NULL, @uuid, NOW() );

# Insert Contact and ContactPhone
INSERT INTO contact ( name, title, email, fk_clientId, fk_contactTypeId, createDate, updateDatetime ) 
VALUES ( "Will Brocklebank", "", @email, @clientId, 1, NOW(), NOW() );

# save the contact id
SET @contactId := (SELECT MAX(id) FROM contact);

INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @contactId, 31, "name", 1, 44, 22, NULL, NULL, "Will Brocklebank", NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @contactId, 31, "title", 1, 44, 22, NULL, NULL, "", NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @contactId, 31, "email", 1, 44, 22, NULL, NULL, @email, NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @contactId, 31, "fk_clientId", 1, 44, 22, NULL, NULL, @clientId, NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @contactId, 31, "fk_contactTypeId", 1, 44, 22, NULL, NULL, "1", NULL, @uuid, NOW() );

INSERT INTO contactPhone ( fk_contactId, phone, fk_phoneTypeId, primaryPhone, createDatetime, updateDatetime ) 
VALUES ( @contactId, "(555)73849121", 1, 1, NOW(), NOW() );

# save the contact phone id
SET @contactPhoneId := (SELECT MAX(id) FROM contactPhone);

INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @contactPhoneId, 45, "fk_contactId", 1, 44, 22, NULL, NULL, @contactId, NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @contactPhoneId, 45, "phone", 1, 44, 22, NULL, NULL, "(555)73849121", NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @contactPhoneId, 45, "fk_phoneTypeId", 1, 44, 22, NULL, NULL, "1", NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @contactPhoneId, 45, "primaryPhone", 1, 44, 22, NULL, NULL, "1", NULL, @uuid, NOW() );

INSERT INTO contactPhone ( fk_contactId, phone, fk_phoneTypeId, primaryPhone, createDatetime, updateDatetime ) 
VALUES ( @contactId, "(555)38709797", 2, 0, NOW(), NOW() );

# save the contact phone id
SET @contactPhoneId := (SELECT MAX(id) FROM contactPhone);

INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @contactPhoneId, 45, "fk_contactId", 1, 44, 22, NULL, NULL, @contactId, NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @contactPhoneId, 45, "phone", 1, 44, 22, NULL, NULL, "(555)38709797", NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @contactPhoneId, 45, "fk_phoneTypeId", 1, 44, 22, NULL, NULL, "2", NULL, @uuid, NOW() );
INSERT INTO history ( entityID, fk_entityTypeId, columnName, fk_actionId, fk_userId, fk_applicationListId, userId, oldValue, newValue, note, batchId, createDatetime ) 
VALUES ( @contactPhoneId, 45, "primaryPhone", 1, 44, 22, NULL, NULL, "0", NULL, @uuid, NOW() );