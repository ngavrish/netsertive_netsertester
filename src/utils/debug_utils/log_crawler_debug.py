"""
Created on Apr 14, 2015

@author = tclark@netsertive.com

Test is used strictly for debugging logCrawler part internally.
Test used to run on QA environment.
Goal: validate that on certain scenarios, server get errors logged and logCrawler catches those errors
"""
import time
from ...ui.flux_capacitor.model.grid import Grid

from ...ui.generator_test import GeneratorTest, check_logs
from ...ui.flux_capacitor.model.flux_capacitor import FluxCapacitor
from ...ui.lyon.model.lyon import LyonModel

class SmokeTestClientGridSort_Test(GeneratorTest):
    """
    Covers Smoke Testing of Client Center Default grid - sorting asc and desc
    """
    def __init__(self):
        super().__init__(app='amc')

    @classmethod
    def setUpClass(self):
        super().setUpClass()

    @check_logs
    def amc(self):
        self.layout_model = None
        self.amc_data = self.get_json_data("amc_data.json")
        self.ID = str(time.time())
        self.amc_model = FluxCapacitor(driver=self.driver)
        self.driver.maximize_window()

        self.amc_model = FluxCapacitor(driver=self.driver)
        self.grid = Grid(driver=self.driver)
        self.driver.get(self.web_ui_credentials.url)
        self.driver.maximize_window()

        self.amc_model.login(self.web_ui_credentials.login, self.web_ui_credentials.password)

        # Access bad url to generate error in logs
        self.driver.get("http://qa-dashboard.netsertive.local/reports/flux_capacitor/client_center/get_list/context/client/target/venue/client_id/1741123123")
        # Navigate back to valid url then logout
        self.driver.get("http://qa-dashboard.netsertive.local/reports/flux_capacitor/client_center/get_list/context/client/target/venue/client_id/1741")
        self.amc_model.logout()

    @check_logs
    def lyon(self):
        lyon_model = LyonModel(driver=self.driver)
        self.driver.get(self.config['QA_LYON']['URL'])
        self.driver.maximize_window()
        lyon_model.login(login=self.config['QA_LYON']['LOGIN'], password=self.config['QA_LYON']['PASSWORD'])

        # Access bad url to generate error in logs
        self.driver.get("https://qa-login.netsertive.com/dashboard/performance123")
        # Navigate back to valid url then logout
        self.driver.get("https://qa-login.netsertive.com/dashboard/performance")
        lyon_model.logout()


    def test(self):
        self.driver.get(self.config['QA_DASHBOARD']['URL'])
        yield self.amc
        # yield self.lyon



        #
        # # cannot test 2 application in 1 suite. we have 'app' parameter that helps us to define which logs should look
        # # into
        # yield self.ui_wrapper, amc_test, self.config
        # yield self.ui_wrapper, lyon_test, self.config
