#!/usr/bin/env bash
#
# Import new client without NetsuiteSynchManager. It doesn't provide much flexibility in case of setting different client
# params. Most of client info is default. That default data can be changed from import_client_inserts.sql
#
# How it works:
# first line compiles sql file that would be executed
# second line connects to remote db and runs script
#
# Script works with positioned arguments $1 - environment
# $1 - String. Database that we are connecting to
# $2 - String. Prefix for company name.
#
# Note: you can use the same prefix. Company name uniqueness is supported automatically inside SQL script.
#
# Example:
# sh import_client.sh 'qa_core' 'my_new_shiny_client'
#
# Requirements:
# import_client_inserts.sql should be in the same folder with the script
# mysql host should be available
#

echo "set @prefix='$2';" > import_client.sql;
cat import_client_inserts.sql >> import_client.sql;

DEV_CORE_USER="$(sed -n "/db/,/\[/p" ../../setup.cfg | awk -F "dev_core_user=" '{print $2}' | sed '/^$/d')"
DEV_CORE_PASSWORD="$(sed -n "/db/,/\[/p" ../../setup.cfg | awk -F "dev_core_password=" '{print $2}' | sed '/^$/d')"
QA_CORE_USER="$(sed -n "/db/,/\[/p" ../../setup.cfg | awk -F "qa_core_user=" '{print $2}' | sed '/^$/d')"
QA_CORE_PASSWORD="$(sed -n "/db/,/\[/p" ../../setup.cfg | awk -F "qa_core_password=" '{print $2}' | sed '/^$/d')"
TST_CORE_USER="$(sed -n "/db/,/\[/p" ../../setup.cfg | awk -F "tst_core_user=" '{print $2}' | sed '/^$/d')"
TST_CORE_PASSWORD="$(sed -n "/db/,/\[/p" ../../setup.cfg | awk -F "tst_core_password=" '{print $2}' | sed '/^$/d')"

if [ "$1" == 'qa_core' ]; then
	mysql -h vh1-qadb-01.netsertive.local --user=$QA_CORE_USER --password=$QA_CORE_PASSWORD $1 -A < import_client.sql
fi
if [ "$1" == 'tst_core' ]; then
	mysql -h vh1-tstdb-01.netsertive.local --user=$TST_CORE_USER --password=$TST_CORE_PASSWORD $1 -A < import_client.sql
fi
if [ "$1" == 'dev_core' ]; then
	mysql -h localhost --user=$DEV_CORE_USER --password=$DEV_CORE_PASSWORD $1 -A < import_client.sql
fi
