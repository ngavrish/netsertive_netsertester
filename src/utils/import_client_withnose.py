import subprocess
from testconfig import config

'''
Created on Mar 24, 2015

@author: Gavrish
'''


def import_client_test_prerequisite():
    """
    Import client into data base using nose fw
    """
    subprocess.call('sh import_client.sh "' + config.get('db', 'qa_core') + '" "' + config.get('name', 'qaautomation')
                    + '" "' + config.get('env', 'qa') + '"', shell=True)
