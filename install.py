import subprocess

cmd = subprocess.call
sp = "\n\n"

print(sp + "STARTING NETSERTESTER INSTALLATION...")

print(sp + "INSTALLING REQUIREMENTS.TXT...")
cmd('sudo pip3 install -r requirements.txt', shell=True)

print(sp + "INSTALLING CUSTOM NOSE HTML OUTPUT PLUGIN...")
cmd('python3 setup.py build', shell=True, cwd="./plugins/nose-htmloutput-withscreens/")
cmd('python3 setup.py install', shell=True, cwd="./plugins/nose-htmloutput-withscreens/")

print(sp + "INSTALLING NOSE-SELENIUM-ERROR-HANDLER")
cmd('python3 setup.py build', shell=True, cwd="./plugins/nose-selenium-error-handler/")
cmd('python3 setup.py install', shell=True, cwd="./plugins/nose-selenium-error-handler/")

print(sp + "UPDATING FOLDER PERMISSIONS FOR HTML REPORTS AND SCREENSHOTS...")
cmd('sudo chmod -R 777 src', shell=True)

print(sp + "Success! Thank you for using netsertester." + sp)
