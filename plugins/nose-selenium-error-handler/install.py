import subprocess

if __name__ == "__main__":
    print("\n\n Installing nose-selenium-error-handler... \n\n")
    subprocess.call('python3 setup.py build', shell=True)
    subprocess.call('python3 setup.py install', shell=True)
    print("\nDone")