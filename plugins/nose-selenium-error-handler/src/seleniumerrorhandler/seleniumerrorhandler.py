import os
from datetime import datetime
from nose.plugins import Plugin


class SeleniumErrorHandler(Plugin):
    """
    Save screenshot on error or fail
    """

    def options(self, parser, env=os.environ):
        super().options(parser, env=env)

    def configure(self, options, conf):
        super().configure(options, conf)
        if not self.enabled:
            return

    @staticmethod
    def _handle_error(test, err):
        print(err)

        if not test.test:
            return

        test = test.test

        if not test.inst or not test.inst.driver:
            return

        path = os.path.realpath(os.path.join(os.getcwd(), 'screenshots', 'errors'))

        if not os.path.exists(path):
            os.makedirs(path)

        filename = datetime.now().strftime(test.method.__name__ + '_%Y-%m-%d_%H-%M-%S.png')

        driver = test.inst.driver
        driver.get_screenshot_as_file(os.path.join(path, filename))

        # browser console logs
        print(driver.get_log('browser'))

    def handleError(self, test, err):
        self._handle_error(test, err)

    def handleFailure(self, test, err):
        self._handle_error(test, err)