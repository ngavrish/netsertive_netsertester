from setuptools import setup, find_packages

setup(
    name = 'seleniumerrorhandler',
    version = '0.0.1',
    install_requires = ['nose', 'selenium'],
    packages = find_packages('src'),
    package_dir = {'':'src'},
    entry_points = {
        'nose.plugins.0.10': [
            'seleniumerrorhandler = seleniumerrorhandler:SeleniumErrorHandler'
        ]
    }
)