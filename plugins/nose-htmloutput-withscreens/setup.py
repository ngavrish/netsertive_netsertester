# -*- encoding: utf-8 -*-
import glob
import io
from os.path import basename
from os.path import dirname
from os.path import join
from os.path import splitext

from setuptools import find_packages
from setuptools import setup


def read(*names, **kwargs):
    return io.open(
        join(dirname(__file__), *names),
        encoding=kwargs.get("encoding", "utf8")
    ).read()

# copy template

setup(
    name="nose-htmloutput-withscreens",
    version="0.6.0",
    license="BSD",
    description="Nose plugin that generates a nice html test report and creates .",    
    author="Ionel Cristian Mărieș, Nikita Gavrish",
    include_package_data=True,
    packages=find_packages("src"),
    package_dir={'': 'src'},
    package_data={'': ['src/*.html']},
    py_modules=[splitext(basename(i))[0] for i in glob.glob("src/*.py")],

    zip_safe=False,
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Operating System :: Unix",
        "Operating System :: POSIX",
        "Operating System :: Microsoft :: Windows",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2.6",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: Implementation :: CPython",
        "Programming Language :: Python :: Implementation :: PyPy",
        "Topic :: Utilities",
    ],
    keywords=[
    ],
    install_requires=[
        'Jinja2',
        'nose',
    ],
    extras_require={
    },
    entry_points={
        'nose.plugins.0.10': [
            'html = nose_htmloutput:HtmlOutput'
        ]
    }

)
